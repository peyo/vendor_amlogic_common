#
# Copyright (C) 2014 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(PRODUCT_USE_PREBUILT_GTVS),yes)
GMS_PRODUCTS := vendor/google_gtvs/overlays/products
GMS_ETC := vendor/google_gtvs/overlays/etc
GMS_SEPOLICY := vendor/google_gtvs/sepolicy/tv
else
GMS_PRODUCTS := vendor/google/gms/src/products
GMS_ETC := vendor/google/data/etc
GMS_SEPOLICY := vendor/google/gms/src/sepolicy/tv
endif

PRODUCT_PACKAGES := \
    AndroidMediaShell \
    AtvRemoteService \
    BugReportSender \
    GooglePackageInstaller \
    FrameworkPackageStubs \
    GoogleCalendarSyncAdapter \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    GoogleTTS \
    LatinIMEGoogleTvPrebuilt \
    PrebuiltGmsCorePano \
    PlayGamesPano \
    SssAuthbridgePrebuilt \
    talkback \
    Tubesky \
    WebViewGoogle \
    YouTubeLeanback \
    YouTubeMusicTVPrebuilt \
    GoogleExtServices \
    GoogleExtShared \
    TVCustomization \
    VideosPano \
    Backdrop \
    Katniss \
    TVLauncher \
    TVRecommendations \
    SetupWraithPrebuilt

# Configuration files for GMS apps
PRODUCT_COPY_FILES += \
    $(GMS_ETC)/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    $(GMS_ETC)/permissions/privapp-permissions-google-system.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google.xml \
    $(GMS_ETC)/permissions/privapp-permissions-google-product.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google-p.xml \
    $(GMS_ETC)/permissions/privapp-permissions-google-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-google-se.xml \
    $(GMS_ETC)/permissions/privapp-permissions-atv-product.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-atv-product.xml \
    $(GMS_ETC)/permissions/privapp-permissions-atv.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-atv.xml \
    $(GMS_ETC)/permissions/split-permissions-google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/split-permissions-google.xml \
    $(GMS_ETC)/sysconfig/google_atv.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_atv.xml \
    $(GMS_ETC)/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google-hiddenapi-package-whitelist.xml

# Play FSI certificate for fs-verity verification.
ifeq ($(PRODUCT_USE_PREBUILT_GTVS),yes)
PRODUCT_COPY_FILES += \
    $(GMS_ETC)/play/play_store_fsi_cert.der:$(TARGET_COPY_OUT_PRODUCT)/etc/security/fsverity/play_store_fsi_cert.der
else
PRODUCT_PACKAGES += \
    play_store_fsi_cert
endif

# Overlay for GMS devices
$(call inherit-product, device/sample/products/backup_overlay.mk)
$(call inherit-product, device/sample/products/location_overlay.mk)
PRODUCT_PACKAGE_OVERLAYS += $(GMS_PRODUCTS)/gms_tv_overlay
PRODUCT_PACKAGE_OVERLAYS += $(GMS_PRODUCTS)/gms_overlay

# Overrides
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.gmsversion=Q_amlogic

# GMS apps additional sepolicy (eg. AndroidMediaShell access to Widevine)
BOARD_SEPOLICY_DIRS += $(GMS_SEPOLICY)
