/*
 * Copyright (c) 2014 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 *     AMLOGIC UsbCameraReceiver
 */

package com.droidlogic;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ContentProviderClient;
import android.media.tv.TvContract;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

import com.droidlogic.app.AudioSettingManager;
import com.droidlogic.app.AudioSystemCmdManager;
import com.droidlogic.app.DroidLogicUtils;
import com.droidlogic.app.tv.AudioEffectManager;

public class DroidlogicApplication extends Application {
    private static final String TAG = "DroidlogicApplication";
    private AudioSettingManager mAudioSettingManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        mAudioSettingManager = new AudioSettingManager(this);
        mHandler.sendEmptyMessage(MSG_CHECK_BOOTVIDEO_FINISHED);
    }

    private boolean isBootvideoStopped() {
        ContentProviderClient tvProvider = null;

        if (mAudioSettingManager.isTunerAudio()) {
            tvProvider = getContentResolver().acquireContentProviderClient(TvContract.AUTHORITY);
        }

        return (mAudioSettingManager.isTunerAudio() && tvProvider != null || !mAudioSettingManager.isTunerAudio()) &&
                (((SystemProperties.getInt("persist.vendor.media.bootvideo", 50)  > 100)
                        && TextUtils.equals(SystemProperties.get("service.bootvideo.exit", "1"), "0"))
                || ((SystemProperties.getInt("persist.vendor.media.bootvideo", 50)  <= 100)));
    }

    private static final int MSG_CHECK_BOOTVIDEO_FINISHED = 0;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CHECK_BOOTVIDEO_FINISHED:
                    if (isBootvideoStopped()) {
                        Log.d(TAG, "bootvideo stopped, start initializing audio");
                        initAudio();
                    } else {
                        if (DroidLogicUtils.getAudioDebugEnable()) {
                            Log.d(TAG, "handleMessage sendEmptyMessageDelayed MSG_CHECK_BOOTVIDEO_FINISHED");
                        }
                        mHandler.sendEmptyMessageDelayed(MSG_CHECK_BOOTVIDEO_FINISHED, 10);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void initAudio () {
        startDroidLogicServices(AudioSystemCmdManager.SERVICE_PACKEGE_NANME, AudioSystemCmdManager.SERVICE_NANME);
        startDroidLogicServices(AudioEffectManager.SERVICE_PACKEGE_NANME, AudioEffectManager.SERVICE_NANME);
        mAudioSettingManager.registerSurroundObserver();
        mAudioSettingManager.initSystemAudioSetting();
    }

    private void startDroidLogicServices (String packageName, String name) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, name));
        intent.setAction(name + ".STARTUP");
        startService(intent);
        Log.i(TAG, "startDroidLogicServices startup service:" + name);
    }
}

