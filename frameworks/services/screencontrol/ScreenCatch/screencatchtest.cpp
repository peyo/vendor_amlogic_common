/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_NDEBUG 0
#define LOG_TAG "screencatch"

#include <errno.h>
#include <cutils/log.h>
#include <cutils/atomic.h>

//#include <media/stagefright/MediaSource.h>
//#include <media/stagefright/MediaBuffer.h>
#include <OMX_IVCommon.h>

#include <utils/List.h>
#include <utils/RefBase.h>
#include <utils/threads.h>

#include <media/stagefright/MetaData.h>

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <ui/PixelFormat.h>
//#include <ui/DisplayInfo.h>

#include <system/graphics.h>

//#include <SkBitmap.h>
//#include <SkDocument.h>
//#include <SkStream.h>

// TODO: Fix Skia.

#include "ScreenCatch.h"
#include "../ScreenManager.h"

using namespace android;

/*****************************************************************************/
#define SAVE_FILE_PNG  0
#define SAVE_FILE_JPEG 1
#define SAVE_FILE_BIN  2

static const char *opt_str = "hpjbt:";
static void help(char *appName)
{
    printf(
        "Usage:\n"
        "  %s [-h] [-p/-j/-b] [-t <type>] [left  top  right  bottom  outWidth  outHeight] \n"
        "\n"
        "Parameters:\n"
        "  -h  :  show this help \n"
        "  -p  :  save as png file (default) \n"
        "  -j  :  save as jpeg file \n"
        "  -b  :  save as binary file \n"
        "  -t <type> : set capture type:\n"
        "             0 -- video only \n"
        "             1 -- video+osd (default) \n"
        "  left  top  right  bottom : capture area, default as 720P (0,0,1280,720) \n"
        "  outWidth  outHeight : output size, default as 720P (1280,720) \n"
        "\n"
        "---NOTICE---\n"
        "Pls run following commands before use:\n"
        "      mkdir -p /data/temp; chmod 777 /data/temp \n"
        , appName);
}

#if 0
static SkColorType flinger2skia(PixelFormat f) {
    switch (f) {
        case PIXEL_FORMAT_RGB_565:
            return kRGB_565_SkColorType;
        default:
            return kN32_SkColorType;
    }
}
#endif

int main(int argc, char **argv)
{
    using namespace android;

    status_t ret = NO_ERROR;
    int status;
    int dumpfd;
    uint32_t type = 1;
    int framecount = 0;
    uint32_t f = PIXEL_FORMAT_RGBA_8888;
    size_t size = 0;
    int ch;
    int saveFileType = SAVE_FILE_PNG;
    int left=0, top=0, right=1280, bottom=720;
    int outWidth=1280, outHeight=720;
    int tmpArgIdx = 0;

    ScreenCatch* mScreenCatch;

    while ((ch = getopt(argc, argv, opt_str)) != -1) {
        switch (ch) {
        case 'h': help(argv[0]); exit(0);
        case 'p': saveFileType = SAVE_FILE_PNG; break;
        case 'j': saveFileType = SAVE_FILE_JPEG; break;
        case 'b': saveFileType = SAVE_FILE_BIN; break;
        case 't': type = atoi(optarg); break;
        default: tmpArgIdx = optind; break;
        }
    }
    printf("argc=%d, tmpArgIdx=%d\n", argc, tmpArgIdx);
    if (++tmpArgIdx < argc && (argc-tmpArgIdx) >= 6) {
        left = atoi(argv[tmpArgIdx++]);
        top = atoi(argv[tmpArgIdx++]);
        right = atoi(argv[tmpArgIdx++]);
        bottom = atoi(argv[tmpArgIdx++]);
        outWidth = atoi(argv[tmpArgIdx++]);
        outHeight = atoi(argv[tmpArgIdx++]);
    }
    printf("type=%d(%s), file type:%s\n"
           "(left,top,right,bottom)=(%d,%d,%d,%d)\n"
           "out(width,height)=(%d,%d)\n",
        type, type==0?"video only":"video+osd",
        SAVE_FILE_PNG==saveFileType?"PNG":(SAVE_FILE_JPEG==saveFileType?"JPEG":"BINARY"),
        left, top, right, bottom, outWidth, outHeight);

    size = outWidth * outHeight * 4;

    sp<MemoryHeapBase> memoryBase(new MemoryHeapBase(size, 0, "screen-capture"));
    void* const base = memoryBase->getBase();

    if (base != MAP_FAILED) {
        fprintf(stderr, "start screencap\n");
        mScreenCatch = new ScreenCatch(outWidth, outHeight, 32, type);
        mScreenCatch->setVideoCrop(left, top, right, bottom);

        MetaData* pMeta;
        pMeta = new MetaData();
        pMeta->setInt32(kKeyColorFormat, OMX_COLOR_Format32bitARGB8888);
        mScreenCatch->start(pMeta);
        char dump_path[128];
        char dump_dir[64] = "/data/temp";

        MediaBuffer *buffer;
        while (framecount < 1) {
            status = mScreenCatch->read(&buffer);
            if (status != OK) {
                usleep(100);
                continue;
            }

            framecount++;
            if (SAVE_FILE_PNG == saveFileType) {
                sprintf(dump_path, "%s/%d.png", dump_dir, framecount);
            } else if (SAVE_FILE_JPEG == saveFileType) {
                sprintf(dump_path, "%s/%d.jpeg", dump_dir, framecount);
            } else {
                sprintf(dump_path, "%s/%d.bin", dump_dir, framecount);
            }
            printf("Save:%s, size=%d\n", dump_path, buffer->size());

            dumpfd = open(dump_path, O_CREAT | O_RDWR | O_TRUNC, 0644);

            if (SAVE_FILE_PNG == saveFileType || SAVE_FILE_JPEG == saveFileType) {
#if 0
                memcpy(base, buffer->data(), buffer->size());
                const SkImageInfo info = SkImageInfo::Make(outWidth, outHeight, flinger2skia(f), kPremul_SkAlphaType, nullptr);
                SkPixmap pixmap(info, base, outWidth * bytesPerPixel(f));
                struct FDWStream final : public SkWStream {
                    size_t fBytesWritten = 0;
                    int fFd;
                    FDWStream(int f) : fFd(f) {}
                    size_t bytesWritten() const override {
                        return fBytesWritten;
                    }
                    bool write(const void* buffer, size_t size) override {
                        fBytesWritten += size;
                        return size == 0 || ::write(fFd, buffer, size) > 0;
                    }
                } fdStream(dumpfd);
                if (SAVE_FILE_PNG == saveFileType) {
                    (void)SkEncodeImage(&fdStream, pixmap, SkEncodedImageFormat::kPNG, 100);
                } else {
                    (void)SkEncodeImage(&fdStream, pixmap, SkEncodedImageFormat::kJPEG, 100);
                }
#else
                //TODO: save as jpeg/png file
                fprintf(stderr, "TYPE: [%s] - Not Support!!!\n", saveFileType==SAVE_FILE_JPEG?"JPG/JPEG":"PNG");
#endif
            }else {
                //save binary
                write(dumpfd, buffer->data(), buffer->size());
            }
            fprintf(stderr, "Transform finish!\n");

            close(dumpfd);
            buffer->release();
            buffer = NULL;
        }

        memoryBase.clear();
        mScreenCatch->stop();
        pMeta->clear();
        delete mScreenCatch;
    } else {
        ret = UNKNOWN_ERROR;
    }
    ALOGI("[%s %d] screencap finish", __FUNCTION__, __LINE__);
    return ret;
}
/*****************************************************************************/
