#define LOG_TAG "WebVtt"

#include "WebVtt.h"

SimpleWebVtt::SimpleWebVtt(std::shared_ptr<DataSource> source): ExtParser(source) {
    mExtParserUtils = new ExtParserUtils(0, source);
}

SimpleWebVtt::~SimpleWebVtt() {
    if (mExtParserUtils != NULL) {
        delete mExtParserUtils;
        mExtParserUtils = NULL;
    }
}

subtitle_t * SimpleWebVtt::ExtSubtitleParser(subtitle_t *current) {
    char line[LINE_LEN + 1];
    int a1=0, a2=0, a3=0, a4=0, b1=0, b2=0, b3=0, b4=0;
    char *p = NULL;
    int i, len;
    while (!current->text.text[0]) {
        if (!mExtParserUtils->ExtSubtitlesfileGets(line)) {
            return NULL;
        }
        if ((len = sscanf(line, "%d:%d:%d.%d --> %d:%d:%d.%d", &a1, &a2, &a3, &a4, &b1, &b2, &b3, &b4)) < 10) {
            if ((len = sscanf(line, "%d:%d.%d --> %d:%d.%d",
                &a2, &a3, &a4,  &b2, &b3, &b4)) < 6) {
                continue;
            }
        }
        current->start = a1 * 360000 + a2 * 6000 + a3 * 100 + a4 / 10;
        current->end = b1 * 360000 + b2 * 6000 + b3 * 100 + b4 / 10;

        for (i = 0; i < SUB_MAX_TEXT;) {
            if (!mExtParserUtils->ExtSubtitlesfileGets(line)) {
                break;
            }
            len = 0;

            for (p = line; *p != '\n' && *p != '\r' && *p;
                    p++, len++);

            if (len) {
                int j = 0, skip = 0;
                char *curptr = current->text.text[i] = (char *)MALLOC(len + 1);
                if (!current->text.text[i]) {
                    return NULL;
                }

                if (len >2 && line[0] == '-' && line[1] == ' ') {
                    skip = 2;
                }
                for (; j < len; j++) {
                    if (skip-- > 0) continue;
                    *curptr = line[j];
                    curptr++;
                }
                *curptr = '\0';
                i++;
            }
            else {
                break;
            }
        }
        current->text.lines = i;
    }
    return current;
}

