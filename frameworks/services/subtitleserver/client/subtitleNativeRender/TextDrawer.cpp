#include "SkTextBox.h"
#include "TextDrawer.h"
TextDrawer& TextDrawer::GetInstance() {
    return _instance;
}
TextDrawer TextDrawer::_instance;

TextDrawer::TextDrawer() {
    // TODO: initialize CJK fonts.
    mTypeFace =SkTypeface::MakeFromFile("/system/fonts/NotoSansCJK-Regular.ttc");
    if (mTypeFace == nullptr) {
        ALOGE("Error! cannot initialize typeface");
    } else {
        ALOGD("initialized typeface");
    }
}

TextDrawer::~TextDrawer() {

}

bool TextDrawer::drawLine(SkCanvas &canvas, const char *data) {

    /*
        1. clear canvas dirty
    */
    canvas.clear(0x00000000);

    SkTextBox box;

// Draw
    SkPaint paint;
    paint.setARGB(255, 255, 255, 255);
    paint.setTextSize(40);
    paint.setAntiAlias(true);
    paint.setSubpixelText(true);
    paint.setLCDRenderText(true);
    paint.setTypeface(mTypeFace);

    paint.setTextAlign(SkPaint::kCenter_Align);


    SkImageInfo info = canvas.imageInfo();

    // Skia can have auto line break and text wrapper.
    box.setBox(0, 0, info.width(), info.height());
    box.setSpacingAlign(SkTextBox::SpacingAlign::kEnd_SpacingAlign);

    box.setText(data, strlen(data), paint);
    box.draw(&canvas);

    SkRect bounds;
    box.getBox(&bounds);
    ALOGD("%d [%f %f %f %f] [%d %d] %s",
        box.countLines(), bounds.fLeft, bounds.fTop, bounds.fRight, bounds.fBottom, info.width(), info.height(), data);

    return true;
}

bool TextDrawer::drawLineAt(SkCanvas &canvas, const char *data, int x, int y) {
    return true;
}

