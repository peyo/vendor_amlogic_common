ifeq ($(BUILD_WITH_WIDEVINECAS),true)
LOCAL_PATH:= $(call my-dir)
WVCAS_PATH_32 := $(TARGET_OUT_VENDOR)/lib/
#####################################################################
# libcasoemcrypto.so
include $(CLEAR_VARS)
LOCAL_MODULE := libcasoemcrypto
LOCAL_MULTILIB := both
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR)/lib
LOCAL_SRC_FILES_arm := $(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)
LOCAL_SHARED_LIBRARIES := libcrypto libcutils libdl liblog libteec libutils libz
include $(BUILD_PREBUILT)
#####################################################################

#####################################################################
# libwvmediacas.so
include $(CLEAR_VARS)
LOCAL_MODULE := libwvmediacas
LOCAL_MULTILIB := both
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE := false
LOCAL_32_BIT_ONLY := true
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR)/lib/mediacas
LOCAL_SRC_FILES_arm := $(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)
LOCAL_SHARED_LIBRARIES := libcrypto libcutils liblog libprotobuf-cpp-lite libutils
include $(BUILD_PREBUILT)
#####################################################################

#####################################################################
# libdec_ca_wvcas.so
include $(CLEAR_VARS)
LOCAL_MODULE := libdec_ca_wvcas
LOCAL_MULTILIB := both
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_PATH_32 := $(WVCAS_PATH_32)
LOCAL_SRC_FILES_arm := $(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)
LOCAL_SHARED_LIBRARIES := libc++ libc libcrypto libcutils libdl libhidlbase liblog libm libssl libstagefright_foundation libutils libz android.hardware.cas.native@1.0 android.hardware.cas@1.0 libteec libmediahal_tsplayer
include $(BUILD_PREBUILT)
#####################################################################

#####################################################################
include $(CLEAR_VARS)
TA_UUID := e043cde0-61d0-11e5-9c26-0002a5d5c5ca
TA_SUFFIX := .ta

ifeq ($(PLATFORM_TDK_VERSION), 38)
PLATFORM_TDK_PATH := $(BOARD_AML_VENDOR_PATH)/tdk_v3
	ifeq ($(filter A311D2 POP1 S905C2 S905C2ENG S905X4 S805X2 S805X2G S905Y4, $(BOARD_AML_SOC_TYPE)),)
		LOCAL_TA := ta/v3/$(TA_UUID)$(TA_SUFFIX)
	else
		LOCAL_TA := ta/v3/dev/$(BOARD_AML_SOC_TYPE)/$(TA_UUID)$(TA_SUFFIX)
	endif
else
PLATFORM_TDK_PATH := $(BOARD_AML_VENDOR_PATH)/tdk
LOCAL_TA := ta/v2/$(TA_UUID)$(TA_SUFFIX)
endif

ifeq ($(TARGET_ENABLE_TA_ENCRYPT), true)
ENCRYPT := 1
else
ENCRYPT := 0
endif

LOCAL_SRC_FILES := $(LOCAL_TA)
LOCAL_MODULE := $(TA_UUID)
LOCAL_MODULE_SUFFIX := $(TA_SUFFIX)
LOCAL_STRIP_MODULE := false
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR)/lib/teetz
ifeq ($(TARGET_ENABLE_TA_SIGN), true)
LOCAL_POST_INSTALL_CMD = $(PLATFORM_TDK_PATH)/ta_export/scripts/sign_ta_auto.py \
		--in=$(shell pwd)/$(LOCAL_MODULE_PATH)/$(TA_UUID)$(LOCAL_MODULE_SUFFIX) \
		--keydir=$(shell pwd)/$(BOARD_AML_TDK_KEY_PATH) \
		--encrypt=$(ENCRYPT)
endif
include $(BUILD_PREBUILT)
#####################################################################

endif
