/*
 * Copyright (c) 2019 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _WVCAS_WRAPPER_H__
#define _WVCAS_WRAPPER_H__

#include <stdio.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include <utils/Trace.h>

#ifdef INVOKE_WVCAS_TA
extern "C" {
#if ((defined PLATFORM_TDK_VERSION) && (PLATFORM_TDK_VERSION >= 38))
#include "stbwrapper_sc2.h"
#endif
}
#endif

//support for MediaPlayer setDataSource with Headers
//#include "drmphal.h"

#ifdef __cplusplus
extern "C" {
#endif

#define WVCAS_ERROR -1
#define WVCAS_SUCCESS 0
#define WV_CA_ID 0x4AD4

#ifdef ATRACE_TAG
#undef ATRACE_TAG
#define ATRACE_TAG ATRACE_TAG_VIDEO
#endif

//#define WVCAS_SAMPLE
//#define PROPERTY_VALUE_MAX 92 //defined in libcutils properties.h

//#define SUPPORT_DETECT_LICENSE_EXPIRED

#define DMX_DEV_NO				0
#define AV_DEV_NO				0
#define DSC_DEV_NO				0
#define AOUT_DEV_NO				0
#define DSC_INIT_INDEX			0x0
#define DSC_INIT_FLAG			0x3
#define MAX_CHAN_COUNT			8
#define VIDEO_CHAN_INDEX		0
#define AUDIO_CHAN_INDEX		1
#define MAX_ECM_PID_NUM			3

#define TS_PACKET_SIZE			188
#define TS_EMPTY_PACKET_PID		0x1FFF
#define TS_PAT_PID				0x0
#define TS_SYNC_BYTE			0x47

#define DBG_LEVEL_L				0x1
#define DBG_LEVEL_M				0x2
#define DBG_LEVEL_H				0x4

#define WVCAS_ERR(number, format, ...) ALOGE("[No-%d][%s:%d] " format, number, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define WVCAS_INFO(number, format, ...) ALOGI("[No-%d][%s:%d] " format, number, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define WVCAS_WRAN(number, format, ...) ALOGW("[No-%d][%s:%d] " format, number, __FUNCTION__, __LINE__, ## __VA_ARGS__)

#define WVCAS_ERR_COMMON(format, ...) ALOGE("[%s:%d] " format, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define WVCAS_INFO_COMMON(format, ...) ALOGI("[%s:%d] " format, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define WVCAS_WRAN_COMMON(format, ...) ALOGW("[%s:%d] " format, __FUNCTION__, __LINE__, ## __VA_ARGS__)

extern uint8_t dbg_level;

#define WVCAS_DBG(flag, number, format, ...) do {\
	if (dbg_level & flag) { \
		ALOGD("[No-%d][%s:%d] " format, number, __FUNCTION__, __LINE__, ## __VA_ARGS__); \
	} \
} while(0)

#define WVCAS_DBG_COMMON(flag, format, ...) do {\
		if (dbg_level & flag) { \
			ALOGD("[%s:%d] " format, __FUNCTION__, __LINE__, ## __VA_ARGS__); \
		} \
	} while(0)

#define WVCAS_TRACE(number) do {\
	if (dbg_level & DBG_LEVEL_M) \
		ALOGI("\033[40;31m [No-%d][%s:%d] \033[0m\n", number, __FUNCTION__, __LINE__); \
} while(0)

#define ChkRet(expr) do { \
		ret = (expr); \
		if (ret) { \
			ALOGE("[%s:%d] error return %x\n", \
				__FUNCTION__, __LINE__, ret); \
			goto ErrorExit; \
		} \
	} while(0);

#if ANDROID_PLATFORM_SDK_VERSION > 28
#define WVCAS_PROP_DBGLEVEL "vendor.media.wvcas.dbglevel"
#define WVCAS_PROP_LICSERVER "vendor.media.wvcas.licserver"
#define WVCAS_PROP_PROXY_VENDOR "vendor.wvcas.proxy.vendor"
#define WVCAS_PROP_CONTENT_TYPE "vendor.wvcas.content.type"
#define WVCAS_PROP_CUSTOMER_DATA "vendor.wvcas.customer.data"
#else
#define WVCAS_PROP_DBGLEVEL "media.wvcas.dbglevel"
#define WVCAS_PROP_LICSERVER "media.wvcas.licserver"
#define WVCAS_PROP_PROXY_VENDOR "wvcas.proxy.vendor"
#define WVCAS_PROP_CONTENT_TYPE "wvcas.content.type"
#define WVCAS_PROP_CUSTOMER_DATA "wvcas.customer.data"
#endif

typedef struct __tagCAS_INIT_HEADERS {
	char *license_url;
	char *provision_url;
	char *request_header;
	char *request_body;
	char *content_type;
} CasInitHeaders;

typedef struct __tagCAS_STREAM_INFO {
	unsigned int		ca_system_id;
	uint16_t			desc_num;
	unsigned int		ecm_pid[MAX_ECM_PID_NUM];
	uint16_t			audio_pid;
	uint16_t			video_pid;
	int					audio_channel;
	int					video_channel;
	bool				av_diff_ecm;
	uint8_t				*private_data;
	unsigned int		pri_data_len;
	CasInitHeaders		*headers;
} CasStreamInfo;

//support for MediaPlayer setDataSource with Headers
int		setup_wvcas_headers(
			void *wvcas_wrapper,
			CasInitHeaders *headers);

int		setup_vendor_info(
			void *wvcas_wrapper);

void	*get_wvcas_wrapper(
			uint32_t idx);

int		init_wvcas(
			void **wvcas_wrapper,
			CasInitHeaders *headers,
			int cas_obj_idx);

int		start_wvcas(
			void *wvcas_wrapper,
			uint8_t **ca_private_data,
			unsigned int len,
			unsigned int av_diff_ecm,
			unsigned int v_ecm_pid,
			unsigned int a_ecm_pid);

int		release_wvcas(
			void *wvcas_wrapper);

void	process_ecm(
			const uint8_t *data,
			int len,
			int fid);

void	process_video_ecm(
			void *wvcas_wrapper,
			const uint8_t *data,
			int len);

void	process_audio_ecm(
			void *wvcas_wrapper,
			const uint8_t *data,
			int len);

int		provision_request(
			void *wvcas_wrapper,
			const char *event_data,
			int size);

int		license_request(
			void *wvcas_wrapper,
			const char *event_data,
			int size,
			bool renew);

int		load_drm_cert(
			void *wvcas_wrapper,
			const char *event_data,
			int size);

int		dsc_dev_setup(
			void *wvcas_wrapper,
			uint32_t dsc_no,
			uint32_t video_id,
			uint32_t audio_id,
			bool av_diff_ecm);

int		dsc_dev_reset(
			void *wvcas_wrapper,
			uint32_t dsc_no);

int		dsc_dev_close(
			void *wvcas_wrapper,
			uint32_t dsc_no);

#ifdef INVOKE_WVCAS_TA
int		ca_dsc_dev_reset(
			void *wvcas_wrapper,
			uint32_t dsc_no,
			uint32_t index,
			uint32_t all);

int		ca_dsc_dev_setup(
			void *wvcas_wrapper,
			uint32_t video_id,
			uint32_t audio_id);

int		ca_dsc_dev_close(
			void *wvcas_wrapper,
			uint32_t dsc_no);
#if ((defined PLATFORM_TDK_VERSION) && (PLATFORM_TDK_VERSION >= 38))
int		open_dsc_dev(
			void *wvcas_dsc_info,
			bool av_diff_ecm,
			int cas_sid);

int		wvcas_start_descrambling(
			void *wvcas_dsc_info,
			int dsc_algo,
			int dsc_type,
			void *wvcas_wrapper);

int		wvcas_stop_descrambling(
			void *wvcas_dsc_info);
#endif
#endif

#ifdef __cplusplus
};
#endif

#endif

