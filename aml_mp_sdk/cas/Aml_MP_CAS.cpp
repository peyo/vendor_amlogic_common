/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_NDEBUG 0
#define LOG_TAG "AmlMpCAS"
#include <Aml_MP/Cas.h>
#include "AmlDvbCasHal.h"
#include <utils/AmlMpHandle.h>
#include <utils/AmlMpUtils.h>

static const char* mName = LOG_TAG;

using namespace aml_mp;
//using namespace android;

///////////////////////////////////////////////////////////////////////////////
//global CAS functions
pthread_once_t g_dvbCasInitFlag = PTHREAD_ONCE_INIT;
CasHandle g_casHandle = 0;

static AM_CA_SECTION convertToCASection(Aml_MP_CASSectionType casSection)
{
    switch (casSection) {
    case AML_MP_CAS_SECTION_PMT:
        return AM_CA_SECTION_PMT;

    case AML_MP_CAS_SECTION_CAT:
        return AM_CA_SECTION_CAT;

    case AML_MP_CAS_SECTION_NIT:
        return AM_CA_SECTION_NIT;
    }
}


int Aml_MP_CAS_Initialize()
{
    MLOG();

#ifdef HAVE_CAS_HAL
    pthread_once(&g_dvbCasInitFlag, [] {
        AM_CA_Init(&g_casHandle);
    });
#endif

    return 0;
}

int Aml_MP_CAS_Terminate()
{
    MLOG();

    AM_RESULT ret = AM_ERROR_GENERAL_ERORR;

#ifdef HAVE_CAS_HAL
    ret = AM_CA_Term(g_casHandle);
#endif

    g_casHandle = 0;

    return ret;
}

int Aml_MP_CAS_IsNeedWholeSection()
{
    int ret = AM_ERROR_GENERAL_ERORR;

#ifdef HAVE_CAS_HAL
    ret = AM_CA_IsNeedWholeSection();
#endif

    MLOG("isNeedWholeSection = %d", ret);
    return ret;
}

int Aml_MP_CAS_IsSystemIdSupported(int caSystemId)
{
    bool ret = false;

#ifdef HAVE_CAS_HAL
    ret = AM_CA_IsSystemIdSupported(caSystemId);
#else
    AML_MP_UNUSED(caSystemId);
#endif

    return ret;
}

int Aml_MP_CAS_ReportSection(Aml_MP_CASSectionReportAttr* pAttr, uint8_t* data, size_t len)
{
    MLOG("dmx_dev:%d, service_id:%d", pAttr->dmxDev, pAttr->serviceId);

    AM_CA_SecAttr_t attr;
    AM_RESULT ret = AM_ERROR_GENERAL_ERORR;

    attr.dmx_dev = pAttr->dmxDev;
    attr.service_id = pAttr->serviceId;
    attr.section_type = convertToCASection(pAttr->sectionType);

#ifdef HAVE_CAS_HAL
    ret = AM_CA_ReportSection(&attr, data, len);
#else
    AML_MP_UNUSED(data);
    AML_MP_UNUSED(len);
#endif

    return ret;
}

int Aml_MP_CAS_SetEmmPid(int dmxDev, uint16_t emmPid)
{
    MLOG("dmxDev:%d, emmPid:%d", dmxDev, emmPid);

    AM_RESULT ret = AM_ERROR_GENERAL_ERORR;
    RETURN_IF(-1, g_casHandle == 0);

#ifdef HAVE_CAS_HAL
    ret = AM_CA_SetEmmPid(g_casHandle, dmxDev, emmPid);
#endif

    return ret;
}

int Aml_MP_CAS_OpenSession(AML_MP_CASSESSION* casSession, Aml_MP_CASServiceType serviceType)
{
    AmlDvbCasHal* dvbCasHal = new AmlDvbCasHal(serviceType);
    dvbCasHal->incStrong(dvbCasHal);

    *casSession = aml_handle_cast(dvbCasHal);

    return 0;
}

int Aml_MP_CAS_CloseSession(AML_MP_CASSESSION casSession)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);
    dvbCasHal->decStrong(casSession);

    return 0;
}

int Aml_MP_CAS_RegisterEventCallback(AML_MP_CASSESSION casSession, Aml_MP_CAS_EventCallback cb, void* userData)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    int ret = 0;

    if (dvbCasHal) {
        ret = dvbCasHal->registerEventCallback(cb, userData);
    } else {
        CAS_EventFunction_t eventFn = reinterpret_cast<CAS_EventFunction_t>(cb);
#ifdef HAVE_CAS_HAL
        ret = AM_CA_RegisterEventCallback((CasSession)nullptr, eventFn);
#else
        AML_MP_UNUSED(eventFn);
#endif
    }

    return ret;
}

int Aml_MP_CAS_StartDescrambling(AML_MP_CASSESSION casSession, Aml_MP_CASServiceInfo* serviceInfo)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->startDescrambling(serviceInfo);
}

int Aml_MP_CAS_StopDescrambling(AML_MP_CASSESSION casSession)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->stopDescrambling();
}

int Aml_MP_CAS_UpdateDescramblingPid(AML_MP_CASSESSION casSession, int oldStreamPid, int newStreamPid)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->updateDescramblingPid(oldStreamPid, newStreamPid);
}

int Aml_MP_CAS_StartDVRRecord(AML_MP_CASSESSION casSession, Aml_MP_CASServiceInfo *serviceInfo)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->startDVRRecord(serviceInfo);
}

int Aml_MP_CAS_StopDVRRecord(AML_MP_CASSESSION casSession)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->stopDVRRecord();
}

int Aml_MP_CAS_StartDVRReplay(AML_MP_CASSESSION casSession, Aml_MP_CASDVRReplayParams *dvrReplayParams)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->startDVRReplay(dvrReplayParams);
}

int Aml_MP_CAS_StopDVRReplay(AML_MP_CASSESSION casSession)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->stopDVRReplay();
}

int Aml_MP_CAS_DVREncrypt(AML_MP_CASSESSION casSession, Aml_MP_CASCryptoParams *cryptoParams)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->DVREncrypt(cryptoParams);
}

int Aml_MP_CAS_DVRDecrypt(AML_MP_CASSESSION casSession, Aml_MP_CASCryptoParams *cryptoParams)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->DVRDecrypt(cryptoParams);
}

AML_MP_SECMEM Aml_MP_CAS_CreateSecmem(AML_MP_CASSESSION casSession, Aml_MP_CASServiceType type, void **pSecbuf, uint32_t *size)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(AML_MP_INVALID_HANDLE, dvbCasHal == nullptr);

    return dvbCasHal->createSecmem(type, pSecbuf, size);
}

int Aml_MP_CAS_DestroySecmem(AML_MP_CASSESSION casSession, AML_MP_SECMEM secMem)
{
    AmlDvbCasHal* dvbCasHal = aml_handle_cast<AmlDvbCasHal>(casSession);
    RETURN_IF(-1, dvbCasHal == nullptr);

    return dvbCasHal->destroySecmem(secMem);
}

