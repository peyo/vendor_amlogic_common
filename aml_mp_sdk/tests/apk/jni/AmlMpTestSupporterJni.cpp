/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_NDEBUG 0
#define LOG_TAG "AmlMpPlayerDemo_TestSupporterJni"
#define mName LOG_TAG

#include "AmlMpTestSupporter.h"
#include "AmlMpTestSupporterJni.h"
#include <utils/AmlMpUtils.h>

#include <utils/Log.h>

using namespace aml_mp;

int Aml_Mp_Test_Player_Create(AML_MP_TEST_PLAYER* handle)
{
    if (handle == nullptr) {
        return -1;
    }
    AmlMpTestSupporter* testSupporter = new AmlMpTestSupporter();
    testSupporter->incStrong(testSupporter);
    *handle = aml_handle_cast(testSupporter);
    return 0;
}


int Aml_Mp_Test_Player_SetDataSource(AML_MP_TEST_PLAYER handle, const std::string& url)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setDataSource(url);
    return 0;
}

int Aml_Mp_Test_Player_Prepare(AML_MP_TEST_PLAYER handle)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->prepare(false);
    return 0;
}

int Aml_Mp_Test_Player_SetVideoWindowSize(AML_MP_TEST_PLAYER handle, int x, int y, int width, int height)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    AmlMpTestSupporter::DisplayParam displayParams = {
        .x = x,
        .y = y,
        .width = width,
        .height = height,
        .videoMode = 1,
    };
    testSupporter->setDisplayParam(displayParams);
    return 0;
}

int Aml_Mp_Test_Player_SetANativeWindow(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    AmlMpTestSupporter::DisplayParam displayParams = {
        .videoMode = 0,
        .aNativeWindow = aNativeWindow,
    };
    testSupporter->setDisplayParam(displayParams);
    return 0;
}

int Aml_Mp_Test_Player_SetSubtitleANativeWindow(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setSubtitleANativeWindow(aNativeWindow);
    return 0;
}

int Aml_Mp_Test_Player_SetSubtitleEnabled(AML_MP_TEST_PLAYER handle, bool enabled)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setSubtitleEnabled(enabled);
    return 0;
}

bool Aml_Mp_Test_Player_GetSubtitleEnabled(AML_MP_TEST_PLAYER handle)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    return testSupporter->getSubtitleEnabled();
}

int Aml_Mp_Test_Player_StartPlay(AML_MP_TEST_PLAYER handle)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->startPlay();
    return 0;
}

int Aml_Mp_Test_Player_SetVolume(AML_MP_TEST_PLAYER handle, float volume)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setVolume(volume);
    return 0;
}

int Aml_Mp_Test_Player_SetMute(AML_MP_TEST_PLAYER handle, bool mute)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setMute(mute);
    return 0;
}

int Aml_Mp_Test_Player_SetRate(AML_MP_TEST_PLAYER handle, float rate)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setRate(rate);
    return 0;
}

int Aml_Mp_Test_Player_Stop(AML_MP_TEST_PLAYER handle)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->stop();
    return 0;
}

bool Aml_Mp_Test_Player_HasVideo(AML_MP_TEST_PLAYER handle)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    return testSupporter->hasVideo();
}

int Aml_Mp_Test_SetOsdBlank(AML_MP_TEST_PLAYER handle, int blank)
{
    AmlMpTestSupporter* testSupporter = aml_handle_cast<AmlMpTestSupporter>(handle);
    RETURN_IF(-1, testSupporter == nullptr);
    testSupporter->setOsdBlank(blank);
    return 0;
}

int Aml_Mp_Test_Player_Destroy(AML_MP_TEST_PLAYER handle)
{
    AmlMpHandle* amlMpHandle = aml_handle_cast<AmlMpHandle>(handle);
    RETURN_IF(-1, amlMpHandle == nullptr);
    amlMpHandle->decStrong(handle);
    return 0;
}
