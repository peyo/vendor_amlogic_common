LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := AmlMpTestJni
LOCAL_MODULE_TAGS := optional
# LOCAL_SYSTEM_EXT_MODULE := true

LOCAL_SRC_FILES := \
	app/src/main/cpp/AmlMpTestPlayerJni.cpp \
	app/src/main/cpp/AmlMpTestPlayerWrapper.cpp

AML_MP_TEST_JNI_C_INC := $(LOCAL_PATH)/app/src/main/cpp/include

AML_MP_TEST_JNI_C_INC_NAMESPACE := \
	$(LOCAL_PATH)/app/src/main/cpp/include_extener \
	$(TOP)/art/libnativeloader/include/nativeloader


AML_MP_TEST_JNI_SHARED_LIBS := \
	libutils \
	libcutils \
	liblog \
	libnativewindow \
	libandroid

ifeq ($(shell expr $(PLATFORM_SDK_VERSION) \>= 30), 1)
	LOCAL_CFLAGS := -DCALL_BY_NAMESPACE
	LOCAL_SHARED_LIBRARIES := libdl_android $(AML_MP_TEST_JNI_SHARED_LIBS)
	LOCAL_C_INCLUDES := $(AML_MP_TEST_JNI_C_INC) $(AML_MP_TEST_JNI_C_INC_NAMESPACE)
else
	LOCAL_SHARED_LIBRARIES := amlMpTestSupporterJni $(AML_MP_TEST_JNI_SHARED_LIBS)
	LOCAL_C_INCLUDES := $(AML_MP_TEST_JNI_C_INC)
endif

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := AmlMpTest
LOCAL_PRIVATE_PLATFORM_APIS := true
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
# LOCAL_SYSTEM_EXT_MODULE := true

LOCAL_SRC_FILES := $(call all-java-files-under, $(LOCAL_PATH)/app/src/main/java)
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/app/src/main/res

LOCAL_STATIC_JAVA_LIBRARIES := \
	androidx-constraintlayout_constraintlayout \
	androidx.appcompat_appcompat \
	com.google.android.material_material

LOCAL_JNI_SHARED_LIBRARIES := AmlMpTestJni


# include $(BUILD_PACKAGE)

##########################################################
#use prebuilt
##########################################################

include $(CLEAR_VARS)
LOCAL_MODULE := AmlMpTest
LOCAL_SRC_FILES := AmlMpTest.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := .apk
LOCAL_CERTIFICATE := platform
#LOCAL_PROPRIETARY_MODULE := true
LOCAL_SYSTEM_EXT_MODULE := true
include $(BUILD_PREBUILT)
