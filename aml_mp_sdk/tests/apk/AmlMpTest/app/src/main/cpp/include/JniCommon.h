//
// Created by Mingshan.Cheng on 2021/3/12.
//

#ifndef AMLMPTEST_JNICOMMON_H
#define AMLMPTEST_JNICOMMON_H

#include <android/log.h>

#define LOG_TAG "AmlCommonPlayer_jni"
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__);
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__);
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);

#endif //AMLMPTEST_JNICOMMON_H
