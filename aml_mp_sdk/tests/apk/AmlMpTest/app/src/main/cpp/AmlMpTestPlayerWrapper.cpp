#include "AmlMpTestPlayerWrapper.h"
#include "include/JniCommon.h"
#include <jni.h>
#include <dlfcn.h>
#include <android/log.h>
#include <cerrno>
#include <android/native_window.h>
#include <string>

#ifdef CALL_BY_NAMESPACE
#include "dlext_namespaces.h"
#endif

#ifdef CALL_BY_NAMESPACE

typedef int(*Aml_Mp_Test_Player_Create_Wrapper)(AML_MP_TEST_PLAYER* handle);
typedef int(*Aml_Mp_Test_Player_SetDataSource_Wrapper)(AML_MP_TEST_PLAYER handle, const std::string& url);
typedef int(*Aml_Mp_Test_Player_Prepare_Wrapper)(AML_MP_TEST_PLAYER handle);
typedef int(*Aml_Mp_Test_Player_SetVideoWindowSize_Wrapper)(AML_MP_TEST_PLAYER handle, int x, int y, int width, int height);
typedef int(*Aml_Mp_Test_Player_SetANativeWindow_Wrapper)(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow);
typedef int(*Aml_Mp_Test_Player_SetSubtitleANativeWindow_Wrapper)(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow);
typedef int(*Aml_Mp_Test_Player_SetSubtitleEnabled_Wrapper)(AML_MP_TEST_PLAYER handle, bool enabled);
typedef bool(*Aml_Mp_Test_Player_GetSubtitleEnabled_Wrapper)(AML_MP_TEST_PLAYER handle);
typedef bool(*Aml_Mp_Test_Player_SetVolume_Wrapper)(AML_MP_TEST_PLAYER handle, float volume);
typedef bool(*Aml_Mp_Test_Player_SetMute_Wrapper)(AML_MP_TEST_PLAYER handle, bool mute);
typedef bool(*Aml_Mp_Test_Player_SetRate_Wrapper)(AML_MP_TEST_PLAYER handle, float rate);
typedef int(*Aml_Mp_Test_Player_StartPlay_Wrapper)(AML_MP_TEST_PLAYER handle);
typedef int(*Aml_Mp_Test_Player_Stop_Wrapper)(AML_MP_TEST_PLAYER handle);
typedef bool(*Aml_Mp_Test_Player_HasVideo_Wrapper)(AML_MP_TEST_PLAYER handle);
typedef int(*Aml_Mp_Test_SetOsdBlank_Wrapper)(AML_MP_TEST_PLAYER handle, int blank);
typedef int(*Aml_Mp_Test_Player_Destroy_Wrapper)(AML_MP_TEST_PLAYER handle);

struct SymbolList {
    Aml_Mp_Test_Player_Create_Wrapper amlMpTestPlayerCreateWrapper;
    Aml_Mp_Test_Player_SetDataSource_Wrapper amlMpTestPlayerSetDataSourceWrapper;
    Aml_Mp_Test_Player_Prepare_Wrapper amlMpTestPlayerPrepareWrapper;
    Aml_Mp_Test_Player_SetVideoWindowSize_Wrapper amlMpTestPlayerSetVideoWindowSizeWrapper;
    Aml_Mp_Test_Player_SetANativeWindow_Wrapper amlMpTestPlayerSetANativeWindowWrapper;
    Aml_Mp_Test_Player_SetSubtitleANativeWindow_Wrapper amlMpTestPlayerSetSubtitleANativeWindowWrapper;
    Aml_Mp_Test_Player_SetSubtitleEnabled_Wrapper amlMpTestPlayerSetSubtitleEnabledWrapper;
    Aml_Mp_Test_Player_GetSubtitleEnabled_Wrapper amlMpTestPlayerGetSubtitleEnabledWrapper;
    Aml_Mp_Test_Player_SetVolume_Wrapper amlMpTestPlayerSetVolumeWrapper;
    Aml_Mp_Test_Player_SetMute_Wrapper amlMpTestPlayerSetMuteWrapper;
    Aml_Mp_Test_Player_SetRate_Wrapper amlMpTestPlayerSetRateWrapper;
    Aml_Mp_Test_Player_StartPlay_Wrapper amlMpTestPlayerStartPlayWrapper;
    Aml_Mp_Test_Player_Stop_Wrapper amlMpTestPlayerStopWrapper;
    Aml_Mp_Test_Player_HasVideo_Wrapper amlMpTestPlayerHasVideoWrapper;
    Aml_Mp_Test_SetOsdBlank_Wrapper amlMpTestSetOsdBlankWrapper;
    Aml_Mp_Test_Player_Destroy_Wrapper amlMpTestPlayerDestroyWrapper;
} static g_symbolList;

struct SymbolOffset {
    const int offset;
    const char* symbol;
} static g_symbolOffsets [] = {
        {offsetof(SymbolList, amlMpTestPlayerCreateWrapper),                    "Aml_Mp_Test_Player_Create"},
        {offsetof(SymbolList, amlMpTestPlayerSetDataSourceWrapper),             "Aml_Mp_Test_Player_SetDataSource"},
        {offsetof(SymbolList, amlMpTestPlayerPrepareWrapper),                   "Aml_Mp_Test_Player_Prepare"},
        {offsetof(SymbolList, amlMpTestPlayerSetVideoWindowSizeWrapper),        "Aml_Mp_Test_Player_SetVideoWindowSize"},
        {offsetof(SymbolList, amlMpTestPlayerSetANativeWindowWrapper),          "Aml_Mp_Test_Player_SetANativeWindow"},
        {offsetof(SymbolList, amlMpTestPlayerSetSubtitleANativeWindowWrapper),  "Aml_Mp_Test_Player_SetSubtitleANativeWindow"},
        {offsetof(SymbolList, amlMpTestPlayerSetSubtitleEnabledWrapper),        "Aml_Mp_Test_Player_SetSubtitleEnabled"},
        {offsetof(SymbolList, amlMpTestPlayerGetSubtitleEnabledWrapper),        "Aml_Mp_Test_Player_GetSubtitleEnabled"},
        {offsetof(SymbolList, amlMpTestPlayerSetVolumeWrapper),                 "Aml_Mp_Test_Player_SetVolume"},
        {offsetof(SymbolList, amlMpTestPlayerSetMuteWrapper),                   "Aml_Mp_Test_Player_SetMute"},
        {offsetof(SymbolList, amlMpTestPlayerSetRateWrapper),                   "Aml_Mp_Test_Player_SetRate"},
        {offsetof(SymbolList, amlMpTestPlayerStartPlayWrapper),                 "Aml_Mp_Test_Player_StartPlay"},
        {offsetof(SymbolList, amlMpTestPlayerStopWrapper),                      "Aml_Mp_Test_Player_Stop"},
        {offsetof(SymbolList, amlMpTestPlayerHasVideoWrapper),                  "Aml_Mp_Test_Player_HasVideo"},
        {offsetof(SymbolList, amlMpTestSetOsdBlankWrapper),                     "Aml_Mp_Test_SetOsdBlank"},
        {offsetof(SymbolList, amlMpTestPlayerDestroyWrapper),                   "Aml_Mp_Test_Player_Destroy"},
};

void* g_libHandle;

#define Aml_Mp_Test_Player_Create(...)                      g_symbolList.amlMpTestPlayerCreateWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetDataSource(...)               g_symbolList.amlMpTestPlayerSetDataSourceWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_Prepare(...)                     g_symbolList.amlMpTestPlayerPrepareWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetVideoWindowSize(...)          g_symbolList.amlMpTestPlayerSetVideoWindowSizeWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetANativeWindow(...)            g_symbolList.amlMpTestPlayerSetANativeWindowWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetSubtitleANativeWindow(...)    g_symbolList.amlMpTestPlayerSetSubtitleANativeWindowWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetSubtitleEnabled(...)          g_symbolList.amlMpTestPlayerSetSubtitleEnabledWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_GetSubtitleEnabled(...)          g_symbolList.amlMpTestPlayerGetSubtitleEnabledWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetVolume(...)                   g_symbolList.amlMpTestPlayerSetVolumeWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetMute(...)                     g_symbolList.amlMpTestPlayerSetMuteWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_SetRate(...)                     g_symbolList.amlMpTestPlayerSetRateWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_StartPlay(...)                   g_symbolList.amlMpTestPlayerStartPlayWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_Stop(...)                        g_symbolList.amlMpTestPlayerStopWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_HasVideo(...)                    g_symbolList.amlMpTestPlayerHasVideoWrapper(__VA_ARGS__)
#define Aml_Mp_Test_SetOsdBlank(...)                        g_symbolList.amlMpTestSetOsdBlankWrapper(__VA_ARGS__)
#define Aml_Mp_Test_Player_Destroy(...)                     g_symbolList.amlMpTestPlayerDestroyWrapper(__VA_ARGS__)

#endif //CALL_BY_NAMESPACE

void AmlMpTestPlayerWrapper::loadLib() {
#ifdef CALL_BY_NAMESPACE
    struct android_namespace_t *ns = android_create_namespace(
            "trustme",
            "/system/lib/",
            "/vendor/lib/",
            ANDROID_NAMESPACE_TYPE_SHARED | ANDROID_NAMESPACE_TYPE_ISOLATED,
            "/system/:/data/:/vendor/",
            nullptr);
    const android_dlextinfo dlextinfo = {
            .flags = ANDROID_DLEXT_USE_NAMESPACE,
            .library_namespace = ns,
    };
    LOGV("before errno:%s", strerror(errno));
    const char* libPath = "/vendor/lib/libamlMpTestSupporterJni.vendor.so";
    g_libHandle = android_dlopen_ext(libPath, RTLD_LOCAL | RTLD_NOW, &dlextinfo);
    LOGV(" handler: %p, errno: %s", g_libHandle, strerror(errno));

    for(const auto& it : g_symbolOffsets){
        *((uint32_t*)((char*)&g_symbolList + it.offset)) = (uint32_t)dlsym(g_libHandle, it.symbol);
    }
    LOGV("load symbol done, libPath: %s", libPath);
#endif
}

bool AmlMpTestPlayerWrapper::create() {
    Aml_Mp_Test_Player_Create(&mPlayerHandle);
    return mPlayerHandle != nullptr;
}

void AmlMpTestPlayerWrapper::setDataSource(const std::string& url) {
    Aml_Mp_Test_Player_SetDataSource(mPlayerHandle, url);
}

void AmlMpTestPlayerWrapper::setVideoWindowSize(int x, int y, int width, int height) {
    Aml_Mp_Test_Player_SetVideoWindowSize(mPlayerHandle, x, y, width, height);
}

void AmlMpTestPlayerWrapper::setANativeWindow(ANativeWindow *aNativeWindow) {
    Aml_Mp_Test_Player_SetANativeWindow(mPlayerHandle, aNativeWindow);
}

void AmlMpTestPlayerWrapper::setSubtitleANativeWindow(ANativeWindow *aNativeWindow) {
    Aml_Mp_Test_Player_SetSubtitleANativeWindow(mPlayerHandle, aNativeWindow);
}

void AmlMpTestPlayerWrapper::prepare() {
    Aml_Mp_Test_Player_Prepare(mPlayerHandle);
}

void AmlMpTestPlayerWrapper::start() {
    Aml_Mp_Test_Player_StartPlay(mPlayerHandle);
}

void AmlMpTestPlayerWrapper::setSubtitleEnabled(bool enabled) {
    Aml_Mp_Test_Player_SetSubtitleEnabled(mPlayerHandle, enabled);
}

bool AmlMpTestPlayerWrapper::getSubtitleEnabled() {
    return Aml_Mp_Test_Player_GetSubtitleEnabled(mPlayerHandle);
}

void AmlMpTestPlayerWrapper::setVolume(float volume) {
    Aml_Mp_Test_Player_SetVolume(mPlayerHandle, volume);
}

void AmlMpTestPlayerWrapper::setMute(bool mute) {
    Aml_Mp_Test_Player_SetMute(mPlayerHandle, mute);
}

void AmlMpTestPlayerWrapper::setRate(float rate) {
    Aml_Mp_Test_Player_SetRate(mPlayerHandle, rate);
}

void AmlMpTestPlayerWrapper::stop() {
    Aml_Mp_Test_Player_Stop(mPlayerHandle);
}

void AmlMpTestPlayerWrapper::destroy() {
    if (mPlayerHandle)
        Aml_Mp_Test_Player_Destroy(mPlayerHandle);
}

void AmlMpTestPlayerWrapper::pause() {

}

void AmlMpTestPlayerWrapper::resume() {

}

void AmlMpTestPlayerWrapper::reset() {

}
