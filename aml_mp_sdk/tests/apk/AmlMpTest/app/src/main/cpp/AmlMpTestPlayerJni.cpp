#include <jni.h>
#include <string>
#include <dlfcn.h>
#include <android/log.h>
#include <cerrno>
#include <android/native_window_jni.h>
#include <asm/fcntl.h>

#include "JniCommon.h"
#include "AmlMpTestPlayerWrapper.h"

struct fields_t {
    jfieldID context;
} static fields;

std::string jstring2string(JNIEnv *env, jstring jStr);

static AmlMpTestPlayerWrapper* getPlayerWrapper(JNIEnv *env, jobject thiz) {
    return (AmlMpTestPlayerWrapper*)env->GetLongField(thiz, fields.context);
}

static void setPlayerWrapper(JNIEnv *env, jobject thiz, AmlMpTestPlayerWrapper* playerWrapper) {
    env->SetLongField(thiz, fields.context, (jlong)playerWrapper);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_nativeSetup(
        JNIEnv *env,
        jclass clazz) {
    AmlMpTestPlayerWrapper::loadLib();
    fields.context = env->GetFieldID(clazz, "mNativeContext", "J");
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_create(
        JNIEnv *env,
        jobject thiz) {
    auto p = new AmlMpTestPlayerWrapper();
    setPlayerWrapper(env, thiz, p);
    p->create();
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setDataSource(
        JNIEnv *env,
        jobject thiz, jstring url) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setDataSource(jstring2string(env, url));
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setVideoWindowSize(
        JNIEnv *env,
        jobject thiz, jint x, jint y, jint width, jint height) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setVideoWindowSize(x, y, width, height);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setSurface(
        JNIEnv *env,
        jobject thiz, jobject jSurface) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    ANativeWindow* aNativeWindow = ANativeWindow_fromSurface(env, jSurface);
    p->setANativeWindow(aNativeWindow);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setSubtitleSurface(
        JNIEnv *env,
        jobject thiz, jobject jSurface) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    ANativeWindow* aNativeWindow = ANativeWindow_fromSurface(env, jSurface);
    LOGI("subtitle nativeWindow:%p", aNativeWindow);
    p->setSubtitleANativeWindow(aNativeWindow);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_prepare(
        JNIEnv *env,
        jobject thiz) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->prepare();
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_start(
        JNIEnv *env,
        jobject thiz) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->start();
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setSubtitleEnabled(
        JNIEnv *env,
        jobject thiz,
        jboolean enabled) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setSubtitleEnabled(enabled);
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_getSubtitleEnabled(
        JNIEnv *env,
        jobject thiz) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return false;
    }
    return p->getSubtitleEnabled();
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setVolume(
        JNIEnv *env,
        jobject thiz,
        jfloat volume) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setVolume(volume);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setMute(
        JNIEnv *env,
        jobject thiz,
        jboolean mute) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setMute(mute);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_setRate(
        JNIEnv *env,
        jobject thiz,
        jfloat rate) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->setRate(rate);
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_stop(
        JNIEnv *env,
        jobject thiz) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->stop();
}

extern "C" JNIEXPORT void JNICALL
Java_com_amlogic_amlmptest_player_AmlMpTestPlayerWrapper_release(
        JNIEnv *env,
        jobject thiz) {
    AmlMpTestPlayerWrapper* p = getPlayerWrapper(env, thiz);
    if(p == nullptr){
        return;
    }
    p->destroy();
    delete p;
}

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    jclass stringClass = env->GetObjectClass(jStr);
    jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    auto stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes,env->NewStringUTF("UTF-8"));

    int length = env->GetArrayLength(stringJbytes);
    jbyte *pBytes = env->GetByteArrayElements(stringJbytes, JNI_FALSE);

    std::string ret = std::string((char *) pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);
    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}
