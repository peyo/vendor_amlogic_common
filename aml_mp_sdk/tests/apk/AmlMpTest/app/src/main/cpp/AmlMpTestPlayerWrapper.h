//
// Created by Mingshan.Cheng on 2021/3/15.
//

#ifndef AMLMPTEST_AMLMPTESTPLAYERWRAPPER_H
#define AMLMPTEST_AMLMPTESTPLAYERWRAPPER_H

#include "AmlMpTestSupporterJni.h"
#include <android/native_window.h>
#include <string>

class AmlMpTestPlayerWrapper {
public:
    static void loadLib();
    bool create();
    void setDataSource(const std::string& url);
    void setVideoWindowSize(int x, int y, int width, int height);
    void setANativeWindow(ANativeWindow* aNativeWindow);
    void setSubtitleANativeWindow(ANativeWindow* aNativeWindow);
    void prepare();
    void start();
    void setSubtitleEnabled(bool enabled);
    bool getSubtitleEnabled();
    void setVolume(float volume);
    void setMute(bool mute);
    void setRate(float rate);
    void pause();
    void resume();
    void stop();
    void reset();
    void destroy();
private:
    AML_MP_TEST_PLAYER mPlayerHandle = nullptr;
};

#endif //AMLMPTEST_AMLMPTESTPLAYERWRAPPER_H
