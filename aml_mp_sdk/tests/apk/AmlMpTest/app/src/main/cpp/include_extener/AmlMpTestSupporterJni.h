/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _AML_MP_TEST_SUPPORTER_JNI_H_
#define _AML_MP_TEST_SUPPORTER_JNI_H_

#include <string>
#include <android/native_window.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef void* AML_MP_TEST_PLAYER;

int Aml_Mp_Test_Player_Create(AML_MP_TEST_PLAYER* handle);

int Aml_Mp_Test_Player_SetDataSource(AML_MP_TEST_PLAYER handle, const std::string& url);

int Aml_Mp_Test_Player_Prepare(AML_MP_TEST_PLAYER handle);

int Aml_Mp_Test_Player_SetVideoWindowSize(AML_MP_TEST_PLAYER handle, int x, int y, int width, int height);

int Aml_Mp_Test_Player_SetANativeWindow(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow);

int Aml_Mp_Test_Player_SetSubtitleANativeWindow(AML_MP_TEST_PLAYER handle, ANativeWindow* aNativeWindow);

int Aml_Mp_Test_Player_SetSubtitleEnabled(AML_MP_TEST_PLAYER handle, bool enabled);

bool Aml_Mp_Test_Player_GetSubtitleEnabled(AML_MP_TEST_PLAYER handle);

int Aml_Mp_Test_Player_SetVolume(AML_MP_TEST_PLAYER handle, float volume);

int Aml_Mp_Test_Player_SetMute(AML_MP_TEST_PLAYER handle, bool mute);

int Aml_Mp_Test_Player_SetRate(AML_MP_TEST_PLAYER handle, float rate);

int Aml_Mp_Test_Player_StartPlay(AML_MP_TEST_PLAYER handle);

int Aml_Mp_Test_Player_Stop(AML_MP_TEST_PLAYER handle);

bool Aml_Mp_Test_Player_HasVideo(AML_MP_TEST_PLAYER handle);

int Aml_Mp_Test_SetOsdBlank(AML_MP_TEST_PLAYER handle, int blank);

int Aml_Mp_Test_Player_Destroy(AML_MP_TEST_PLAYER handle);

#ifdef __cplusplus
}
#endif


#endif //_AML_MP_TEST_SUPPORTER_JNI_H_
