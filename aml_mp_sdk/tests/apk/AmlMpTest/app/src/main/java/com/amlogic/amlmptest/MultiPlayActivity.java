package com.amlogic.amlmptest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.Toast;

import java.util.ArrayList;

import static com.amlogic.amlmptest.MainActivity.SP_SPEED_KEY;

public class MultiPlayActivity extends AppCompatActivity {
    private static final String TAG = "MultiPlayActivity";
    private static String KEY_URL_LIST = "url_list";
    private static String SP_URL_LIST_KEY = "url_list_";
    private FrameLayout mFrameLayout;
    private ArrayList<String> mUrlList;
    private PlayEntryManager mPlayEntryManager;
    private SharedPreferences sp;
    private float initPlayerSpeed = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_multi_play);
        sp = getSharedPreferences(MainActivity.SP_NAME, Context.MODE_PRIVATE);

        mFrameLayout = findViewById(R.id.fl_sv_container);
        AmlMpTestApplication application = (AmlMpTestApplication) getApplication();
        mPlayEntryManager = new PlayEntryManager(this, mFrameLayout, application.getScreenWidth(), application.getScreenHeight());

        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        int playerNum = intent.getIntExtra(MainActivity.KEY_INTENT_PLAYER_NUM, 4);
        initPlayerSpeed = intent.getFloatExtra(MainActivity.KEY_INTENT_PLAYER_SPEED, 1);
        boolean delayStart = false;
        Log.i(TAG, "onCreate: playerNum: " + playerNum);
        if(data != null) {
            mUrlList = data.getStringArrayList(KEY_URL_LIST);
        }
        if(mUrlList != null){
            for(String url : mUrlList){
                PlayEntryManager.PlayEntry playEntry = mPlayEntryManager.addPlayEntry();
                //todo
                playEntry.setDataSource(url);
                playEntry.setRate(initPlayerSpeed);
            }
        } else if(!delayStart) {
            for(int i = 0; i < playerNum; i++){
                PlayEntryManager.PlayEntry playEntry = mPlayEntryManager.addPlayEntry();
                int pos = i;
                String url = sp.getString(SP_URL_LIST_KEY + i, "");
                if(TextUtils.isEmpty(url)){
                    url = sp.getString(MainActivity.SP_URL_KEY, "");
                }
                if(!TextUtils.isEmpty(url)){
                    playEntry.setDataSource(url);
                    if (i == 0) {
                        playEntry.setMute(false);
                    } else {
                        playEntry.setMute(true);
                    }
                    playEntry.createPlayer();
                    playEntry.setRate(initPlayerSpeed);
                    playEntry.startPlayWhenReady();
                }
                playEntry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        chooseFile(playEntry, pos);
                    }
                });
            }
        } else {
            for(int i = 0; i < playerNum; i++){
                mPlayEntryManager.addPlayEntry();
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i < playerNum; i++){
                                PlayEntryManager.PlayEntry playEntry = mPlayEntryManager.getPlayEntryList().get(i);
                                int pos = i;
                                String url = sp.getString(SP_URL_LIST_KEY + i, "");
                                if(TextUtils.isEmpty(url)){
                                    url = sp.getString(MainActivity.SP_URL_KEY, "");
                                }
                                if(!TextUtils.isEmpty(url)){
                                    playEntry.setDataSource(url);
                                    if (i == 0) {
                                        playEntry.setMute(false);
                                    } else {
                                        playEntry.setMute(true);
                                    }
                                    playEntry.createPlayer();
                                    playEntry.setRate(initPlayerSpeed);
                                    playEntry.startPlayWhenReady();
                                }
                                playEntry.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        chooseFile(playEntry, pos);
                                    }
                                });
                            }
                        }
                    });
                }
            }).start();
        }
        for(int i = 0; i < playerNum; i++){
            sp.edit().putFloat(SP_SPEED_KEY + i, initPlayerSpeed).apply();
        }
    }

    private void chooseFile(PlayEntryManager.PlayEntry playEntry, int pos){
        new ChooseFileDialog(this, playEntry, pos).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayEntryManager.removeAllPlayEntry();
    }

    @Override
    protected void onPause() {
        super.onPause();
        for(PlayEntryManager.PlayEntry playEntry : mPlayEntryManager.getPlayEntryList()){
            playEntry.stopPlay();
            playEntry.releasePlayer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        for(PlayEntryManager.PlayEntry playEntry : mPlayEntryManager.getPlayEntryList()){
            playEntry.createPlayer();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                nextSpeed();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void nextSpeed() {
        String[] speeds = getResources().getStringArray(R.array.play_speed);
        int playEntryPos = mPlayEntryManager.getPlayEntryList().indexOf(mPlayEntryManager.getCurrentFocusPlayEntry());
        float speed = sp.getFloat(SP_SPEED_KEY + playEntryPos, 1);
        for(int i = 0; i < speeds.length; i++){
            if(speed == Float.parseFloat(speeds[i])){
                int position = i;
                position++;
                position %= speeds.length;
                sp.edit().putFloat(SP_SPEED_KEY + playEntryPos, Float.parseFloat(speeds[position])).apply();
                mPlayEntryManager.getCurrentFocusPlayEntry().setRate(Float.parseFloat(speeds[position]));

                showToast("player:" + playEntryPos + " setSpeed:" + Float.parseFloat(speeds[position]));
                break;
            }
        }
    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private class ChooseFileDialog extends Dialog {
        private PlayEntryManager.PlayEntry mPlayEntry;
        private EditText etUrl;
        private Button btConfirm;
        private Button btStop;
        private int mPos;
        public ChooseFileDialog(@NonNull Context context, PlayEntryManager.PlayEntry playEntry, int pos) {
            super(context);
            setContentView(R.layout.dialog_choose_file);
            mPos = pos;
            mPlayEntry = playEntry;
            etUrl = findViewById(R.id.et_url);
            etUrl.setInputType(InputType.TYPE_NULL);
            if(!TextUtils.isEmpty(playEntry.getUrl())){
                etUrl.setText(playEntry.getUrl());
            }
            btConfirm = findViewById(R.id.bt_confirm);
            btConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPlayEntry.stopPlay();
                    mPlayEntry.setDataSource(etUrl.getText().toString());
                    mPlayEntry.startPlayWhenReady();
                    String url = sp.getString(MainActivity.SP_URL_KEY, "");
                    if(!url.equals(etUrl.getText().toString())){
                        sp.edit().putString(SP_URL_LIST_KEY + pos, etUrl.getText().toString()).apply();
                    }else{
                        sp.edit().remove(SP_URL_LIST_KEY + pos).apply();
                    }
                    ChooseFileDialog.this.dismiss();
                }
            });
            btStop = findViewById(R.id.bt_stop);
            btStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPlayEntry.stopPlay();
                    ChooseFileDialog.this.dismiss();
                }
            });
        }
    }
}
