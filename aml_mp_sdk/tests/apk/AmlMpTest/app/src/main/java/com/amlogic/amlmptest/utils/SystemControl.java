package com.amlogic.amlmptest.utils;

import android.annotation.SuppressLint;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@SuppressLint("PrivateApi")
public class SystemControl {
    @SuppressWarnings("rawtypes")
    static Class SystemProperties;
    static Method setProp;
    static {
        try {
            SystemProperties = Class.forName("android.os.SystemProperties");
            setProp = SystemProperties.getDeclaredMethod("set", String.class, String.class);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    public static void setProp(String key, String value){
        try {
            setProp.invoke(SystemProperties, key, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
