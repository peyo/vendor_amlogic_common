package com.amlogic.amlmptest;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.constraintlayout.solver.widgets.Rectangle;

import com.amlogic.amlmptest.player.AmlMpTestPlayerWrapper;
import com.amlogic.amlmptest.utils.ULog;

import java.util.ArrayList;

public class PlayEntryManager {
    private static final String TAG = "PlayEntryManager";
    private static boolean isKt = true;

    private Context mContext;
    private FrameLayout mFrameLayout;
    private int width;
    private int height;
    private boolean isPip = true;

    private PlayEntry currentFocusPlayEntry;


    public class PlayEntry {
        private String url;
        private float volume = -1;
        private boolean mute = false;
        private float rate = 1;
        private int instanceId;
        private AmlMpTestPlayerWrapper amlMpTestPlayerWrapper;
        private int preferId;
        private boolean startFlag;
        private boolean waitForSurfaceFlag;
        private View root;
        private LinearLayout ll;
        private SurfaceView surfaceView;
        private SurfaceViewCallBack surfaceViewCallBack;

        private SurfaceView subtitleSurfaceView;

        public PlayEntry(){
        }

        public void setSubtitleSurfaceView(SurfaceView subtitleSurfaceView){
            this.subtitleSurfaceView = subtitleSurfaceView;
        }

        public void setDataSource(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public void setVolume(float volume) {
            this.volume = volume;
        }

        public void setMute(boolean mute) {
            this.mute = mute;
        }

        public void setRate(float rate) {
            this.rate = rate;
            if (startFlag && !waitForSurfaceFlag) {
                ULog.i(TAG, "setRate " + rate);
                amlMpTestPlayerWrapper.setRate(rate);
            }
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            ll.setFocusable(true);
            ll.setBackgroundResource(R.drawable.sv_video_selector);
            int padding = 10;
            ll.setPadding(padding, padding, padding, padding);
            if (onClickListener != null) {
                ll.setOnClickListener(onClickListener);
            }
            ll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        currentFocusPlayEntry = PlayEntry.this;
                        mute = false;
                    } else {
                        mute = true;
                    }
                    if (startFlag && !waitForSurfaceFlag) {
                        ULog.i(TAG, "setMute " + mute);
                        amlMpTestPlayerWrapper.setMute(mute);
                    }
                }
            });
        }

        public AmlMpTestPlayerWrapper createPlayer() {
            if(instanceId < 0){
                instanceId = PlayerManger.requestPlayer(preferId);
                amlMpTestPlayerWrapper = PlayerManger.getPlayerInstance(instanceId);
            }
            return amlMpTestPlayerWrapper;
        }

        public void startPlayWhenReady() {
            if (startFlag || TextUtils.isEmpty(url) || instanceId < 0) {
                return;
            }
            startFlag = true;
            if(!surfaceView.getHolder().getSurface().isValid()){
                ULog.i(TAG, "wait for surface ready");
                waitForSurfaceFlag = true;
                return;
            }
            start();
        }

        private void start() {
            ULog.i(TAG, "start: " + this.toString());
            amlMpTestPlayerWrapper.create();
            amlMpTestPlayerWrapper.setDataSource(url + "?demuxid=" + instanceId + "&sourceid=" + instanceId + "&middleware=" + (isKt ? "kt" : "null"));
            amlMpTestPlayerWrapper.setSurface(surfaceView.getHolder().getSurface());
            if(subtitleSurfaceView != null) {
                amlMpTestPlayerWrapper.setSubtitleSurface(subtitleSurfaceView.getHolder().getSurface());
            }
            amlMpTestPlayerWrapper.prepare();
            if (mute) {
                ULog.i(TAG, "before start, setMute " + mute);
                amlMpTestPlayerWrapper.setMute(mute);
            }
            if (rate != 1) {
                ULog.i(TAG, "before start, setRate " + rate);
                amlMpTestPlayerWrapper.setRate(rate);
            }
            amlMpTestPlayerWrapper.start();
        }

        public void setSubtitleEnable(boolean enabled) {
            if (!startFlag) {
                return;
            }
            amlMpTestPlayerWrapper.setSubtitleEnabled(enabled);
        }

        public boolean getSubtitleEnabled() {
            if (!startFlag) {
                return false;
            }
            return amlMpTestPlayerWrapper.getSubtitleEnabled();
        }

        public void stopPlay() {
            if (!startFlag) {
                return;
            }
            startFlag = false;
            if(waitForSurfaceFlag){
                waitForSurfaceFlag = false;
                return;
            }
            stop();
        }

        private void stop() {
            ULog.i(TAG, "stop: " + this.toString());
            amlMpTestPlayerWrapper.stop();
            amlMpTestPlayerWrapper.release();
        }

        public void releasePlayer() {
            if(instanceId < 0){
                return;
            }
            ULog.i(TAG, "releasePlayer" + this.toString());
            PlayerManger.releasePlayer(instanceId);
            amlMpTestPlayerWrapper = null;
            instanceId = -1;
        }

        @Override
        public String toString() {
            return "PlayEntry{" +
                    "startFlag=" + startFlag +
                    ", waitForSurfaceFlag=" + waitForSurfaceFlag +
                    ", instanceId=" + instanceId +
                    ", ll=" + getIdentityString(ll) +
                    ", surfaceView=" + getIdentityString(surfaceView) +
                    ", surfaceViewCallBack=" + getIdentityString(surfaceViewCallBack) +
                    ", url='" + url + '\'' +
                    '}';
        }

        private String getIdentityString(Object obj){
            return "0x" + Integer.toHexString(System.identityHashCode(obj));
        }
    }

    private final ArrayList<PlayEntry> playEntryList = new ArrayList<>();

    public PlayEntryManager(@NonNull Context context, FrameLayout frameLayout, int width, int height) {
        mContext = context;
        mFrameLayout = frameLayout;
        this.width = width;
        this.height = height;
    }

    public static void setIsKt(boolean isKt){
        PlayEntryManager.isKt = isKt;
    }

    public PlayEntry addPlayEntry() {
        return addPlayEntry(0);
    }

    public PlayEntry addPlayEntry(int preferId) {
        PlayEntry playEntry = new PlayEntry();
        playEntry.preferId = preferId;

        View v = LayoutInflater.from(mContext).inflate(R.layout.play_entry, mFrameLayout, false);
        LinearLayout ll = v.findViewById(R.id.ll_play_entry);
        int id = View.generateViewId();
        ULog.i(TAG, "generateViewId: " + id);
        ll.setId(id);
        ll.setTag(playEntry);
        SurfaceView sv = v.findViewById(R.id.sv_play_window);
        SurfaceViewCallBack callBack = new SurfaceViewCallBack(playEntry);
        sv.getHolder().addCallback(callBack);
        mFrameLayout.addView(v);
        playEntry.root = v;

        playEntryList.add(playEntry);
        if(playEntryList.size() == 1){
            currentFocusPlayEntry = playEntry;
        }
        playEntry.instanceId = -1;
        playEntry.ll = ll;
        playEntry.surfaceView = sv;
        playEntry.surfaceViewCallBack = callBack;

        reLayoutSurfaceView();
        return playEntry;
    }

    public PlayEntry getCurrentFocusPlayEntry() {
        return currentFocusPlayEntry;
    }

    public void removePlayEntry(PlayEntry playEntry) {
        playEntry.stopPlay();
        playEntry.releasePlayer();

        SurfaceView sv = playEntry.surfaceView;
//        sv.getHolder().removeCallback(playEntry.surfaceViewCallBack);
        mFrameLayout.removeView(playEntry.root);
        playEntryList.remove(playEntry);
        reLayoutSurfaceView();
    }

    public void removeAllPlayEntry() {
        for (PlayEntry playEntry : playEntryList) {
            playEntry.stopPlay();
            playEntry.releasePlayer();
            SurfaceView sv = playEntry.surfaceView;
//            sv.getHolder().removeCallback(playEntry.surfaceViewCallBack);
//            mGridLayout.removeView(playEntry.ll);
        }
        mFrameLayout.removeAllViews();
        playEntryList.clear();
    }

    public void stopAllPlayEntry() {
        for (PlayEntry playEntry : playEntryList) {
            playEntry.stopPlay();
            playEntry.releasePlayer();
        }
    }

    public void startAllPlayEntry() {
        for (PlayEntry playEntry : playEntryList) {
            playEntry.createPlayer();
            playEntry.startPlayWhenReady();
        }
    }

    public ArrayList<PlayEntry> getPlayEntryList(){
        return playEntryList;
    }

    private void reLayoutSurfaceView() {
        int column;
        int row;
        int playerCount = playEntryList.size();
        if (playerCount == 1) {
            //1x1
            column = 1;
            row = (playerCount - 1) / column + 1;
        } else if (playerCount <= 2) {
            //1x2
            column = 2;
            row = (playerCount - 1) / column + 1;
        } else if (playerCount <= 4) {
            //2x2
            column = 2;
            row = (playerCount - 1) / column + 1;
        } else {
            //3x3
            column = 3;
            row = (playerCount - 1) / column + 1;
        }
        Rectangle[] rectangle = new Rectangle[playerCount];
        for(int i = 0; i < rectangle.length; i++){
            rectangle[i] = new Rectangle();
            rectangle[i].height = height / row;
            rectangle[i].width = width / column;
            rectangle[i].x = (i % column) * rectangle[i].width;
            rectangle[i].y = (i / column) * rectangle[i].height;
        }


        if(isPip && playerCount == 2){
            //use for pip layout
            rectangle[0].height = height;
            rectangle[0].width = width;
            rectangle[0].x = 0;
            rectangle[0].y = 0;

            rectangle[1].height = height / 3;
            rectangle[1].width = width / 3;
            rectangle[1].x = width - (width / 3) - 20;
            rectangle[1].y = height - (height / 3) - 20;
        }

        for (int i = 0; i < playerCount; i++) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.width = rectangle[i].width;
            layoutParams.height = rectangle[i].height;
            layoutParams.setMargins(rectangle[i].x, rectangle[i].y, 0, 0);
            playEntryList.get(i).ll.setLayoutParams(layoutParams);
        }

        if(isPip && playerCount == 2){
            int id0 = playEntryList.get(0).ll.getId();
            int id1 = playEntryList.get(1).ll.getId();
            playEntryList.get(0).ll.setNextFocusDownId(id1);
            playEntryList.get(0).ll.setNextFocusRightId(id1);
            playEntryList.get(1).ll.setNextFocusUpId(id0);
            playEntryList.get(1).ll.setNextFocusLeftId(id0);
        } else if (playerCount > 2) {
            playEntryList.get(0).ll.setNextFocusDownId(View.NO_ID);
            playEntryList.get(0).ll.setNextFocusRightId(View.NO_ID);
            playEntryList.get(1).ll.setNextFocusUpId(View.NO_ID);
            playEntryList.get(1).ll.setNextFocusLeftId(View.NO_ID);
        }
    }

    private static class SurfaceViewCallBack implements SurfaceHolder.Callback {
        private final PlayEntry mPlayEntry;

        public SurfaceViewCallBack(PlayEntry playEntry) {
            mPlayEntry = playEntry;
        }

        @Override
        public void surfaceCreated(@NonNull SurfaceHolder holder) {
            ULog.i(TAG, "surfaceCreated: mPlayEntry:" + mPlayEntry);
            if(mPlayEntry.startFlag && mPlayEntry.waitForSurfaceFlag){
                mPlayEntry.createPlayer();
                mPlayEntry.start();
                mPlayEntry.waitForSurfaceFlag = false;
            }
        }

        @Override
        public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
        }

        @Override
        public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
            ULog.i(TAG, "surfaceDestroyed: mPlayEntry:" + mPlayEntry);
            mPlayEntry.stopPlay();
            mPlayEntry.releasePlayer();
        }
    }

}
