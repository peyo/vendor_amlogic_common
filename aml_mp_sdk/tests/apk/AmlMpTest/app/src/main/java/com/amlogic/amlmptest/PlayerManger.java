package com.amlogic.amlmptest;

import com.amlogic.amlmptest.player.AmlMpTestPlayerWrapper;
import com.amlogic.amlmptest.utils.ULog;

public class PlayerManger {
    private static final String TAG = "PlayerManger";
    private static final int MAX_INSTANCES = 9;
    private static final boolean[] playerActive = new boolean[MAX_INSTANCES];
    private static final AmlMpTestPlayerWrapper[] amlMpTestPlayerWrappers = new AmlMpTestPlayerWrapper[MAX_INSTANCES];

    public static synchronized int requestPlayer() {
        int availableInstanceId = 0;
        for (; availableInstanceId < MAX_INSTANCES; availableInstanceId++) {
            if (!playerActive[availableInstanceId]) {
                break;
            }
        }
        if (availableInstanceId >= MAX_INSTANCES) {
            ULog.i(TAG, "request player instanceId: -1, failed");
            return -1;
        }
        playerActive[availableInstanceId] = true;
        amlMpTestPlayerWrappers[availableInstanceId] = new AmlMpTestPlayerWrapper();
        ULog.i(TAG, "request player instanceId: " + availableInstanceId);
        return availableInstanceId;
    }

    public static synchronized int requestPlayer(int preferId) {
        if (preferId >= 0 && preferId < MAX_INSTANCES) {
            if(!playerActive[preferId]){
                playerActive[preferId] = true;
                amlMpTestPlayerWrappers[preferId] = new AmlMpTestPlayerWrapper();
                ULog.i(TAG, "request player instanceId: " + preferId);
                return preferId;
            }
        }
        return requestPlayer();
    }

    public static synchronized AmlMpTestPlayerWrapper getPlayerInstance(int instanceId) {
        if(instanceId < 0 || instanceId > MAX_INSTANCES){
            return null;
        }
        if (!playerActive[instanceId]) {
            return null;
        }
        return amlMpTestPlayerWrappers[instanceId];
    }

    public static synchronized void releasePlayer(int instanceId) {
        if(instanceId < 0 || instanceId > MAX_INSTANCES){
            return;
        }
        ULog.i(TAG, "release player instanceId: " + instanceId);
        playerActive[instanceId] = false;
        amlMpTestPlayerWrappers[instanceId] = null;
    }
}
