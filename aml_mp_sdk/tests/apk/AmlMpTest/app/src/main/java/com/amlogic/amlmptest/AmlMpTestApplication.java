package com.amlogic.amlmptest;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.amlogic.amlmptest.utils.ULog;

public class AmlMpTestApplication extends Application {
    private static final String TAG = "AmlMpTestApplication";
    private int screenWidth;
    private int screenHeight;
    @Override
    public void onCreate() {
        super.onCreate();
        getScreenSize();
    }
    private void getScreenSize(){
        WindowManager wm =
                (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        ULog.i(TAG, "screenWidth:" + screenWidth + ", screenHeight:" + screenHeight);
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }
}
