package com.amlogic.amlmptest.player;

import android.view.Surface;

public class AmlMpTestPlayerWrapper {
    static {
        System.loadLibrary("AmlMpTestJni");
        nativeSetup();
    }


    public native void create();
    public native void setDataSource(String url);
    public native void setVideoWindowSize(int x, int y, int width, int height);
    public native void setSurface(Surface surface);
    public native void setSubtitleSurface(Surface surface);
    public native void prepare();
    public native void start();
    public native void setSubtitleEnabled(boolean enabled);
    public native boolean getSubtitleEnabled();
    public native void setVolume(float volume);
    public native void setMute(boolean mute);
    public native void setRate(float rate);
    public native void stop();
    public native void release();

    private static native void nativeSetup();
    private long mNativeContext;
}
