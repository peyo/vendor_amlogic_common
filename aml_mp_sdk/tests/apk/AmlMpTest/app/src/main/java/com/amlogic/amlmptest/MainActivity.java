package com.amlogic.amlmptest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.amlogic.amlmptest.utils.SystemControl;
import com.amlogic.amlmptest.utils.ULog;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "AmlMpTestJni";
    public static final String SP_NAME = "AmlMpTest";
    public static final String SP_URL_KEY = "url";
    public static final String SP_CB_IS_KT = "is_kt";
    public static final String SP_MULTI_PLAY_NUM = "multiPlayNum";
    public static final String SP_SPEED_KEY = "speed";
    public static final String KEY_INTENT_PLAYER_NUM = "multiPlayerNum";
    public static final String KEY_INTENT_PLAYER_SPEED = "multiPlayerSpeed";
    private SharedPreferences sp;
    private PlayEntryManager.PlayEntry currentFocusPlayEntry;
    private FrameLayout mFrameLayout;
    ConstraintLayout clMenu;
    private EditText etUrl;
    private Button btPlay;
    private Button btStop;
    private PlayEntryManager mPlayEntryManager;
    private Button btMultiPlay;
    private CheckBox cbIsKt;
    private Spinner spPlayerNum;
    private Spinner spPlayerSpeed;
    private Switch swSubtitle;
    private int speedSetCount = 0;

    private SurfaceView subtitleSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        sp = getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);

        mFrameLayout = findViewById(R.id.fl_sv_container);
        AmlMpTestApplication application = (AmlMpTestApplication) getApplication();
        mPlayEntryManager = new PlayEntryManager(this, mFrameLayout, application.getScreenWidth(), application.getScreenHeight());

        cbIsKt = findViewById(R.id.cb_is_kt);
        boolean isKt = sp.getBoolean(SP_CB_IS_KT, true);
        cbIsKt.setChecked(isKt);
        PlayEntryManager.setIsKt(isKt);
        cbIsKt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sp.edit().putBoolean(SP_CB_IS_KT, isChecked).apply();
                PlayEntryManager.setIsKt(isChecked);
            }
        });

        etUrl = findViewById(R.id.et_url);
        etUrl.setInputType(InputType.TYPE_NULL);

        String url = sp.getString(SP_URL_KEY, "");
        etUrl.setText(url);
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sp.edit().putString(SP_URL_KEY, s.toString()).apply();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btPlay = findViewById(R.id.bt_play);
        btPlay.setEnabled(true);
        btPlay.setOnClickListener(v -> {
            currentFocusPlayEntry.setDataSource(etUrl.getText().toString());
            if (TextUtils.isEmpty(currentFocusPlayEntry.getUrl())) {
                return;
            }
            sp.edit().putString(SP_URL_KEY, currentFocusPlayEntry.getUrl()).apply();
            if(swSubtitle.isChecked()){
                currentFocusPlayEntry.setSubtitleSurfaceView(subtitleSurfaceView);
            } else {
                currentFocusPlayEntry.setSubtitleSurfaceView(null);
            }
            currentFocusPlayEntry.setMute(false);
            float speed = sp.getFloat(SP_SPEED_KEY, 1);
            currentFocusPlayEntry.setRate(speed);
            currentFocusPlayEntry.startPlayWhenReady();
            btStop.setEnabled(true);
            btPlay.setEnabled(false);
            etUrl.setFocusable(false);
            btStop.requestFocus();
            btMultiPlay.setEnabled(false);
            swSubtitle.setEnabled(false);
        });
        btStop = findViewById(R.id.bt_stop);
        btStop.setEnabled(false);
        btStop.setOnClickListener(v -> {
            currentFocusPlayEntry.stopPlay();
            btStop.setEnabled(false);
            btPlay.setEnabled(true);
            etUrl.setFocusable(true);
            btPlay.requestFocus();
            btMultiPlay.setEnabled(true);
            swSubtitle.setEnabled(true);
        });
        btMultiPlay = findViewById(R.id.bt_multiPlay);
        btMultiPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MultiPlayActivity.class);
                intent.putExtra(KEY_INTENT_PLAYER_NUM, spPlayerNum.getSelectedItemPosition() + 2);
                String[] speeds = getResources().getStringArray(R.array.play_speed);
//                intent.putExtra(KEY_INTENT_PLAYER_SPEED, Float.parseFloat(speeds[spPlayerSpeed.getSelectedItemPosition()]));
                intent.putExtra(KEY_INTENT_PLAYER_SPEED, 1);
                startActivity(intent);
            }
        });

        spPlayerSpeed = findViewById(R.id.sp_player_speed);
        spPlayerSpeed.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ULog.i(TAG, "onItemSelected: " + position);
                if(speedSetCount > 0){
                    speedSetCount--;
                    return;
                }
                String[] speeds = getResources().getStringArray(R.array.play_speed);
                sp.edit().putFloat(SP_SPEED_KEY, Float.parseFloat(speeds[position])).apply();
                currentFocusPlayEntry.setRate(Float.parseFloat(speeds[position]));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        clMenu = findViewById(R.id.ll_menu);
        currentFocusPlayEntry = mPlayEntryManager.addPlayEntry();
//        currentFocusPlayEntry.setUrl(url);
        spPlayerNum = findViewById(R.id.sp_player_num);
        int playerNum = sp.getInt(SP_MULTI_PLAY_NUM, 4);
        spPlayerNum.setSelection(playerNum - 2);
        spPlayerNum.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ULog.i(TAG, "onItemSelected: " + position);
                sp.edit().putInt(SP_MULTI_PLAY_NUM, position + 2).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        subtitleSurfaceView = findViewById(R.id.sv_subtitle);
        subtitleSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                ULog.i(TAG, "subtitle surfaceView created!");
//                currentFocusPlayEntry.setSubtitleSurfaceView(subtitleSurfaceView);
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

            }
        });
        subtitleSurfaceView.getHolder().setFormat(PixelFormat.RGBA_8888);
        //subtitleSurfaceView.setZOrderMediaOverlay(true);


        swSubtitle = findViewById(R.id.sw_subtitle);
        swSubtitle.setChecked(false);
        swSubtitle.setEnabled(true);
        subtitleSurfaceView.setVisibility(View.GONE);
        swSubtitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    subtitleSurfaceView.setVisibility(View.VISIBLE);
                } else {
                    subtitleSurfaceView.setVisibility(View.GONE);
                }
//                currentFocusPlayEntry.setSubtitleEnable(isChecked);
            }
        });
    }

    private void hideMenu() {
        clMenu.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        currentFocusPlayEntry.stopPlay();
        currentFocusPlayEntry.releasePlayer();
        currentFocusPlayEntry.setDataSource(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] speeds = getResources().getStringArray(R.array.play_speed);
        float speed = sp.getFloat(SP_SPEED_KEY, 1);
        for(int i = 0; i < speeds.length; i++){
            if(Float.parseFloat(speeds[i]) == speed){
                spPlayerSpeed.setSelection(i);
                break;
            }
        }
        currentFocusPlayEntry.createPlayer();
    }

    private void showMenu() {
        clMenu.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_CENTER:
                showMenu();
                break;
            case KeyEvent.KEYCODE_BACK:
                if(clMenu.getVisibility() == View.VISIBLE){
                    hideMenu();
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                nextSpeed();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void nextSpeed() {
        String[] speeds = getResources().getStringArray(R.array.play_speed);
        float speed = sp.getFloat(SP_SPEED_KEY, 1);
        for(int i = 0; i < speeds.length; i++){
            if(speed == Float.parseFloat(speeds[i])){
                int position = i;
                position++;
                position %= speeds.length;
                speedSetCount++;
                sp.edit().putFloat(SP_SPEED_KEY, Float.parseFloat(speeds[position])).apply();
                spPlayerSpeed.setSelection(position);
                currentFocusPlayEntry.setRate(Float.parseFloat(speeds[position]));
                showToast("setSpeed: " + Float.parseFloat(speeds[position]));
                break;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayEntryManager.stopAllPlayEntry();
    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
