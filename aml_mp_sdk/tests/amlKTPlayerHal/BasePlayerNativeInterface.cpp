/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */


#define LOG_NDEBUG 0
#define LOG_TAG "BasePlayerNativeIF"

#include <BasePlayerNativeInterface.h>
#include "AmlPlayerManager.h"

//using namespace aml_mp;

#define VALIDCHECK(x)	(1)

bool isValidPlyaerId(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( (handleId >= BPNI_PLAYER_HANDLE_ID_MAIN) && (handleId <= BPNI_PLAYER_HANDLE_ID_SUB3))
		return true;
	else
		return false;
}

/**
 * register event callback
 *
 * @param[in] handleId handle ID.
 * @param[in] callback function.
  */
void bpni_registerPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback)
{
	if ( nullptr != callback )
		AmlPlayerManager::instance().registerPlayerEventCallback(handleId, callback);
}


/*** AML NOTE: tunerId usage (normally use this way, but not always, there will be exceptional case)
 *	1-channel playback: tunerId = 1
 *  2-Channel PIP :		tunerId = 1, 2
 *  4-Channel Multiview :	tunerId = 1, 3, 4, 5
 ***/
/**
 * Set TunerID(or Demux Id), TS stream data will feed this Tuner(or Demux)
 * And CAS will use this Tuner ID
 * @param[in] handleId handle ID.
 * @param[in] tunerId tuner ID.
 */
void bpni_setTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId)
{
	if ( isValidPlyaerId(handleId) )
	{
		AmlPlayerManager::instance().setTunerId(handleId, tunerId);
	}
}

/**
 * Return Tuner Id(or demux Id)
 *
 * @param[in] handleId handle ID.
 * @return Tuner ID. if empty, then return "-1" (if not set before, or source were some kind of URLs)
 */
int bpni_getTunerId(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getTunerId(handleId);
	}
	return -1;
}


/**
 * Feed TS stream data for Live Channel, VoD to given player
 * @param[in] handleId handle ID.
 * @param[in] data TS stream data.
 * @param[in] len data length.
 */
void bpni_feedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().feedData(handleId, data, len);
	}
}

/**
 * Set ANativeWindow (came from upper JAVA layer's Surface object) for setting video surface
 *
 * 		NOTE: Java layer will use this API after conversion by ANativeWindow_fromSurface() from jobject surface to ANativeWindow
 *		conversion: ANativeWindow* ANativeWindow_fromSurface(JNIEnv* env, jobject surface)
 *
 * @param[in] handleId handle ID.
 * @param[in] pointer of native window surface for video
 */
void bpni_setSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	if ( isValidPlyaerId(handleId) )
	{
		AmlPlayerManager::instance().setSurfaceNativeWindow(handleId, window);
	}
}

/**
 *
 * @param[in] handleId handle ID.
 * @return return Player's pointer of ANativeWindow
 */
ANativeWindow* bpni_getSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getSurfaceNativeWindow(handleId);
	}
	return nullptr;
}



/*** AML NOTE:
 *	bpni_prepare is almost same as CreatePlayer
 ***/
/**
 * Prepare playback.
 * blocks until MediaPlayer is ready for playback
 *
 * Note : calling sequence - this function would be called after setTunerId() and setSurfaceNativeWindow()
 *
 * @param[in] handleId handle ID.
 * @return 0 Success. -1 fail.
 */
int bpni_prepare(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().prepare(handleId);
	}
	return -1;
}

/**
 * Play A/V with given a-v PID
 * <br> this API can be called multiple times without calling stop()
 * <br> i.e.) playStream() -> stop() -> playStream()
 * <br> ex 1) playStream(8006, 8005) -> playStream(8007, 8005)  : change audio language or change audio track
 * <br> ex 2) playStream(8006, 8005) -> playSteram(0, 8005) : normal AV play -> play video ONLY
 * <br> ex 3) playStream(0, 8005) -> playSteram(8006, 8005) : play video ONLY -> player both A/V
 * <br> ex 4) playStream(8006, 8005) -> playStream(0, 0) : same as stop(), no A/V playing
 *
 * @param[in] handleId handle ID.
 * @param[in] audiPID - audio PID. "0" means NO audio or stop audio (audio decoder stop)
 * @param[in] videoPID - video PID. "0" means NO video or stop video (video decoder stop)
 *
 * @return 0 Success. -1 fail.
 */
int bpni_playStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().playStream(handleId, audiPID, videoPID);
	}
	return -1;
}

/**
 * Stop A/V.
 * Stop selective Audio or Video. (choiceable)
 *
 * @param[in] handleId handle ID.
 * @param[in] isAudioStop if true,  audio stop
 * @param[in] isVideoStop if true, video stop
 * @return 0 Success, -1 fail.
 */
int bpni_stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().stop(handleId, isAudioStop, isVideoStop);
	}
	return -1;
}



/**
 * set volume control
 *
 * @param[in] handleId handle ID.
 * @param[in] volume value: range = 0.0f ~ 1.0f, step => 0.01
 * @return 0 Success. -1 fail.
 */
int bpni_setVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().setVolume(handleId, volume);
	}
	return -1;
}

/**
 * return current volume level
 *
 * @param[in] handleId handle ID.
 * @return current volume level. If not available or fail, return -1.0f.
 */
float bpni_getVolume(BPNI_PLAYER_HANDLE_ID handleId)
{

	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getVolume(handleId );
	}
	return -1.0f;

}

int bpni_setAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().setAudioEnabled(handleId, enabled );
	}
	return -1;
}


bool bpni_getAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getAudioEnabled(handleId);
	}
	return false;
}



/**
 * Configure audio PID for Bluetooth device
 *
 * @param[in] handleId handle ID.
 * @param[in] audioPid audio PID.
 * @return success: 0,  fail: -1
 */
int bpni_setBTAudioOutputPid(BPNI_PLAYER_HANDLE_ID handleId, int audioPid)
{

	//TODO:::: *****


	return -1;
}

/**
 * Set trick playback rate
 * In case of normal trick (NOT 0.8x, 1.2x, 1.5x), flush remained buffer
 *
 * @param[in] handleId handle ID.
 * @param rate  : playback rate
 * @return 0 Success. -1 fail.
 */
int bpni_setRate(BPNI_PLAYER_HANDLE_ID handleId, float rate)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().setRate(handleId, rate );
	}

	return -1;
}

/**
 * return current playback rate
 *
 * @param[in] handleId handle ID.
 * @return current playback rate. If not playing, return 0f.
 */
float bpni_getRate(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getRate(handleId);
	}

	return -1;
}

/**
 * Return PTS
 *
 * @param[in] handleId handle ID.
 * @return PTS. fail: -1 return.
 */
double bpni_getPts(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getPts(handleId);
	}
	return -1;
}

/**
 *  Flush stream data buffer inside of Player
 *
 * @param[in] handleId handle ID.
 */
void bpni_flush(BPNI_PLAYER_HANDLE_ID handleId)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().flush(handleId);
	}
}

/**
 * Set ANativeWindow surface for drawing ClosedCaption
 *
 * @param[in] handleId handle ID.
 * @param[in] window pointer of ANativeWindow surface for CC
 */
void bpni_setSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	//TODO
	ALOGI("bpni_setSubtitleSurfaceNativeWindow: window:%p", window);
}

/**
 * Subtitle (Closed Caption / VoD SMI subtitle)  On / Off
 *
 * @param[in] handleId handle ID.
 * @param[in] enabled true = On , false = Off
 */
void bpni_setSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	//TODO
	ALOGI("bpni_setSubtitleEnabled: ", enabled);
}

/**
 * Return Subtitle On or Off status
 *
 * @param[in] handleId handle ID.
 * @return On / Off 여부
 */
bool bpni_getSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	//TODO
	ALOGI("bpni_getSubtitleEnabled: ");
	return false;
}


/**
 * Set SMI URL for VoD
 *
 * @param[in] handleId handle ID.
 * @param[in] url SMI subtitle URL
 */
void bpni_setSubtitleSmiUrl(BPNI_PLAYER_HANDLE_ID handleId, char* url)
{
	//TODO

}

/**
 * Set raw PMT table
 *
 * @param[in] handleId handle ID.
 * @param[in] pmtData PMT data.
 * @param[in] len PMT data's length
 */
void bpni_setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len)
{
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().setPmt(handleId, pmtData, len);
	}
}

/**
 * Get A/V meta data
 *
 * @param[in] handleId handle ID.
 * @return BaseMediaMetaData
 */
BPNI_MediaMetaData bpni_getMetaData(BPNI_PLAYER_HANDLE_ID handleId)
{
	BPNI_MediaMetaData meta;
	if ( isValidPlyaerId(handleId) )
	{
		return AmlPlayerManager::instance().getMetaData(handleId);
	}

	return meta;
}


/**
  * Register BPNI_MDStatusCallback
  *
  * @param[in] handleId handle ID.
  * @param[in] callback
  */
void bpni_cas_registerMDStatusCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback)
{



}


/**
 * This function would be called every starting/ending session for Live & VoD playbak.
 * This indicates CAS plugin have to prepare new stream playback.
 * starting new cahnnel : valid url
 * ending current channel: NULL url  (AML note: almost same as destroy player)
 *
 * @param[in] handleId handle ID.
 * @param[in] service_id: program number // In case of VoD, it should be "0"
 * @param[in] url session URL. (e.g.   udp://xxx.xxx.xxx.xxx.:port)
 * @param[in] type session Type. VoD : 0 , BTV : 1
 * @param[in] number_of_components number of ES stream
 * @param[in] pid_list array of ESPID
 * @return success: 0, fail: -1.
 */
int bpni_cas_channelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list)
{


	return 0;
}

/**
 * Whenever application starts receiving Meta data this function would be called, KT CAS plugin will start metat data by this.
  *
 * @param[in] handleId handle ID.
  * @param[in] descriptor_blob_input (string type) received VoD meta data from server by Java App
 * @param[in] descriptor_blob_input_length length of VOD Meta data
 * @return success: 0, fail: -1
 */
int bpni_cas_MDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, char* descriptor_blob_input, int descriptor_blob_input_length)
{


	return 0;

}

/**
 * In case BPNI_MDStatusCallback with success event, call bpni_cas_MDReceptionEnd for notifying
 *
 * @param[in] handleId handle ID.
 */
void bpni_cas_MDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId)
{



}

/**
 * Start VoD playback, if necessary, with adjusting playback position or direction and speed
 *
 * @param[in] handleId handle ID.
 * @param[in] [Unused] position position of VoD playback. milli second
 * @param[in] speed (+)FF, (-)REW
 * @return success 0, fail -1.
 */
int bpni_cas_setPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, long position, int speed)
{


	return 0;
}

/**
  * Send CAS Event
  *
  * @param[in] jsonString
  * @return 0 Success. -1 fail.
  */
int bpni_cas_sendCasEvent(char* jsonString)
{


	return 0;
}



/**
 * Register CAS event callback
 *
 * @param[in] callback Callback function
  */
void bpni_cas_registerCasEventCallback(BPNI_CAS_CasEventCallback callback)
{


}

/**
  * Setting EMM Address
  *
  * @return 0 Success. -1 fail.
  */
int bpni_cas_setEMMAddress(char* address)
{
	//TODO: OEM HAL, or CAS session?

	return -1;
}


