/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
#define LOG_TAG "KTHALAmlMpPlayerWrapper"
#define KEEP_ALOGX
#include <cutils/properties.h>
#include <utils/Log.h>
#include <unistd.h>
#include <AmlMpPlayerWrapper.h>
#include <BasePlayerNativeInterface.h>
#include "AltiCasHelper.h"

const int PID_INITIAL_UNUSED		= 0xffff;
const int PID_DECODE_STOP			= 0;


AmlMpPlayerWrapper::AmlMpPlayerWrapper(int playerId, sp<ICas>& ICas)
: mJniPlayerId(playerId), mANativeWindow(nullptr), mIsPlayerSetANativeWindow(false)
{
	 mDemuxId = getDemuxId(playerId);
	//mDemuxId = (Aml_MP_DemuxId)(playerId - 1);
	// mJniTunerId = mDemuxId; //TODO :CAS plugin, check default mJniTunerId as playerId?
	mAudioPid = PID_INITIAL_UNUSED;
	mVideoPid = PID_INITIAL_UNUSED;

	Aml_MP_IptvCasParams casParams;
	mCasHandle = std::make_shared<AmlKTAltiIptvCas>(&casParams, playerId, mICas);

#if USE_AMLTS_PARSER
	mParser = new Parser(mDemuxId, true, true);
#endif
}

AmlMpPlayerWrapper::~AmlMpPlayerWrapper()
{
	clear();
	mCasHandle.reset();
}

void AmlMpPlayerWrapper::clear(void)
{
	std::unique_lock<std::mutex> _l(mLock);

    if (mCasHandle) {
        mCasHandle->closeSession();
    }

	mPids.clear();
	mProgramInfo = nullptr;
	mIsPmtReceived = false;
	mAudioPid = PID_INITIAL_UNUSED;
	mVideoPid = PID_INITIAL_UNUSED;
	mIsPlayerSetANativeWindow = false;
	mMediaMeta.ar_type = BPNI_ASPECT_RATIO_UNKNOWN;
	mMediaMeta.width = 0;
	mMediaMeta.height = 0;
	mMediaMeta.audioCodec = BPNI_AUDIO_CODEC_UNKNOWN;
	mMediaMeta.videoCodec = BPNI_VIDEO_CODEC_UNKNOWN;
	mReadyToPlay= false;
	mStopFlag = true;
}

int AmlMpPlayerWrapper::createMpPlayer(void)
{
	//-> prepare()
	mIsPlayerSetANativeWindow = false;
	return 0;
}

int AmlMpPlayerWrapper::destroyMpPlayer(void)
{
	stop(true,true);
	clear();
	return 0;
}


Aml_MP_DemuxId AmlMpPlayerWrapper::getDemuxId(int playerId)
{
	Aml_MP_DemuxId demuxId;
	//BPNI_PLAYER_HANDLE_ID : 1:main, 2:pip, 3~5:multiview
	switch (playerId) {
		case 1:
			demuxId = AML_MP_HW_DEMUX_ID_0;
		break;
		case 2:
			demuxId = AML_MP_HW_DEMUX_ID_1;
		break;		
		case 3:
			demuxId = AML_MP_HW_DEMUX_ID_2;
		break;
		case 4:
			demuxId = AML_MP_HW_DEMUX_ID_3;
		break;
		case 5:
			demuxId = AML_MP_HW_DEMUX_ID_4;
		break;
		default:
			ALOGE("Unsupprted playerId %d", playerId);
			demuxId = AML_MP_DEMUX_ID_DEFAULT;
		break;
	}
	return demuxId;
}

void AmlMpPlayerWrapper::registerPlayerEventCallback(BPNI_PlayerEventCallback callback)
{
	mJniCallback = callback;
}


void AmlMpPlayerWrapper::feedData(char* data, int len)
{
	//L_TRACE_LINE();
	//KTPLAYER_HEXDUMP(LOG_TAG, data, len);
	if(not mReadyToPlay)
		return;
	if ( (data != nullptr) && (len >= 0) && (mPlayerHandle != nullptr) )
	{
		uint8_t* buffer = (uint8_t*)data;
		int writeSize = 0;
		int retryCount = 0;
		int writeLen = 0;
		while (writeSize < len) {
			if (writeLen < 0) {
				usleep(1000 * 10);
				retryCount++;
				if (retryCount > 500) {
					ALOGI("feedData: retry too much, but do nothing...");
					//retryCount = 0;
					return;
				}
			}
			std::unique_lock<std::mutex> _l(mLock);
			if (mStopFlag) {
				break;
			}
			writeLen = Aml_MP_Player_WriteData(mPlayerHandle, buffer + writeSize, len - writeSize);
			if (writeLen > 0) {
				writeSize += writeLen;
			}
		}

		//ALOGI("feedData: done %p %d retryCount(%d)", data, len,retryCount);
	}
}

void AmlMpPlayerWrapper::SetANativeWindow(ANativeWindow* window)
{
	//deprecated : now using sideband at client side
	if ( (window != nullptr) )
	{
		mANativeWindow = window; // reuse for next amlMpPlayer, before startPlayer
		if (mPlayerHandle != nullptr) {
			Aml_MP_Player_SetANativeWindow(mPlayerHandle, window);
			mIsPlayerSetANativeWindow = true;
		}
	}
}

int AmlMpPlayerWrapper::prepare()
{
	ALOGD("prepare mJniPlayerId(%d) mDemuxId(%d)",mJniPlayerId,mDemuxId);

	if(mReadyToPlay)
	{
		stop(true, true);
	}
	
    Aml_MP_PlayerCreateParams createParams;
    createParams.channelId = AML_MP_CHANNEL_ID_AUTO; //TODO:
    createParams.demuxId = mDemuxId;
	createParams.drmMode = AML_MP_INPUT_STREAM_NORMAL; //TODO:
	if (mIsPmtReceived) {
		if (mProgramInfo->scrambled) {
			createParams.drmMode = AML_MP_INPUT_STREAM_ENCRYPTED;
			ALOGD("ENCRYPTED channel(%d) ",mJniPlayerId);
			
		}
	}
	createParams.sourceType = AML_MP_INPUT_SOURCE_TS_MEMORY;
    int ret = Aml_MP_Player_Create(&createParams, &mPlayerHandle);
	if (ret)
	{
		ALOGE("Fail  Aml_MP_Player_Create return (0x%x)",ret);
		return -1;
	}
	mStopFlag = false;

	if (not mIsPmtReceived)
	{
	    Aml_MP_VideoParams videoParams;
	    videoParams.pid = mVideoPid;
	    videoParams.videoCodec = AML_MP_CODEC_UNKNOWN;
	    Aml_MP_Player_SetVideoParams(mPlayerHandle, &videoParams);

	    Aml_MP_AudioParams audioParams;
	    audioParams.pid = mAudioPid;
	    audioParams.audioCodec = AML_MP_CODEC_UNKNOWN;
	    Aml_MP_Player_SetAudioParams(mPlayerHandle, &audioParams);
    }

	if (not mIsPlayerSetANativeWindow && mANativeWindow) { //TODO: check nativewindows, maybe reuse this..
		ALOGE("something wrong, CHECK  mIsPlayerSetANativeWindow");
	    Aml_MP_Player_SetANativeWindow(mPlayerHandle, mANativeWindow);
		mIsPlayerSetANativeWindow = true;
    }

	// event callback
    registerEventCallback([](void* userData, Aml_MP_PlayerEventType event, int64_t param) {
        AmlMpPlayerWrapper* self = (AmlMpPlayerWrapper*)userData;
        return self->eventCallback(event, param);
    }, this);

	mReadyToPlay = true;
	return 0;
}

void AmlMpPlayerWrapper::eventCallback(Aml_MP_PlayerEventType event, int64_t param)
{
	//std::unique_lock<std::mutex> _l(mLock);
    switch (event) {
	    case AML_MP_PLAYER_EVENT_FIRST_FRAME:
	    {
	        mFirstVFrameDisplayed = true;
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_VIDEO_CHANGED:
	    {
			if (param) {
		        Aml_MP_PlayerEventVideoFormat* videoFormatEvent = (Aml_MP_PlayerEventVideoFormat*)param;
		        ALOGD(" frame_width  =%d",videoFormatEvent->frame_width);
		        ALOGD(" frame_height =%d",videoFormatEvent->frame_height);
		        ALOGD(" frame_rate   =%d",videoFormatEvent->frame_rate);
		        ALOGD("TODO: aspectratio  =%d",videoFormatEvent->frame_aspectratio);
		        mMediaMeta.width = videoFormatEvent->frame_width;
		        mMediaMeta.height = videoFormatEvent->frame_height;
		        mMediaMeta.ar_type = BPNI_ASPECT_RATIO_16_9; //videoFormatEvent->frame_aspectratio TODO: need convert
	        }
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_AUDIO_CHANGED:
	    {
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_AV_SYNC_DONE:
	    {
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_DATA_LOSS:
	    {
            mPlayingErrorCounts++;
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_DATA_RESUME:
	    {
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_SCRAMBLING:
	    {
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_USERDATA_AFD:
	    {
	    }
	    break;
	    case AML_MP_PLAYER_EVENT_USERDATA_CC:
		{
		}
		break;
		default:
		break;
    }
}


void AmlMpPlayerWrapper::registerEventCallback(Aml_MP_PlayerEventCallback cb, void* userData)
{
    mEventCallback = cb;
    mUserData = userData;
	Aml_MP_Player_RegisterEventCallBack(mPlayerHandle,mEventCallback, mUserData);

}

void AmlMpPlayerWrapper::setPmt(char* pmtData, int len)
{
	ALOGD("%s (%d)",__FUNCTION__, len);
	uint8_t* buffer = (uint8_t*)pmtData;
#if USE_AMLTS_PARSER
	mParser->parsePmt(100/*temp pid*/, len, buffer);
	mProgramInfo = mParser->getProgramInfo();
#else
	sptr<SimpleParser> parser = new SimpleParser(mProgramNumber);
	parser->parsePmt(100/*temp*/, len, (const uint8_t*)pmtData);
	mProgramInfo = parser->getProgramInfo();
#endif
	if (mProgramInfo){
		ALOGD("found program info");	
		mProgramInfo->debugLog();
		mIsPmtReceived = true;
	}
}


int AmlMpPlayerWrapper::casChannelChange(int service_id, char* url, int type, int number_of_components, int* pid_list)
{
	// TODO: add Media CAS control
	ALOGD("%s mJniPlayerId(%d) service(%d) %s",__FUNCTION__, mJniPlayerId, service_id, url);
	mCasUrl = url;
	if (url == nullptr)
	{
		//TODO: close/destroy MpPlayer, CAS close session
		//
		stop(true,true);
		destroyMpPlayer();
		return 0;
	}else
	{
		mProgramNumber = service_id;
		mIsLiveChannel = (type==0)? false: true; //VoD : 0 , BTV : 1
		for (auto i = 0; i < number_of_components; i++)
		{
			mPids.push_back(*pid_list);
			pid_list++;
		}
		//CAS channel change command
		cas_channel_change_info info;
		info.connection = (int)mDemuxId;
		info.service_id = service_id;
		info.url = url;
		info.type = type;
		info.number_of_components = number_of_components;
		info.pid_list = pid_list;
		std::string command = AltiCasHelper::buildCasChannelChange(info);
		
	}

	return 0;
}


void AmlMpPlayerWrapper::casMDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, std::string& descriptor_blob_input, int descriptor_blob_input_length)
{
	//TODO:
	ALOGD("%s handleId(%d) %s",__FUNCTION__, (int)handleId, descriptor_blob_input.c_str());
}

void AmlMpPlayerWrapper::casMDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId)
{
	ALOGD("%s handleId(%d) %s",__FUNCTION__, (int)handleId);


}

int32_t AmlMpPlayerWrapper::casSetPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, int32_t position, int32_t speed)
{

	return 0;

}

int32_t AmlMpPlayerWrapper::casSendCasEvent(std::string& jsonStr)
{

	ALOGD("%s %s",__FUNCTION__, jsonStr.c_str());

	return 0;
}

int32_t AmlMpPlayerWrapper::casSetEMMAddress(std::string& addr)
{
	ALOGD("%s addr=%s",__FUNCTION__, addr.c_str());

	return 0;

}

/**
 * Play A/V with given a-v PID
 * <br> this API can be called multiple times without calling stop()
 * <br> i.e.) playStream() -> stop() -> playStream()
 * <br> ex 1) playStream(8006, 8005) -> playStream(8007, 8005)  : change audio language or change audio track
 * <br> ex 2) playStream(8006, 8005) -> playSteram(0, 8005) : normal AV play -> play video ONLY
 * <br> ex 3) playStream(0, 8005) -> playSteram(8006, 8005) : play video ONLY -> player both A/V
 * <br> ex 4) playStream(8006, 8005) -> playStream(0, 0) : same as stop(), no A/V playing
 *
 * @param[in] handleId handle ID.
 * @param[in] audiPID - audio PID. "0" means NO audio or stop audio (audio decoder stop)
 * @param[in] videoPID - video PID. "0" means NO video or stop video (video decoder stop)
 *
 * @return 0 Success. -1 fail.
 */
int AmlMpPlayerWrapper::playStream(int audiPID, int videoPID)
{
	//TODO: stop audio only, stop video only
	ALOGD("%s mJniPlayerId(%d), apid=0x%x, vpid=0x%x",__FUNCTION__, mJniPlayerId, audiPID, videoPID);

	if (not mIsPmtReceived)
	{
		ALOGE("Missing PMT");
		return -1;
	}

	Aml_MP_Player_SetParameter(mPlayerHandle, AML_MP_PLAYER_PARAMETER_VIDEO_TUNNEL_ID, (void*)&mJniTunerId);
	// Video
	// TODO: video change, stop or audio only
	Aml_MP_VideoParams videoParams;
	videoParams.pid = mVideoPid = videoPID;
	if (mProgramInfo == nullptr) {
		ALOGE("ProgramInfo invalid");
		return -1;
	}

	videoParams.videoCodec = AML_MP_CODEC_UNKNOWN;
	for (auto it : mProgramInfo->videoStreams) {
		if (it.pid == videoPID) {
			videoParams.videoCodec = it.codecId;
		}
	}

	Aml_MP_Player_SetVideoParams(mPlayerHandle, &videoParams);
	mVideoCodec = videoParams.videoCodec;


	// Audio
	Aml_MP_AudioParams audioParams;
	audioParams.pid = audiPID;

	audioParams.audioCodec = AML_MP_CODEC_UNKNOWN;
	for (auto it : mProgramInfo->audioStreams) {
		if (it.pid == audiPID) {
			audioParams.audioCodec = it.codecId;
		}
	}

	if (mAudioPid == PID_INITIAL_UNUSED)	// first audio start
	{
		Aml_MP_Player_SetAudioParams(mPlayerHandle, &audioParams);
	}else if ( (mAudioPid != PID_INITIAL_UNUSED) && (mAudioPid != 0)) {
		if (audiPID) // audio play => audio play (switch audio track)
		{
			Aml_MP_Player_SwitchAudioTrack(mPlayerHandle, &audioParams);
			//return 0; // TODO: check return (already playstarted)
		}else{	// TODO: audio play => audio stop  **
			Aml_MP_Player_StopAudioDecoding(mPlayerHandle);
		}
	}else{ // mAudioPid==0, prev audio stop

		if (audiPID) //audio stop => now play audio
		{
			Aml_MP_Player_SetAudioParams(mPlayerHandle, &audioParams);
			Aml_MP_Player_StartAudioDecoding(mPlayerHandle);
		}else{	//audio stop => keep stop
			//
		}
	}

	mAudioPid = audiPID;
	mAudioCodec = audioParams.audioCodec;
	storeMediaMeta();

	// TODO: subtitle....
#if 0// we need direct CAS control due to incompatible with the wv/vmx iptv cas.
	if(KTALTICAST_CA_SYSTEM_ID == mProgramInfo->caSystemId)
	{
		ALOGE("FOUND KTCAS SYSTEM_ID 0x%x", mProgramInfo->caSystemId);
	Aml_MP_IptvCasParams casParams;
	memset(&casParams, 0, sizeof(casParams));
	casParams.type = AML_MP_CAS_UNKNOWN;  //ML_MP_CAS_ALTI_KT;
	casParams.audioCodec = audioParams.audioCodec;
	casParams.audioPid = audioParams.pid;
	casParams.videoCodec = videoParams.videoCodec;
	casParams.videoPid = videoParams.pid;
	casParams.caSystemId = mProgramInfo->caSystemId;
	casParams.ecmPid[ECM_INDEX_AUDIO] = mProgramInfo->ecmPid[ECM_INDEX_AUDIO];
	casParams.ecmPid[ECM_INDEX_VIDEO] = mProgramInfo->ecmPid[ECM_INDEX_VIDEO];
	casParams.demuxId = mDemuxId;
	casParams.private_size = mProgramInfo->privateDataLength;
	memcpy(casParams.private_data, mProgramInfo->privateData, mProgramInfo->privateDataLength);

	Aml_MP_Player_SetIptvCASParams(mPlayerHandle, &casParams);
	}
#endif
	return Aml_MP_Player_Start(mPlayerHandle);

}

int AmlMpPlayerWrapper::stop(bool isAudioStop, bool isVideoStop)
{
	if ( isAudioStop & isVideoStop)
	{
		std::unique_lock<std::mutex> _l(mLock);
		if(not mStopFlag)
		{
			Aml_MP_Player_Stop(mPlayerHandle);
			Aml_MP_Player_Destroy(mPlayerHandle);
			mEnableAudio = false;
			mStopFlag = true;
		}
	} else {
		if (isAudioStop) {
			Aml_MP_Player_StopAudioDecoding(mPlayerHandle);
			mEnableAudio = false;
		}
		if (isVideoStop) {
			Aml_MP_Player_StopVideoDecoding(mPlayerHandle);
		}
	}
	return 0;
}

//TODO: check volume range
int AmlMpPlayerWrapper::setVolume(float volume)
{
	int ret = 0;
	float _vol = volume*100.0f;
	bool mute = (_vol == 0);
	ret += Aml_MP_Player_SetParameter(mPlayerHandle, AML_MP_PLAYER_PARAMETER_AUDIO_MUTE, (void*)&mute);
	if (_vol > 0) {
		ret += Aml_MP_Player_SetVolume(mPlayerHandle, _vol);
	}
	return ret;
}

float AmlMpPlayerWrapper::getVolume()
{
	float _vol;
	Aml_MP_Player_GetVolume(mPlayerHandle, &_vol);
	return _vol/100.0f;
}

int32_t AmlMpPlayerWrapper::setAudioEnabled(bool enabled)
{
	mEnableAudio = enabled;
	if(enabled)
		Aml_MP_Player_StartAudioDecoding(mPlayerHandle);
	else
		Aml_MP_Player_StopAudioDecoding(mPlayerHandle);
	return 0;
}

bool AmlMpPlayerWrapper::getAudioEnabled()
{
	return mEnableAudio;
}


int AmlMpPlayerWrapper::setRate( float rate)
{
	Aml_MP_Player_SetPlaybackRate(mPlayerHandle, rate);
	mPlayRate = rate;
	return 0;
}

double AmlMpPlayerWrapper::getPts()
{
	int64_t pts;
	int ret;
	if (mPlayerHandle == nullptr)
		return -1;

	ret = Aml_MP_Player_GetCurrentPts(mPlayerHandle, AML_MP_STREAM_TYPE_AUDIO, &pts);
	if (ret == -1) {
		ret = Aml_MP_Player_GetCurrentPts(mPlayerHandle, AML_MP_STREAM_TYPE_VIDEO, &pts);
		ALOGD("Current video pts: 0x%llx, ret: %d\n", pts, ret);
		if (ret == -1) {
			ALOGW("Fail to get Current a/v pts");
			return -1;
		}
		return pts;
	} else {
		ALOGD("Current audio pts: 0x%llx, ret: %d\n", pts, ret);
		return pts;
	}
	return -1;
}

void AmlMpPlayerWrapper::flush()
{
	Aml_MP_Player_Flush(mPlayerHandle);
}

BPNI_AUDIO_CODEC AmlMpPlayerWrapper::convertAmlCodecToJniAudio(Aml_MP_CodecID codec)
{
	BPNI_AUDIO_CODEC jniAudioCodec;
	switch (codec) {
	case AML_MP_AUDIO_CODEC_MP2:
		jniAudioCodec = BPNI_AUDIO_CODEC_MPEG; break;
    case AML_MP_AUDIO_CODEC_MP3:
		jniAudioCodec = BPNI_AUDIO_CODEC_MP3; break;
    case AML_MP_AUDIO_CODEC_AC3:
		jniAudioCodec = BPNI_AUDIO_CODEC_AC3; break;
    case AML_MP_AUDIO_CODEC_EAC3:
		jniAudioCodec = BPNI_AUDIO_CODEC_AC3; break;  //TODO DD+?
    case AML_MP_AUDIO_CODEC_DTS:
		jniAudioCodec = BPNI_AUDIO_CODEC_DTS; break;
    case AML_MP_AUDIO_CODEC_AAC:
		jniAudioCodec = BPNI_AUDIO_CODEC_AAC; break;
    case AML_MP_AUDIO_CODEC_LATM:
		jniAudioCodec = BPNI_AUDIO_CODEC_AAC;break;    //TODO
    case AML_MP_AUDIO_CODEC_PCM:
		jniAudioCodec = BPNI_AUDIO_CODEC_PCM; break;
	default:
		jniAudioCodec = BPNI_AUDIO_CODEC_UNKNOWN; break;
	}
	return jniAudioCodec;
}



BPNI_VIDEO_CODEC AmlMpPlayerWrapper::convertAmlCodecToJniVideo(Aml_MP_CodecID codec)
{
	BPNI_VIDEO_CODEC jniVideoCodec;

	switch (codec) {
	case AML_MP_VIDEO_CODEC_BASE:
		jniVideoCodec = BPNI_VIDEO_CODEC_NONE; break;
    case AML_MP_VIDEO_CODEC_MPEG12:
		jniVideoCodec = BPNI_VIDEO_CODEC_MPEG2; break;  //TODO mpeg1, mpeg2
    case AML_MP_VIDEO_CODEC_MPEG4:
		jniVideoCodec = BPNI_VIDEO_CODEC_MPEG4; break;
    case AML_MP_VIDEO_CODEC_H264:
		jniVideoCodec = BPNI_VIDEO_CODEC_H264; break;
    case AML_MP_VIDEO_CODEC_VC1:
		jniVideoCodec = BPNI_VIDEO_CODEC_VC1; break;
    case AML_MP_VIDEO_CODEC_AVS:
		jniVideoCodec = BPNI_VIDEO_CODEC_AVS; break;
    case AML_MP_VIDEO_CODEC_HEVC:
		ALOGD("TODO: no matching AML_MP_VIDEO_CODEC_HEVC");
		break;   //TODO: check Alticast format.. no H265, HEVC
    case AML_MP_VIDEO_CODEC_VP9:
		ALOGD("TODO: no matching AML_MP_VIDEO_CODEC_VP9");
		break;  //TODO: check Alticast format..
    case AML_MP_VIDEO_CODEC_AVS2:
		ALOGD("TODO: no matching AML_MP_VIDEO_CODEC_AVS2");
		break;  //TODO: check Alticast format..
	default:
		jniVideoCodec = BPNI_VIDEO_CODEC_UNKNOWN;
		break;
	}
	return jniVideoCodec;
}

void AmlMpPlayerWrapper::storeMediaMeta()
{
	mMediaMeta.videoCodec = convertAmlCodecToJniVideo(mVideoCodec);
	mMediaMeta.audioCodec = convertAmlCodecToJniAudio(mAudioCodec);
}

BPNI_MediaMetaData AmlMpPlayerWrapper::getMetaData()
{
	return mMediaMeta;
}




