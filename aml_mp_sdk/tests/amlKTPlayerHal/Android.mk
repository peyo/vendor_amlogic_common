LOCAL_PATH:= $(call my-dir)
###############################################################################
# libaml_kt_hal.vendor
include $(CLEAR_VARS)
AML_KT_PLAYER_HAL_SRC := \
	BasePlayerNativeInterface.cpp \
	AmlMpPlayerWrapper.cpp \
	AmlPlayerManager.cpp \
	KTPlayerCommon.cpp \
	SimpleParser.cpp \
	

### KT CAS:
AML_KT_PLAYER_HAL_SRC += \
	KtAlti_iptvcas/AmlKTAltiIptvCas.cpp	 \
	KtAlti_iptvcas/AmKtCasIPTVImpl.cpp	 \
	KtAlti_iptvcas/AltiCasHelper.cpp


## AML_MP_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) -std=c++11
AML_MP_CFLAGS := -DANDROID_PLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

AML_MP_INC := $(LOCAL_PATH) \
	$(LOCAL_PATH)/KtAlti_iptvcas \
	$(TOP)/vendor/amlogic/common/apps/LibTsPlayer/jni/include \
	$(TOP)/vendor/amlogic/common/libdvr_release/include \
	$(TOP)/vendor/amlogic/common/external/DTVKit/cas_hal/libamcas/include \
	$(TOP)/vendor/amlogic/common/mediahal_sdk/include \
	$(TOP)/hardware/amlogic/gralloc \
	$(TOP)/hardware/amlogic/media/amcodec/include \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/utils \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/include \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/cas \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/tests/amlMpTestSupporter \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/tests/amlMpTestSupporter/source \
	$(TOP)/vendor/amlogic/common/frameworks/services/subtiltleserver/client


AML_MP_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/include \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/KtAlti_iptvcas \
	$(TOP)/hardware/amlogic/media/amcodec/include \
	$(TOP)/vendor/amlogic/common/libdvr_release/include \
	$(TOP)/vendor/amlogic/common/mediahal_sdk/include \
	$(TOP)/vendor/amlogic/common/mediahal_sdk/ \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/utils \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/cas/ktalti_iptvcas \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/tests/amlMpTestSupporter \
	$(TOP)/vendor/amlogic/common/aml_mp_sdk/tests/amlMpTestSupporter/source \
	
AML_MP_VENDOR_SHARED_LIBS := \
	libutils \
	libcutils \
	liblog \
	libnativewindow \
	libui \
	libstagefright_foundation \
	libaml_mp_sdk.vendor \
	

AML_MP_VENDOR_SHARED_LIBS += \
	libjsoncpp\
	libhidlbase \
	android.hardware.cas@1.0 \
	android.hardware.cas.native@1.0

#TODO: AML_MP_CFLAGS += -DHAVE_KTALTI_IPTV_CAS

LOCAL_STATIC_LIBRARIES += \
	libamlMpTestSupporter.vendor


#TODO: copy system lib?
LOCAL_MODULE := libaml_kt_hal.vendor
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(AML_KT_PLAYER_HAL_SRC)
LOCAL_CFLAGS := $(AML_MP_CFLAGS)
LOCAL_C_INCLUDES := $(AML_MP_INC)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(AML_MP_EXPORT_C_INCLUDE_DIRS)
LOCAL_SHARED_LIBRARIES := $(AML_MP_SHARED_LIBS) $(AML_MP_VENDOR_SHARED_LIBS) $(AML_MP_VENDOR_SHARED_LIBS_$(PLATFORM_SDK_VERSION))
include $(BUILD_SHARED_LIBRARY)



