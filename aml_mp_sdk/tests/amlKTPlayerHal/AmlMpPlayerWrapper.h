/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _AML_MP_PLAYER_WRAPPER_H_
#define _AML_MP_PLAYER_WRAPPER_H_

#define USE_AMLTS_PARSER	0 

#include <vector>
#include "utils/AmlMpRefBase.h"
#include "Aml_MP/Aml_MP.h"
#include "BasePlayerNativeInterface.h"
#if USE_AMLTS_PARSER
#include "demux/AmlTsParser.h"
#else
#include "SimpleParser.h"
#endif
#include "KTPlayerCommon.h"
#include "KtCasCommon.h"
#include "AmlKTAltiIptvCas.h"

using namespace aml_mp;

///////////////////////////////////////////////////////
//NOTE: Alticast App didn't call player create/destory.

//TODO: think mutex
class AmlMpPlayerWrapper
{
public:
	AmlMpPlayerWrapper(int playerId, sp<ICas>& ICas );
	virtual ~AmlMpPlayerWrapper();
	int createMpPlayer(void);
	int destroyMpPlayer(void);
	Aml_MP_DemuxId getDemuxId(int playerId);
	void registerPlayerEventCallback(BPNI_PlayerEventCallback callback);
	void setPmt(char* pmtData, int len);
	void setTunerId(int tunerId) { mJniTunerId = tunerId; };
	int getTunerId(void) { return mJniTunerId; };
	void feedData(char* data, int len);
	void SetANativeWindow(ANativeWindow* window);
	ANativeWindow* getSurfaceNativeWindow() { return mANativeWindow; };
	int prepare(); //create MpPlayer
	int playStream(int audiPID, int videoPID);
	int stop(bool isAudioStop, bool isVideoStop);
	int setVolume(float volume);
	float getVolume();
	int32_t setAudioEnabled(bool enabled);
	bool getAudioEnabled();
	int setRate(float rate);
	float getRate() { return mPlayRate; };
	double getPts();
	void flush();
	BPNI_MediaMetaData getMetaData();
	int casChannelChange(int service_id, char* url, int type, int number_of_components, int* pid_list);
	void casMDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, std::string& descriptor_blob_input, int descriptor_blob_input_length);
	void casMDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId);
	int32_t casSetPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, int32_t position, int32_t speed);
	int32_t casSendCasEvent(std::string& jsonStr);
	int32_t casSetEMMAddress(std::string& addr);

private:
	void registerEventCallback(Aml_MP_PlayerEventCallback cb, void* userData);
	void clear(void);
	BPNI_VIDEO_CODEC convertAmlCodecToJniVideo(Aml_MP_CodecID codec);
	BPNI_AUDIO_CODEC convertAmlCodecToJniAudio(Aml_MP_CodecID codec);
	void storeMediaMeta();
	void eventCallback(Aml_MP_PlayerEventType event, int64_t param);

private:
	sp<ICas> mICas;

	AML_MP_PLAYER mPlayerHandle;
	Aml_MP_DemuxId mDemuxId;
	ANativeWindow* mANativeWindow;
	bool mIsPlayerSetANativeWindow;
	int mJniPlayerId;
	int mJniTunerId = -1;
	std::shared_ptr<AmlKTAltiIptvCas> mCasHandle = nullptr;
#if USE_AMLTS_PARSER
	sptr<Parser> mParser;
#endif
    
	Aml_MP_PlayerCreateParams createParams;
	BPNI_PlayerEventCallback mJniCallback;
	int 					mProgramNumber = -1;	// In case of VoD, it should be "0"
	char*					mCasUrl = nullptr;
	bool 					mIsLiveChannel = true; //VOD:false, live channel:true
	bool					mIsPmtReceived = false;
	std::vector<int>		mPids;
	sptr<ProgramInfo>		mProgramInfo;

	uint16_t               mVideoPid;
	uint16_t               mAudioPid;
	uint16_t               mEcmPid;
	Aml_MP_CodecID          mVideoCodec;
	Aml_MP_CodecID          mAudioCodec;
	float 					mPlayRate;
	BPNI_MediaMetaData		mMediaMeta;
    Aml_MP_PlayerEventCallback mEventCallback = nullptr;
    void* mUserData = nullptr;
    bool mFirstVFrameDisplayed = false;
    int mPlayingErrorCounts = 0;
	bool mStopFlag = false;
    std::mutex mLock;
    bool mReadyToPlay=false;
    bool mEnableAudio=true;

};


#endif

