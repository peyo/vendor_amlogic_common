/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _KT_PLAYER_COMMON_H__
#define _KT_PLAYER_COMMON_H__
#include <log/log.h>
#include <ctype.h>

#include "utils/AmlMpRefBase.h"
#include "Aml_MP/Aml_MP.h"
#include "BasePlayerNativeInterface.h"




#ifndef KTALTICAST_CA_SYSTEM_ID
#define KTALTICAST_CA_SYSTEM_ID		0x0E01
#endif


#define LOG_TRACE_LINE()				ALOGD("%s() : Line %d", __FUNCTION__, __LINE__)
#define RETURN_VOID_IF_MSG(cond, msg) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return - " msg, __FUNCTION__, __LINE__); \
				return; \
			} \
		} while(0)

#define RETURN_VOID_IF(cond) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return <- %s", __FUNCTION__, __LINE__, #cond); \
				return; \
			} \
		} while(0)

#define RETURN_IF(error, cond) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return %s <- %s", __FUNCTION__, __LINE__, #error, #cond); \
				return error; \
			} \
		} while(0)


inline void KTPLAYER_HEXDUMP(const char * tag, const void *_data, size_t size)
{
	const char * data = (const char * )_data;
	//50hexdecimal + |1 + 16b + |1 = 68 chars per line
	char _buffer[128];

	for (size_t i = 0; i < size; i += 16)
	{
		char * buffer = _buffer;

		//	write hexdecimal line
		buffer = _buffer;
		size_t progress = 0;
		for (size_t j = 0; j < 16; ++j)
		{
			if (j == 8)	//	add more space after 8 bytes written.
				progress++;

			if (i + j < size)
				snprintf(buffer + progress + j * 3, 5, "%02x  ", uint8_t(data[i + j]));
			else
			{
				snprintf(buffer + progress + j * 3, 5, "    ");
			}
		}

		//	write text |...|
		buffer = _buffer;
		progress = 50;
		buffer[progress] = '|';
		progress++;

		for (size_t j = 0; j < 16; ++j)
		{
			if (i + j < size)
			{
				if(isprint(data[i + j]))
					buffer[progress + j] = uint8_t(data[i + j]);
				else
					buffer[progress + j] = '.';
			}
			else
				buffer[progress + j] = ' ';
		}
		buffer[progress + 16] = '|';
		buffer[progress + 17] = '\0';

		ALOGD("[%08zd] %s \n", i, _buffer);
	}
}



#endif

