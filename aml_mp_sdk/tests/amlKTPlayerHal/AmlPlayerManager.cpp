/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_NDEBUG 0
#define LOG_TAG "KTHALAmlPlayerManager"
#include <sstream>
#include <utils/Log.h>
#include "AmlPlayerManager.h"

#define ENABLE_KTCAS_SUPPORT	0

#define MAX_PLAYERS_NUM	6


using namespace aml_mp;
using android::hardware::cas::V1_0::Status;

sp<IMediaCasService> AmlPlayerManager::gCasService = nullptr;

class casListenerImpl : public ICasListener
{
public:
	casListenerImpl(AmlPlayerManager* host) : mHost(host) { }
	virtual ~casListenerImpl() { }
    virtual Return<void> onEvent(int32_t event, int32_t arg, const hidl_vec<uint8_t>& data) override {
    	std::vector<uint8_t> _data = data;
		//if(mHost)
			mHost->onCasEvent(event, arg, _data);
	   return Void();
   	}

private:
	AmlPlayerManager* mHost;
};


//NOTE: Alticast CAS limit ->  call one time createPlugin
// so we cannot register callback for each session
// createPlugin() at here, openSession() will be called at each player
bool AmlPlayerManager::createPlugin(const sp<ICasListener>& listener)
{
	if(gCasService == nullptr)
	{
		ALOGE("no Cas service");
		return false;
	}
	const int32_t CA_system_id = KTALTICAST_CA_SYSTEM_ID;
	//TODO: need to multiple listener, call multi createPlugin: check plugin
	//  maybe need to use one listener, then need moving to global
	auto pluginStatus = gCasService->createPlugin(CA_system_id, listener);
	if (!pluginStatus.isOk()) {
		ALOGE("Cannot createPlugin");
		return false;
	}
	mICas = pluginStatus;
	if (mICas == nullptr) {
		ALOGE("=====================");
		ALOGE("FATAL: Cannot KTCAS createPlugin");
		ALOGE("=====================");
		return false;
	}
	ALOGD("KTCAS Plugin created");
	return true;
}

void AmlPlayerManager::initMediaCas()
{
	while (gCasService == nullptr)
	{
		gCasService = IMediaCasService::getService("default");
		if (gCasService == nullptr)
		{
			ALOGD("Cannot obtain IMediaCasService");
			usleep(200*1000);//sleep 200ms
		}
	}
	
	mCasListener = new casListenerImpl(this);
	if ( createPlugin(mCasListener) )
	{
	    mIptvCas = std::make_shared<AmKtCasIPTVImpl>(mICas);
    }

//	//TODO: delete me
//	std::string testUrl = "udp://233.18.158.252:1901;udp://233.18.158.250:1902";
//    casSetEMMAddress(testUrl);
}

void AmlPlayerManager::init()
{
#if ENABLE_KTCAS_SUPPORT
	initMediaCas();
#endif


	Aml_MP_Initialize();
	int version = Aml_MP_GetVersion(nullptr);
	ALOGE("=== Aml MP version = 0x%x ===", version);

	for (int32_t id=1; id< MAX_PLAYERS_NUM ; id++) { //BPNI_PLAYER_HANDLE_ID_SUB3 (1~5)
	    auto player = std::make_shared<AmlMpPlayerWrapper>(id, mICas);
		mMapSession.insert(std::make_pair(id,player));
	}
}

void AmlPlayerManager::onCasEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data)
{
	//TODO: async event process
	std::string event_str(data.begin(),data.end());
	ALOGI("cas event:");
	ALOGI("%s", event_str.c_str());
//	if (auto listener = mListener.lock())
//	{
//		ALOGD("sending listener--->");
//		listener->onEvent(event, arg, data);
//	}
}



void AmlPlayerManager::registerPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		it->second->registerPlayerEventCallback(callback);
	} else {
		ALOGE("something wrong handleId(%d)", handleId);
	}
}

void AmlPlayerManager::setTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		it->second->setTunerId(tunerId);
	} else {
		ALOGE("something wrong handleId(%d)", handleId);
	}
}

int AmlPlayerManager::getTunerId(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getTunerId();
	} else {
		ALOGE("something wrong handleId(%d)", handleId);
		return -1;
	}
}

void AmlPlayerManager::feedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len)
{
#if 1
	if(mMapSession[handleId])
		mMapSession[handleId]->feedData(data,len);
#else
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->feedData(data,len);
	} else {
		ALOGE("something wrong handleId(%d)", handleId);
	}
#endif
}



void AmlPlayerManager::setSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->SetANativeWindow(window);
	}
}

ANativeWindow* AmlPlayerManager::getSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getSurfaceNativeWindow();
	}

	return nullptr;
}

int AmlPlayerManager::prepare(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->prepare();
	}
	return -1;
}


void AmlPlayerManager::setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len)
{
	if ( (pmtData == nullptr) || (len < 16) ) {
		ALOGE("setPmt error handleId(%d)", handleId);
		return;
	}
	if(mMapSession[handleId])
		mMapSession[handleId]->setPmt(pmtData, len);
}

int AmlPlayerManager::casChannelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list)
{
#if ENABLE_KTCAS_SUPPORT
	if ( pid_list == nullptr ) {
		ALOGE("casChannelChange error handleId(%d)", handleId);
		return -1;
	}

	if(mMapSession[handleId])
		mMapSession[handleId]->casChannelChange(service_id, url, type, number_of_components, pid_list);
#endif

	return 0;
}


void AmlPlayerManager::casMDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, std::string& descriptor_blob_input, int descriptor_blob_input_length)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->casMDReceptionStart(handleId, descriptor_blob_input, descriptor_blob_input_length);
	}
}

void AmlPlayerManager::casMDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->casMDReceptionEnd(handleId);
	}
}


int32_t AmlPlayerManager::casSetPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, int32_t position, int32_t speed)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->casSetPlayPosition(handleId, position, speed);
	}
	return -1;
}

int32_t AmlPlayerManager::casSendCasEvent(std::string& jsonStr)
{
#if ENABLE_KTCAS_SUPPORT

	auto it = mMapSession.find(BPNI_PLAYER_HANDLE_ID_MAIN);
	if ( it != mMapSession.end() )
	{
		return it->second->casSendCasEvent(jsonStr);
	}
#endif
	return 0;
}

int32_t AmlPlayerManager::casSetEMMAddress(std::string& addr)
{
#if ENABLE_KTCAS_SUPPORT

	std::string DEFAULT_IMS_ADDR = "233.18.158.250:1902";
	int ret;
	if(casMcasUrl == addr)
	{
		ALOGD("skip duplicated url =%s", addr.c_str());
		return 0;
	}

	//provision
	hidl_string provision_str(addr);
	auto returnStatus = mICas->provision(provision_str);
	if(not returnStatus.isOk())
	ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (android::hardware::cas::V1_0::Status)returnStatus);
	
	//parsing URLs
	ALOGE("addr = %s", addr.c_str());
	std::istringstream ss(addr);
	getline(ss, mEmmUrl, ';');
	ALOGE("EMM addr = %s", mEmmUrl.c_str());
	getline(ss, mImsUrl, ';');
	ALOGE("IMS addr = %s", mImsUrl.c_str());
	if (mImsUrl.find("0.0.0.0") != std::string::npos) {
		mImsUrl = DEFAULT_IMS_ADDR;
	}

	//UDP receiver - EMM
	mEmmSource = Source::create(mEmmUrl.c_str());
    if (mEmmSource == nullptr) {
        ALOGE("create mEmmSource failed!");
        return -1;
    }
    if (mEmmSource->initCheck() < 0) {
        ALOGE("mEmmSource initCheck failed!");
        return -1;
    }
    mEmmReceiver = new EmmReceiver;
	mEmmSource->addSourceReceiver(mEmmReceiver);
    ret = mEmmSource->start();
    if (ret < 0) {
        ALOGE("mEmmSource start failed!");
        return -1;
    }
	//UDP receiver - IMS
	mImsSource = Source::create(mImsUrl.c_str());
    if (mImsSource == nullptr) {
        ALOGE("create mImsSource failed!");
        return -1;
    }
    if (mImsSource->initCheck() < 0) {
        ALOGE("mImsSource initCheck failed!");
        return -1;
    }
    mImsReceiver = new ImsReceiver;
	mImsSource->addSourceReceiver(mImsReceiver);
    ret = mImsSource->start();
    if (ret < 0) {
        ALOGE("mImsSource start failed!");
        return -1;
    }
	casMcasUrl = addr;
#endif
	return 0;
}


int AmlPlayerManager::playStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->playStream(audiPID, videoPID);
	}
	return -1;
}



int AmlPlayerManager::stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->stop(isAudioStop, isVideoStop);
	}
	return -1;
}


int AmlPlayerManager::setVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->setVolume(volume);
	}
	return -1;
}


float AmlPlayerManager::getVolume(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getVolume();
	}

	return 0.0f;
}


int32_t AmlPlayerManager::setAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	if(mMapSession[handleId])
		return mMapSession[handleId]->setAudioEnabled(enabled);
	return -1;
}
bool AmlPlayerManager::getAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	if(mMapSession[handleId])
		return mMapSession[handleId]->getAudioEnabled();
	return false;
}



int AmlPlayerManager::setRate(BPNI_PLAYER_HANDLE_ID handleId, float rate)
{

	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->setRate(rate);
	}

	return -1;


}

float AmlPlayerManager::getRate(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getRate();
	}
	return -1;

}


double AmlPlayerManager::getPts(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getPts();
	}
	return -1;
}


void AmlPlayerManager::flush(BPNI_PLAYER_HANDLE_ID handleId)
{
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->flush();
	}
}

BPNI_MediaMetaData AmlPlayerManager::getMetaData(BPNI_PLAYER_HANDLE_ID handleId)
{
	BPNI_MediaMetaData meta;
	auto it = mMapSession.find(handleId);
	if ( it != mMapSession.end() )
	{
		return it->second->getMetaData();
	}
	return meta;
}

void AmlPlayerManager::stopAllPlayers()
{
	for (const auto& kv : mMapSession) {
		kv.second->destroyMpPlayer();
	}


}

void AmlPlayerManager::processEmm(const uint8_t* buffer, size_t size)
{
	//ALOGD("EMM size=%d", size);

	if(mICas == nullptr) return;
	hidl_vec<uint8_t> hidlEmm;
	hidlEmm.setToExternal((unsigned char *)buffer, size);
	auto returnStatus = mICas->processEmm(hidlEmm);
	if(not returnStatus.isOk())
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (android::hardware::cas::V1_0::Status)returnStatus);
}

void AmlPlayerManager::setPrivateData(const uint8_t* buffer, size_t size)
{
	//ALOGD("IMS size=%d", size);

	if(mICas == nullptr) return;
	hidl_vec<uint8_t> privateData;
	privateData.setToExternal((unsigned char *)buffer, size);
	auto returnStatus = mICas->setPrivateData(privateData);
	if(not returnStatus.isOk()) 
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (android::hardware::cas::V1_0::Status)returnStatus);
}


int AmlPlayerManager::EmmReceiver::writeData(const uint8_t* buffer, size_t size)
{
	AmlPlayerManager::instance().processEmm(buffer, size);
	return size;
}

int AmlPlayerManager::ImsReceiver::writeData(const uint8_t* buffer, size_t size)
{
	AmlPlayerManager::instance().setPrivateData(buffer, size);
	return size;
}


