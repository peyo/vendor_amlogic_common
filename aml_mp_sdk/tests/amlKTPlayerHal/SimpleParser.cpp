/*
 * Copyright (c) 2020 AALOGIc, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_NDEBUG 0
#define LOG_TAG "KTHALSimpleParser"
#include <utils/Log.h>
#include "SimpleParser.h"
#include <media/stagefright/foundation/ADebug.h>
#include <vector>
#include <utils/AmlMpUtils.h>

//namespace aml_mp {
///////////////////////////////////////////////////////////////////////////////
struct StreamType {
    int esStreamType;
    Aml_MP_StreamType mpStreamType;
    Aml_MP_CodecID codecId;
};

static const StreamType g_streamTypes[] = {
    {0x01, AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_MPEG12},
    {0x02, AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_MPEG12},
    {0x03, AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_MP3},
    {0x04, AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_MP3},
    {0x0f, AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_AAC},
    {0x10, AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_MPEG4},
    {0x11, AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_LATM},
    {0x1b, AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_H264},
    {0x24, AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_HEVC},
    {0x81, AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_AC3},
    {0x82, AML_MP_STREAM_TYPE_SUBTITLE, AML_MP_SUBTITLE_CODEC_SCTE27},
    {0, AML_MP_STREAM_TYPE_UNKNOWN, AML_MP_CODEC_UNKNOWN},
};

static const StreamType g_descTypes[] = {
    { 0x6a, AML_MP_STREAM_TYPE_AUDIO,    AML_MP_AUDIO_CODEC_AC3          }, /* AC-3 descriptor */
    { 0x7a, AML_MP_STREAM_TYPE_AUDIO,    AML_MP_AUDIO_CODEC_EAC3         }, /* E-AC-3 descriptor */
    { 0x7b, AML_MP_STREAM_TYPE_AUDIO,    AML_MP_AUDIO_CODEC_DTS          },
    { 0x56, AML_MP_STREAM_TYPE_SUBTITLE, AML_MP_SUBTITLE_CODEC_TELETEXT },
    { 0x59, AML_MP_STREAM_TYPE_SUBTITLE, AML_MP_SUBTITLE_CODEC_DVB }, /* subtitling descriptor */
    { 0, AML_MP_STREAM_TYPE_UNKNOWN, AML_MP_CODEC_UNKNOWN},
};

#define MKTAG(a,b,c,d) ((a) | ((b) << 8) | ((c) << 16) | ((unsigned)(d) << 24))
static const StreamType g_identifierTypes[] = {
    { MKTAG('A', 'C', '-', '3'), AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_AC3},
    { MKTAG('E', 'A', 'C', '3'), AML_MP_STREAM_TYPE_AUDIO, AML_MP_AUDIO_CODEC_EAC3},
    { MKTAG('H', 'E', 'V', 'C'), AML_MP_STREAM_TYPE_VIDEO, AML_MP_VIDEO_CODEC_HEVC},
    {0, AML_MP_STREAM_TYPE_UNKNOWN, AML_MP_CODEC_UNKNOWN},
};

static const struct StreamType* getStreamTypeInfo(int esStreamType, const StreamType* table = g_streamTypes)
{
    const struct StreamType* result = nullptr;
    const StreamType *s = table;

    for (; s->esStreamType; s++) {
        if (esStreamType == s->esStreamType) {
            result = s;
            break;
        }
    }

    return result;
}

void ProgramInfo::debugLog() const
{
    ALOGI("ProgramInfo: programNumber=%d, pid=%d", programNumber, pmtPid);
    for (auto it : videoStreams) {
        ALOGI("ProgramInfo: videoStream: pid:%d, codecId:%d", it.pid, it.codecId);
    }
    for (auto it : audioStreams) {
        ALOGI("ProgramInfo: audioStream: pid:%d, codecId:%d", it.pid, it.codecId);
    }
    for (auto it : subtitleStreams) {
        ALOGI("ProgramInfo: subtitleStream: pid:%d, codecId:%d", it.pid, it.codecId);
    }
    if(scrambled) {
        ALOGI("ProgramInfo: is scrambled, caSystemId:0x%04X, ecmPid:0x%04X, privateDataLength:%d", caSystemId, ecmPid[0], privateDataLength);
        std::string privateDataHex;
        char hex[3];
        for(int i = 0; i < privateDataLength; i++){
            snprintf(hex, sizeof(hex), "%02X", privateData[i]);
            privateDataHex.append(hex);
            privateDataHex.append(" ");
        }
        ALOGI("ProgramInfo: privateData: %s", privateDataHex.c_str());
    }
}

///////////////////////////////////////////////////////////////////////////////
SimpleParser::SimpleParser(int programNumber)
: mProgramNumber(programNumber)
, mProgramInfo(new ProgramInfo)
{

}

SimpleParser::~SimpleParser()
{

}


sptr<ProgramInfo> SimpleParser::getProgramInfo() const
{
    sptr<ProgramInfo> info = mProgramInfo;

    if (info && info->isComplete()) {
        return info;
    }

    return nullptr;
}



int SimpleParser::parsePmt(int pid, size_t size, const uint8_t* data)
{
    ALOGI("parsePmt, pid:%d, size:%d", pid, size);
    Section section(data, size);
    const uint8_t* p = section.data();
    //int table_id = p[0];
    int section_syntax_indicator = (p[1]&0x80) >> 7;
    if (section_syntax_indicator != 1) {
        ALOGE("pmt section_syntax_indicator CHECK failed!");
        return -1;
    }
    int section_length = (p[1] & 0x0F) << 4 | p[2];
    CHECK(section_length <= 4093);
    ALOGI("section_length = %d, size:%d", section_length, size);

    PMTSection results;
    results.origSection = &section;
    results.pmtPid = pid;

    p = section.advance(3);
    int programNumber = p[0]<<8 | p[1];
    results.programNumber = programNumber;
    //check version_number and current_next_indicator to skip same pmt
    results.version_number = p[2] & 0x3E;
    results.current_next_indicator = p[2] & 0x01;
    //ALOGI("pmt cb, version_number:%d, current_next_indicator:%d", results.version_number, results.current_next_indicator);
    if (results.current_next_indicator == 0) {
        //ALOGI("just skip this pmt, because the current_next_indicator is zero");
        return 0;
    }

    if(0){
        // check version_number is same
        auto it = mPidPmtMap.find(pid);
        if (it != mPidPmtMap.end()) {
            if (results.version_number == it->second.version_number) {
                //ALOGI("just skip this pmt, because the version_number(%d) is same", results.version_number);
                return 0;
            }
        }
    }

    p = section.advance(5);
    int pcr_pid = (p[0]&0x1F)<<8 | p[1];
    results.pcrPid = pcr_pid;
    int program_info_length = (p[2]&0x0F) << 8 | p[3];
    CHECK(program_info_length < 1024);
    p = section.advance(4);

    results.privateDataLength = 0;
    if (program_info_length > 0) {
        int descriptorsRemaining = program_info_length;
        const uint8_t* p2 = p;
        int count = 0;
        while (descriptorsRemaining >= 2) {
            int descriptor_tag = p2[0];
            int descriptor_length = p2[1];
            switch (descriptor_tag) {
            case 0x09:
            {
                int ca_system_id = p2[2]<<8 | p2[3];
                int ecm_pid = (p2[4]&0x1F)<<8 | p2[5];
                ALOGI("ca_system_id:%#x, ecm_pid:%#x, count:%d, descriptor_length:%d\n", ca_system_id, ecm_pid, ++count, descriptor_length);

                results.scrambled = true;
                results.caSystemId = ca_system_id;
                results.ecmPid = ecm_pid;
                results.privateDataLength = descriptor_length - 4;
                memcpy(results.privateData, &p2[6], results.privateDataLength);
            }
            break;

            case 0x65:
            {
                int scrambleAlgorithm = p[2];
                ALOGI("scrambleAlgorithm:%d", scrambleAlgorithm);

                results.scrambleAlgorithm = scrambleAlgorithm;
            }
            break;

            default:
                ALOGI("descriptor_tag:%#x", descriptor_tag);
                break;
            }

            p2 += 2 + descriptor_length;
            descriptorsRemaining -= 2 + descriptor_length;
        }
    }

    //skip program info
    p = section.advance(program_info_length);
    int infoBytesRemaining = section.dataSize() - 4;

    while (infoBytesRemaining >= 5) {
        int stream_type = p[0];
        int elementary_pid = (p[1]&0x1F) << 8 | p[2];
        int es_info_length = (p[3]&0x0F) << 8 | p[4];
        p = section.advance(5);
        infoBytesRemaining -= 5;

        PMTStream esStream;
        esStream.streamType = stream_type;
        esStream.streamPid = elementary_pid;

        if (es_info_length > 0) {
            int descriptorsRemaining = es_info_length;
            const uint8_t* p2 = p;
            int count = 0;
            while (descriptorsRemaining >= 2) {
                int descriptor_tag = p2[0];
                int descriptor_length = p2[1];

                esStream.descriptorTags[esStream.descriptorCount++] = descriptor_tag;

                switch (descriptor_tag) {
                case 0x09:
                {
                    int ca_system_id = p2[2]<<8 | p2[3];
                    int ecm_pid = (p2[4]&0x1F)<<8 | p2[4];
                    ALOGI("streamType:%#x, pid:%d, ca_system_id:%#x, ecm_pid:%#x, count:%d, descriptor_length:%d\n",
                            stream_type, elementary_pid, ca_system_id, ecm_pid, ++count, descriptor_length);
                    if (descriptor_length > 4) {
                        int has_iv = p2[6] & 0x1;
                        int aligned = ( p2[6] & 0x4 ) >> 2;
                        int scramble_mode = ( p2[6] & 0x8 ) >> 3;
                        int algorithm = ( p2[6] & 0xE0 ) >> 5;
                        ALOGI("%s, @@this is ca_private_data, data:%#x, iv:%d, aligned:%d, mode:%d, algo:%d",
                                  __FUNCTION__, p2[6], has_iv, aligned, scramble_mode, algorithm );
                        if ( algorithm == 1 ) {
                            results.scrambleInfo.algo = SCRAMBLE_ALGO_AES;
                            if ( scramble_mode == 0 ) {
                                results.scrambleInfo.mode = SCRAMBLE_MODE_ECB;
                            } else {
                                results.scrambleInfo.mode = SCRAMBLE_MODE_CBC;
                            }
                            results.scrambleInfo.alignment = (SCRAMBLE_ALIGNMENT_t)aligned;
                            results.scrambleInfo.has_iv_value = has_iv;
                            if ( has_iv && descriptor_length > 4 + 16 ) {
                                memcpy(results.scrambleInfo.iv_value_data, &p2[7], 16);
                            }
                        }
                    }

                    results.scrambled = true;
                    results.caSystemId = ca_system_id;

                    esStream.ecmPid = ecm_pid;
                }
                break;

                case 0x59:
                {
                    int language_count = descriptor_length / 8;
                    if (language_count > 0) {
                        esStream.compositionPageId = p2[6] << 8 | p2[7];
                        esStream.ancillaryPageId = p2[7] << 8 | p2[9];
                        ALOGI("compositionPageId:%#x, ancillaryPageId:%#x", esStream.compositionPageId, esStream.ancillaryPageId);
                    }

                }
                break;

                case 0x05:
                {
                    int32_t formatIdentifier = MKTAG(p2[2], p2[3], p2[4], p2[5]);
                    ALOGI("formatIdentifier:%#x, %x %x %x %x", formatIdentifier, p2[2], p2[3], p2[4], p2[5]);
                    esStream.descriptorTags[esStream.descriptorCount-1] = formatIdentifier;
                }
                break;

                default:
                    ALOGI("unhandled stream descriptor_tag:%#x, length:%d", descriptor_tag, descriptor_length);
                    break;
                }

                p2 += 2 + descriptor_length;
                descriptorsRemaining -= 2 + descriptor_length;
            }

            p = section.advance(es_info_length);
            infoBytesRemaining -= es_info_length;
        }

        results.streamCount++;
        results.streams.push_back(esStream);
        ALOGE("programNumber:%d, stream pid:%d, type:%#x\n", programNumber, elementary_pid, stream_type);

    }

    onPmtParsed(results);

    return 0;
}

void SimpleParser::onPmtParsed(const PMTSection& results)
{
    if (results.streamCount == 0) {
        return;
    }

    Aml_MP_PlayerEventPidChangeInfo pidChangeInfo;
    sptr<ProgramInfo> programInfo = mProgramInfo;
    results.origSection->resetBegin();
    programInfo->setPmtSectionData(results.origSection->data(), results.origSection->dataSize());
    programInfo->programNumber = results.programNumber;
    programInfo->pmtPid = results.pmtPid;
    programInfo->caSystemId = results.caSystemId;
    programInfo->scrambled = results.scrambled;
    programInfo->scrambleInfo = results.scrambleInfo;
    programInfo->serviceIndex = 0;
    programInfo->serviceNum = 0;
    programInfo->ecmPid[ECM_INDEX_AUDIO] = results.ecmPid;
    programInfo->ecmPid[ECM_INDEX_VIDEO] = results.ecmPid;
    programInfo->ecmPid[ECM_INDEX_SUB] = results.ecmPid;
    programInfo->privateDataLength = results.privateDataLength;
    memcpy(programInfo->privateData, results.privateData, results.privateDataLength);


    const struct StreamType* typeInfo;
    for (auto it : results.streams) {
        PMTStream* stream = &it;
        typeInfo = getStreamTypeInfo(stream->streamType);
        if (typeInfo == nullptr) {
            for (size_t j = 0; j < stream->descriptorCount; ++j) {
                typeInfo = getStreamTypeInfo(stream->descriptorTags[j], g_descTypes);
                if (typeInfo != nullptr) {
                    ALOGI("stream pid:%d, found tag:%#x", stream->streamPid, stream->descriptorTags[j]);
                    break;
                }

                typeInfo = getStreamTypeInfo(stream->descriptorTags[j], g_identifierTypes);
                if (typeInfo != nullptr) {
                    ALOGI("identified stream pid:%d, %.4s", stream->streamPid, (char*)&stream->descriptorTags[j]);
                    break;
                }
            }
        }

        if (typeInfo == nullptr) {
            continue;
        }

        switch (typeInfo->mpStreamType) {
        case AML_MP_STREAM_TYPE_AUDIO:
        {
            if (programInfo->audioPid == AML_MP_INVALID_PID) {
                programInfo->audioPid = stream->streamPid;
                programInfo->audioCodec = typeInfo->codecId;
                if (stream->ecmPid != AML_MP_INVALID_PID) {
                    programInfo->ecmPid[ECM_INDEX_AUDIO] = stream->ecmPid;
                }
            }
            StreamInfo streamInfo;
            streamInfo.type = TYPE_AUDIO;
            streamInfo.pid = stream->streamPid;
            streamInfo.codecId = typeInfo->codecId;
            programInfo->audioStreams.push_back(streamInfo);
            ALOGI("audio pid:%d(%#x), codec:%s", streamInfo.pid, streamInfo.pid, mpCodecId2Str(streamInfo.codecId));
            break;
        }

        case AML_MP_STREAM_TYPE_VIDEO:
        {
            if (programInfo->videoPid == AML_MP_INVALID_PID) {
                programInfo->videoPid = stream->streamPid;
                programInfo->videoCodec = typeInfo->codecId;
                if (stream->ecmPid != AML_MP_INVALID_PID) {
                    programInfo->ecmPid[ECM_INDEX_VIDEO] = stream->ecmPid;
                }
            }
            StreamInfo streamInfo;
            streamInfo.type = TYPE_VIDEO;
            streamInfo.pid = stream->streamPid;
            streamInfo.codecId = typeInfo->codecId;
            programInfo->videoStreams.push_back(streamInfo);
            ALOGI("video pid:%d(%#x), codec:%s", streamInfo.pid, streamInfo.pid, mpCodecId2Str(streamInfo.codecId));
            break;
        }

        case AML_MP_STREAM_TYPE_SUBTITLE:
        {
            if (programInfo->subtitlePid == AML_MP_INVALID_PID) {
                programInfo->subtitlePid = stream->streamPid;
                programInfo->subtitleCodec = typeInfo->codecId;
                if (stream->ecmPid != AML_MP_INVALID_PID) {
                    programInfo->ecmPid[ECM_INDEX_SUB] = stream->ecmPid;
                }
                if (programInfo->subtitleCodec == AML_MP_SUBTITLE_CODEC_DVB) {
                    programInfo->compositionPageId = stream->compositionPageId;
                    programInfo->ancillaryPageId = stream->ancillaryPageId;
                }
            }
            StreamInfo streamInfo;
            streamInfo.type = TYPE_SUBTITLE;
            streamInfo.pid = stream->streamPid;
            streamInfo.codecId = typeInfo->codecId;
            if (streamInfo.codecId == AML_MP_SUBTITLE_CODEC_DVB) {
                streamInfo.compositionPageId = stream->compositionPageId;
                streamInfo.ancillaryPageId = stream->ancillaryPageId;
            }
            programInfo->subtitleStreams.push_back(streamInfo);
            ALOGI("subtitle pid:%d(%#x), codec:%s", streamInfo.pid, streamInfo.pid, mpCodecId2Str(streamInfo.codecId));
            break;
        }

        case AML_MP_STREAM_TYPE_AD:
            break;

        default:
            break;
        }
    }
}



//}
