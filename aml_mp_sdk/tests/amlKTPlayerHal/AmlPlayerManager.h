/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _AML_PLAYER_MANAGER_H_
#define _AML_PLAYER_MANAGER_H_
#include <utils/AmlMpRefBase.h>
#include <Aml_MP/Aml_MP.h>
#include <AmlMpPlayerWrapper.h>
#include "KtCasCommon.h"
#include "AmKtCasIPTVImpl.h"
#include "Source.h"


class EmmReceiver;
class ImsReceiver;


class AmlPlayerManager
{
public:
	static AmlPlayerManager& instance() {
		static AmlPlayerManager* instance = new AmlPlayerManager();
		return *instance;
	};

	void registerPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback);
	void setTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId);
	int getTunerId(BPNI_PLAYER_HANDLE_ID handleId);
	void feedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len);
	void setSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);
	ANativeWindow* getSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId);
	int prepare(BPNI_PLAYER_HANDLE_ID handleId);
	void setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len);
	int casChannelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list);
	void casMDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, std::string& descriptor_blob_input, int descriptor_blob_input_length);
	void casMDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId);
	int32_t casSetPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, int32_t position, int32_t speed);
	int32_t casSendCasEvent(std::string& jsonStr);
	int32_t casSetEMMAddress(std::string& addr);
	
	int playStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID);
	int stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop);
	int setVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume);
	float getVolume(BPNI_PLAYER_HANDLE_ID handleId);
	int32_t setAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled);
	bool getAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId);
	int setRate(BPNI_PLAYER_HANDLE_ID handleId, float rate);
	float getRate(BPNI_PLAYER_HANDLE_ID handleId);
	double getPts(BPNI_PLAYER_HANDLE_ID handleId);
	void flush(BPNI_PLAYER_HANDLE_ID handleId);
	BPNI_MediaMetaData getMetaData(BPNI_PLAYER_HANDLE_ID handleId);
	void stopAllPlayers();


	// ICasListener is called
	virtual void onCasEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data);

	//global CAS
	void processEmm(const uint8_t* buffer, size_t size);
	void setPrivateData(const uint8_t* buffer, size_t size);

private:
	AmlPlayerManager() { init(); };
	~AmlPlayerManager() {};
	void init();
	void initMediaCas();
	bool createPlugin(const sp<ICasListener>& listener);


	class EmmReceiver : public ISourceReceiver
	{
	public:
		virtual int writeData(const uint8_t* buffer, size_t size) override;
	};
	
	class ImsReceiver : public ISourceReceiver
	{
	public:
		virtual int writeData(const uint8_t* buffer, size_t size) override;
	};


	sp<ICas> mICas = nullptr;
	sp<ICasListener> mCasListener = nullptr;
	std::shared_ptr<AmKtCasIPTVImpl> mIptvCas = nullptr;
	std::string casMcasUrl;
	std::string mEmmUrl;
	std::string mImsUrl;
	sptr<Source> mEmmSource;	//udp mcast
	sptr<Source> mImsSource;	//udp mcast(KTCAS private)
	sptr<EmmReceiver>	mEmmReceiver;
	sptr<ImsReceiver>	mImsReceiver;
	
	std::map<int32_t, std::shared_ptr<AmlMpPlayerWrapper>> mMapSession;  //TODO: using vector? not sure JNI player callsequence/pattern for multiview/PIP

	static android::sp<IMediaCasService> gCasService;


};

#endif

