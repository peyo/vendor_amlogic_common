/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_TAG "KTHALAmlKTAltiIptvCas"
#include <utils/Log.h>
#include <utils/AmlMpUtils.h>
#include "AmlKTAltiIptvCas.h"
#include <dlfcn.h>
#include <cutils/properties.h>
#include "utils/Amlsysfsutils.h"
#include <unistd.h>
#include <utils/AmlMpLog.h>
#include "AmKtCasIPTVImpl.h"

namespace aml_mp {

AmlKTAltiIptvCas::AmlKTAltiIptvCas(const Aml_MP_IptvCasParams* param, int instanceId, sp<ICas>& ICas)
: mIptvCasParam(*param)
, mInstanceId(instanceId)
{
    pIptvCas = std::make_shared<AmKtCasIPTVImpl>(ICas);
    if (pIptvCas == nullptr) {
        MLOGE(" call AmKtCasIPTVImpl() failed");
        return;
    }
    mEventListenerProxy = std::make_shared<AmKtCasIPTVImpl::EventListenerProxy>(this);
	pIptvCas->setEventListener(mEventListenerProxy);
    mFirstEcm = 0;
}

AmlKTAltiIptvCas::~AmlKTAltiIptvCas()
{
    int ret = 0;
	closeSession();
    if (pIptvCas) {
        pIptvCas.reset();
    }

}

int AmlKTAltiIptvCas::openSession()
{
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->openSession();
    }
    return ret;
}

int AmlKTAltiIptvCas::closeSession()
{
    MLOGI("closeSession");
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->closeSession();
    }
    return ret;
}

int AmlKTAltiIptvCas::setPrivateData(const uint8_t* data, size_t size)
{
    int ret = 0;

    if (pIptvCas) {
        ret = pIptvCas->setPrivateData((uint8_t*)data, size);
        if (ret != 0) {
            MLOGI("setPrivateData failed, ret =%d", ret);
            return ret;
        }
    }

    return ret;
}

int AmlKTAltiIptvCas::checkEcmProcess(uint8_t* pBuffer, uint32_t vEcmPid, uint32_t aEcmPid,size_t * nSize)
{
  int ret = 0;
  int len = 0,pid = 0;
  unsigned int rem = *nSize;

  uint8_t * psync = pBuffer;
  uint8_t * current = NULL;

  while (rem >= TS_PACKET_SIZE)
  {
      if (*psync != 0x47)
      {
          ++psync;
          --rem;
          continue;
      }
      if ((*(psync) == 0x47) && ((rem == TS_PACKET_SIZE) || (*(psync+TS_PACKET_SIZE) == 0x47)))
      {
          current = psync;
          pid = (( current[1] << 8 | current[2]) & 0x1FFF);
          if (pid == vEcmPid || pid == aEcmPid)
          {
              if (memcmp(mEcmTsPacket + 4,psync + 4,TS_PACKET_SIZE- 4))
              {
                  memcpy(mEcmTsPacket, psync, TS_PACKET_SIZE);
                  std::string ecmDataStr;
                  char hex[3];
                  for (int i = 0; i < 64; i++) {
                      snprintf(hex, sizeof(hex), "%02X", mEcmTsPacket[i]);
                      ecmDataStr.append(hex);
                      ecmDataStr.append(" ");
                  }
                  MLOGI("checkEcmProcess, ecmDataStr.c_str()=%s", ecmDataStr.c_str());
                  if (pIptvCas)
                      ret = pIptvCas->processEcm(0, pid, mEcmTsPacket, TS_PACKET_SIZE);
              }
              if (mFirstEcm != 1) {
                  MLOGI("first_SetECM find\n");
                  mFirstEcm = 1;
              }
          }
      }
      psync += TS_PACKET_SIZE;
      rem -= TS_PACKET_SIZE;
  }

  return ret;
}


int AmlKTAltiIptvCas::processEcm(const uint8_t* data, size_t size)
{
    int ret = 0;

    //==============FIXME=============
    if (pIptvCas) {
        uint8_t *pdata = const_cast<uint8_t *>(data);
        MLOGV("%s, pid=0x%x, size=%zu", __func__, mIptvCasParam.videoPid, size);
        if (size < 188) {
            MLOGI("%s, pid=0x%x, size=%zu", __func__, mIptvCasParam.videoPid, size);
            ret = pIptvCas->processEcm(1, mIptvCasParam.ecmPid[0] ,pdata, size);
        } else {
            checkEcmProcess(pdata, mIptvCasParam.ecmPid[0], mIptvCasParam.ecmPid[1], &size);
        }
    }

    return ret;
}

int AmlKTAltiIptvCas::processEmm(const uint8_t* data, size_t size)
{
    int ret = 0;

    if (pIptvCas) {
        uint8_t *pdata = const_cast<uint8_t *>(data);
        ret = pIptvCas->processEmm(0, mIptvCasParam.videoPid ,pdata, size);
    }

    return ret;
}


int AmlKTAltiIptvCas::provision(std::string& pssh)
{
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->provision(pssh);
    }
    return ret;

}

int AmlKTAltiIptvCas::setPids(std::vector<int>& pids)
{
	mPids.clear();
	mPids.assign(pids.begin(), pids.end());
	return 0;
}

int AmlKTAltiIptvCas::sendEvent(int event, int arg, uint8_t *pBuffer, int iBufferLength)
{
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->sendEvent(event, arg, pBuffer, iBufferLength);
    }
    return ret;
}

int AmlKTAltiIptvCas::setPrivateData(uint8_t* pBuffer, size_t iBufferLength)
{
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->setPrivateData(pBuffer, iBufferLength);
    }
    return ret;

}

int AmlKTAltiIptvCas::setSessionPrivateData(uint8_t* pBuffer, size_t iBufferLength)
{
    int ret = 0;
    if (pIptvCas) {
        ret = pIptvCas->setSessionPrivateData(pBuffer, iBufferLength);
    }
    return ret;
}

void AmlKTAltiIptvCas::onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data)
{

}


}
