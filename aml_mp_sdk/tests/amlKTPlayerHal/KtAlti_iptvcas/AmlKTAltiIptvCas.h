/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _AML_KT_ALTI_IPTV_CAS_H_
#define _AML_KT_ALTI_IPTV_CAS_H_

#include <mutex>
#include <vector>
#include <Aml_MP/Common.h>
#include "cas/AmlCasBase.h"
#include "AmKtCasIPTVImpl.h"
#include "KtCasCommon.h"

namespace aml_mp {


//template<typename T> using uptr = std::unique_ptr<T>;
//template<typename T> using sptr = std::shared_ptr<T>;
//template<typename T> using wptr = std::weak_ptr<T>;


class AmlKTAltiIptvCas : public AmlCasBase, public AmKtCasIPTVImpl::EventListener
{
public:
    AmlKTAltiIptvCas(const Aml_MP_IptvCasParams* param, int instanceId, sp<ICas>& ICas);
    ~AmlKTAltiIptvCas();
	virtual int openSession() override;
    virtual int closeSession() override;
    virtual int setPrivateData(const uint8_t* data, size_t size) override;
    virtual int processEcm(const uint8_t* data, size_t size) override;
    virtual int processEmm(const uint8_t* data, size_t size) override;
	int registerEventCallback(Aml_MP_CAS_EventCallback cb, void* userData)	{
		if(cb){
			mEventCb = cb; 
			mUserData = userData;
		}
		return 0;
	};

	//more apis
    int provision(std::string& pssh);
    int setPids(std::vector<int>&    vecPid);
    int sendEvent(int event, int arg, uint8_t *pBuffer, int iBufferLength);
	int setPrivateData(uint8_t* pBuffer, size_t iBufferLength);
	int setSessionPrivateData(uint8_t* pBuffer, size_t iBufferLength);

	//event from plugin -> move to manager
	void onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data);

private:
	int checkEcmProcess(uint8_t* pBuffer, uint32_t vEcmPid, uint32_t aEcmPid,size_t * nSize);

    Aml_MP_IptvCasParams mIptvCasParam;
    std::shared_ptr<AmKtCasIPTVImpl> pIptvCas = nullptr;
    std::shared_ptr<AmKtCasIPTVImpl::EventListener> mEventListenerProxy;
    Aml_MP_CAS_EventCallback mEventCb = nullptr;
    void* mUserData = nullptr;

    std::vector<uint8_t> mSessionId;
    std::vector<int>    mPids;
    int mInstanceId;
    char mName[64];

    int mFirstEcm;
    uint8_t mEcmTsPacket[188];

    AmlKTAltiIptvCas(const AmlKTAltiIptvCas&) = delete;
    AmlKTAltiIptvCas& operator= (const AmlKTAltiIptvCas&) = delete;
};

}

#endif
