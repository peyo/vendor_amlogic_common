/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_TAG "KTHALAltiCasHelper"
#include <utils/Log.h>
#include <utils/AmlMpUtils.h>
#include "AltiCasHelper.h"
#include <json/json.h>

namespace aml_mp {

static UniqueIdGenerator<unsigned int, 0, 0xFFFFFFFF> gEventIdGenerator;


AltiCasHelper::AltiCasHelper()
{

}

AltiCasHelper::~AltiCasHelper()
{

}

std::string AltiCasHelper::buildCasChannelChange(const cas_channel_change_info& info)
{
//int service_id, char* url, int type, int number_of_components, int* pid_list
	Json::Value root;
	root["event_id"]	= gEventIdGenerator.next();
	root["type"]		= "CaFunction";

    Json::Value format;
	format["function"]="ChannelChange";

	Json::Value arguments;
	arguments["connection"] = info.connection;
	arguments["service_id"] = info.service_id;
	std::string url(info.url);
	arguments["fname"] = url;
	arguments["type"] = info.type;
	arguments["number_of_components"] = info.number_of_components;
	Json::Value pid_list;
	for(int i=0; i<info.number_of_components; i++){
		int pid = *(info.pid_list+i);
		arguments["pid_list"].append(pid);
	}
	format["arguments"].append(arguments);
	root["format"].append(format);

	//write
    Json::FastWriter writer;
	std::string output = writer.write(root);
	ALOGD("==CAS Channel Change==");
	ALOGD("%s", output.c_str());

	return output;
}



void json2Map(STRING_MAP &m, const std::string &j)
{
    Json::Reader reader;
    Json::Value root;
    Json::FastWriter writer;

    if (!reader.parse(j, root))
        return;

    for (Json::Value::iterator it = root.begin(); it != root.end(); it++) {
        std::string v(writer.write(*it));
        if (v.compare(v.size()-1, 1, "\n") == 0)
            v.erase(v.size()-1);
        if (v[0] == '\"')
            m.insert(STRING_MAP::value_type(it.key().asString(), v.substr(1, v.size()-2)));
        else
            m.insert(STRING_MAP::value_type(it.key().asString(), v));
    }
}

void map2Json(std::string &j, const STRING_MAP &m)
{
    int has_member = 0;

    if (m.empty())
        return;

    j.append("{");
    for (STRING_MAP::const_iterator it = m.begin(); it != m.end(); ++it) {
        if (has_member)
            j.append(",");
        j.append("\"").append(it->first).append("\":").append(it->second);
        has_member = 1;
    }
    j.append("}");
}

void map2Json(std::string &j, const STRING_MAP &m, const std::string &k)
{
    if (m.empty())
        return;

    if (k.size())
        j.append("\"").append(k).append("\":");

    map2Json(j, m);
}

void map2JsonAppend(std::string &j, const STRING_MAP &m, const std::string &k)
{
    if (m.empty())
        return;

    int append = 0;

    if (!j.empty()) {
        append = 1;
        j.replace(j.size()-1, 1, 1, ',');
    }

    map2Json(j, m, k);

    if (append)
        j.append("}");
}

CasJsonParams CasJsonParams::operator + (const CasJsonParams &p)
{
    CasJsonParams pnew(*this);
    for (STRING_MAP::const_iterator it = p.mparas.begin(); it != p.mparas.end(); ++it)
        pnew.mparas.insert(*it);
    return pnew;
}

int CasJsonParams::getInt(const char *key, int def) const
{
    STRING_MAP::const_iterator it = mparas.find(std::string(key));
    if (it == mparas.end())
        return def;
    return atoi(it->second.c_str());
}

void CasJsonParams::setInt(const char *key, int v)
{
    char cs[64];
    sprintf(cs, "%d", v);
    STRING_MAP::iterator it = mparas.find(std::string(key));
    if (it != mparas.end()) {
        it->second.assign(cs);
    } else {
        std::pair<std::map<std::string, std::string>::iterator, bool> ret;
        ret = mparas.insert(std::pair<std::string, std::string>(std::string(key), std::string(cs)));
        if (ret.second == false) {
            ALOGE("error: map can not insert");
        }
    }
}

}
