/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */


#ifndef _KT_CAS_COMMON_H_
#define _KT_CAS_COMMON_H_

#include <android/hardware/cas/1.0/types.h>
#include <android/hardware/cas/1.0/ICas.h>
#include <android/hardware/cas/1.0/ICasListener.h>
#include <android/hardware/cas/1.0/IMediaCasService.h>
#include <android/hardware/cas/native/1.0/IDescrambler.h>
#include <android/hardware/cas/native/1.0/types.h>
#include <utils/RefBase.h>
#include <hidl/Status.h>
#include <hidl/HidlSupport.h>


using namespace android;
using namespace android::hardware;
using namespace android::hardware::cas;
using namespace android::hardware::cas::V1_0;

using android::sp;
using android::wp;
using hardware::hidl_vec;
using hardware::Return;
using android::hardware::cas::V1_0::IMediaCasService;
using android::hardware::cas::V1_0::ICas;
using android::hardware::cas::V1_0::ICasListener;
using android::hardware::cas::V1_0::Status;


#ifndef KTALTICAST_CA_SYSTEM_ID
#define KTALTICAST_CA_SYSTEM_ID		0x0E01
#endif


typedef enum
{
	AM_CAS_SUCCESS,
	AM_CAS_ERROR,
	AM_CAS_ERR_SYS,
} AmCasCode_t;




#endif