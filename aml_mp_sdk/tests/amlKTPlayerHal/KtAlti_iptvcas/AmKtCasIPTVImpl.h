/*
 * Copyright (c) 2019 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef AMKTCAS_IPTV_IMPL_H
#define AMKTCAS_IPTV_IMPL_H

#include <mutex>
#include <vector>
#include <utils/RefBase.h>
#include <hidl/Status.h>
#include <hidl/HidlSupport.h>


#include "cas/AmlCasBase.h"
#include "KtCasCommon.h"

namespace aml_mp {
using namespace android;
using namespace android::hardware;
using namespace android::hardware::cas;
using namespace android::hardware::cas::V1_0;

using android::sp;
using android::wp;
using hardware::hidl_vec;
using hardware::Return;
using android::hardware::cas::V1_0::IMediaCasService;
using android::hardware::cas::V1_0::ICas;
using android::hardware::cas::V1_0::ICasListener;
using android::hardware::cas::V1_0::Status;




class  AmKtCasIPTVImpl //: public AmCasIPTV
{
public:
	//class Session; //remove: AmKtCasIPTVImpl is 1:1 session
	class EventListener;
	class EventListenerProxy;

public:
    AmKtCasIPTVImpl(sp<ICas>& ICas);
    virtual ~AmKtCasIPTVImpl();
    AmCasCode_t provision(std::string& pssh);
    AmCasCode_t openSession();
    AmCasCode_t closeSession();
    //AmCasCode_t setPids(int vPid, int aPid);
    AmCasCode_t processEcm(int isSection, int iPid, uint8_t*pBuffer, int iBufferLength);
    AmCasCode_t processEmm(int isSection, int iPid, uint8_t *pBuffer, int iBufferLength);
	AmCasCode_t sendEvent(int event, int arg, uint8_t *pBuffer, int iBufferLength);
    AmCasCode_t setPrivateData(uint8_t *pBuffer, int iBufferLength);
	AmCasCode_t setSessionPrivateData(uint8_t *pBuffer, int iBufferLength);
	void setEventListener(std::shared_ptr<EventListener> & listener) { mListener = listener; }

	// ICasListener(casListenerImpl) will call
	//virtual void onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data);


private:
    void init();

	sp<ICas> mICas = nullptr;
	std::weak_ptr<EventListener> mListener;
	std::vector<uint8_t> mSessionId;

};

// all event will handled at manager, not session.
class AmKtCasIPTVImpl::EventListener
{
public:
	EventListener(){};
	virtual ~EventListener(){};
	virtual void onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data) = 0;
};

class AmKtCasIPTVImpl::EventListenerProxy : public AmKtCasIPTVImpl::EventListener
{
private:
	AmKtCasIPTVImpl::EventListener * mListener;
public:
	EventListenerProxy(AmKtCasIPTVImpl::EventListener * listener){ mListener = listener; };
	virtual ~EventListenerProxy(){};
	virtual void onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data) override
	{
		ALOGD("AmKtCasIPTVImpl::EventListenerProxy event=%d", event);
		mListener->onEvent(event, arg, data);
	}
};



}

#endif
