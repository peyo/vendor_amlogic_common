/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _ALTICAS_HELPER_H_
#define _ALTICAS_HELPER_H_

#include "cas/AmlCasBase.h"
#include "KtCasCommon.h"
#include <Aml_MP/Common.h>
#include <map>
#include <mutex>


namespace aml_mp {

typedef std::map<std::string, std::string> STRING_MAP;
extern void json2Map(STRING_MAP &m, const std::string &j);
extern void map2Json(std::string &j, const STRING_MAP &m);
extern void map2Json(std::string &j, const STRING_MAP &m, const std::string &k);
extern void map2JsonAppend(std::string &j, const STRING_MAP &m, const std::string &k);



struct cas_channel_change_info
{
	int connection;
	int service_id;
	char* url;
	int type;
	int number_of_components;
	int* pid_list;
};

template<typename T, T min, T max>
struct UniqueIdGenerator
{
	T next()
	{
		mValue++;
		if(mValue >= max)
			mValue = min;
		return mValue;
	}

private:
	T mValue = min;
};


class AltiCasHelper 
{
public:
    AltiCasHelper();
    ~AltiCasHelper();
	static std::string buildCasChannelChange(const cas_channel_change_info& info);

private:

    AltiCasHelper(const AltiCasHelper&) = delete;
    AltiCasHelper& operator= (const AltiCasHelper&) = delete;
};


class CasJsonParams {
protected:
    STRING_MAP mparas;

public:
    CasJsonParams() {}
    CasJsonParams(const CasJsonParams &paras):mparas(paras.mparas) {}
    CasJsonParams(const char *paras) { json2Map(mparas, std::string(paras)); }
    CasJsonParams(const STRING_MAP &paras):mparas(paras) {}
    void clear() { mparas.clear(); }

    int getInt (const char *key, int def) const;
    void setInt(const char *key, int v);

    const std::string toString() { std::string s; map2Json(s, mparas); return s; }

    CasJsonParams operator + (const CasJsonParams &p);
    CasJsonParams& operator = (const CasJsonParams &p) { mparas = p.mparas; return *this; };
};


}

#endif
