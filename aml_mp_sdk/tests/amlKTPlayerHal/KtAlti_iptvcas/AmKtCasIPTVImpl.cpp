/*
 * Copyright (c) 2019 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
#define LOG_TAG "KTHALAmlKtCasImpl"
#include <utils/Log.h>
#include "AmKtCasIPTVImpl.h"


namespace aml_mp {




AmKtCasIPTVImpl::AmKtCasIPTVImpl(sp<ICas>& ICas)
:mICas(ICas)
{
	init();
}
AmKtCasIPTVImpl::~AmKtCasIPTVImpl()
{

}

void AmKtCasIPTVImpl::init()
{

}



AmCasCode_t AmKtCasIPTVImpl::provision(std::string& pssh)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_string provision_str = pssh;
	ALOGD("%s string ==> %s", __func__, provision_str.c_str());
	auto returnStatus = mICas->provision(provision_str);

	if (!returnStatus.isOk() || returnStatus != Status::OK) {
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
		return AM_CAS_ERROR;
	}
	ALOGD("%s success", __func__);
	return AM_CAS_SUCCESS;
}


AmCasCode_t AmKtCasIPTVImpl::openSession()
{
	std::vector<uint8_t> sessionId;

	Status sessionStatus;
	if(mICas == nullptr)
		return AM_CAS_ERROR;
	auto returnStatus = mICas->openSession(
		[&sessionStatus, &sessionId] (Status _status, const hidl_vec<uint8_t>& _sessionId) {
		sessionStatus = _status;
		sessionId = _sessionId;
	});
	if (!returnStatus.isOk() || sessionStatus != Status::OK) {
		if(not returnStatus.isOk())
		{
			ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)sessionStatus);
			return AM_CAS_ERROR;
		}
	}
	mSessionId = sessionId;
	return AM_CAS_SUCCESS;
}
AmCasCode_t AmKtCasIPTVImpl::closeSession()
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	auto returnStatus = mICas->closeSession(mSessionId);
	if(not returnStatus.isOk())
		ALOGW("%s fail: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;
}

//AmCasCode_t AmKtCasIPTVImpl::setPids(int vPid, int aPid)
//{
//
//
//	return AM_CAS_SUCCESS;
//}

AmCasCode_t AmKtCasIPTVImpl::processEcm(int isSection, int iPid, uint8_t*pBuffer, int iBufferLength)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_vec<uint8_t> hidlEcm;
	hidlEcm.setToExternal(pBuffer, iBufferLength);
	auto returnStatus = mICas->processEcm(mSessionId, hidlEcm);
	if(not returnStatus.isOk())
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;


}
AmCasCode_t AmKtCasIPTVImpl::processEmm(int isSection, int iPid, uint8_t *pBuffer, int iBufferLength)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_vec<uint8_t> hidlEmm;
	hidlEmm.setToExternal(pBuffer, iBufferLength);
	auto returnStatus = mICas->processEmm(hidlEmm);
	if(not returnStatus.isOk())
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;
}
AmCasCode_t AmKtCasIPTVImpl::sendEvent(int event, int arg, uint8_t *pBuffer, int iBufferLength)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_vec<uint8_t> eventData;
	eventData.setToExternal((uint8_t*)pBuffer, iBufferLength);
    auto returnStatus = mICas->sendEvent(event, arg, eventData);
	if(not returnStatus.isOk())
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;
}
AmCasCode_t AmKtCasIPTVImpl::setPrivateData(uint8_t *pBuffer, int iBufferLength)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_vec<uint8_t> privateData;
	privateData.setToExternal(pBuffer, iBufferLength);

	auto returnStatus = mICas->setPrivateData(privateData);
	if(not returnStatus.isOk()) 
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;
}

AmCasCode_t AmKtCasIPTVImpl::setSessionPrivateData(uint8_t *pBuffer, int iBufferLength)
{
	if(mICas == nullptr) return AM_CAS_ERROR;
	hidl_vec<uint8_t> privateData;
	privateData.setToExternal(pBuffer, iBufferLength);
	auto returnStatus = mICas->setSessionPrivateData(mSessionId, privateData);
	if(not returnStatus.isOk()) 
		ALOGW("Failed %s: trans=%s, status=%d",__func__, returnStatus.description().c_str(), (Status)returnStatus);
	return AM_CAS_SUCCESS;
}


//
//void AmKtCasIPTVImpl::onEvent(int32_t event, int32_t arg, std::vector<uint8_t>& data)
//{
//	//TODO: async event process
//	if (auto listener = mListener.lock())
//	{
//		ALOGD("sending listener--->");
//		listener->onEvent(event, arg, data);
//	}
//}


}

