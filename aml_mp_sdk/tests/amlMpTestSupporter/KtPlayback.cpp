/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#define LOG_TAG "AmlMpPlayerDemo_KtPlayback"
#include <utils/Log.h>
#include "KtPlayback.h"
#include <cutils/properties.h>
#include <vector>
#include <string>

#define MLOG(fmt, ...) ALOGI("[%s:%d] " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

namespace aml_mp {

KtPlayback::KtPlayback(Aml_MP_DemuxId demuxId, const sptr<ProgramInfo>& programInfo)
: mProgramInfo(programInfo)
, mDemuxId(demuxId)
{
    mHandleId = (BPNI_PLAYER_HANDLE_ID)((int)demuxId + 1);
}

KtPlayback::~KtPlayback()
{
    stop();
}

int KtPlayback::setSubtitleDisplayWindow(const android::sp<ANativeWindow>& window) {
    bpni_setSubtitleSurfaceNativeWindow(mHandleId, window.get());
    return 0;
}

int KtPlayback::setSubtitleEnabled(bool enabled) {
    bpni_setSubtitleEnabled(mHandleId, enabled);
    return 0;
}

bool KtPlayback::getSubtitleEnabled() {
    return bpni_getSubtitleEnabled(mHandleId);
}

int KtPlayback::setParameter(Aml_MP_PlayerParameterKey key, void* parameter)
{
    // todo
    return 0;
}

void KtPlayback::setANativeWindow(const android::sp<ANativeWindow>& window)
{
    ALOGI("setANativeWindow");
    bpni_setSurfaceNativeWindow(mHandleId, window.get());
}

void KtPlayback::setPmtBuffer(const uint8_t* buffer, int size)
{
    bpni_setPmt(mHandleId, (char*)buffer, size);
    // mHandleId = BPNI_PLAYER_HANDLE_ID_MAIN;
    int ret = bpni_prepare(mHandleId);
    if (ret < 0) {
        ALOGE("prepare player failed!");
        return;
    }
}

void KtPlayback::registerEventCallback(Aml_MP_PlayerEventCallback cb, void* userData)
{
    mEventCallback = cb;
    mUserData = userData;
}

int KtPlayback::start()
{
    int ret = bpni_playStream(mHandleId, mProgramInfo->audioPid, mProgramInfo->videoPid);

    if (ret != 0) {
        ALOGE("player start failed!");
    }

    return ret;
}

int KtPlayback::setVolume(float volume)
{
    return bpni_setVolume(mHandleId, volume);
}

int KtPlayback::setMute(bool mute)
{
    // alticast has no mute interface
    return bpni_setVolume(mHandleId, mute ? 0 : 1);
}

int KtPlayback::setRate(float rate)
{
    return bpni_setRate(mHandleId, rate);
}

int KtPlayback::stop()
{
    int ret = 0;
    ret = bpni_stop(mHandleId, true, true);
    bpni_setSurfaceNativeWindow(mHandleId, nullptr);

    return ret;
}

void KtPlayback::signalQuit()
{
    ALOGI("signalQuit!");
}

int KtPlayback::writeData(const uint8_t* buffer, size_t size)
{
    // ALOGI("kTPlayback writeData: buffer=%p, size=%d", buffer, size);
    bpni_feedData(mHandleId, (char*)buffer, size);
    return size;
}

///////////////////////////////////////////////////////////////////////////////
static struct TestModule::Command g_commandTable[] = {
    {
        "help", 0, "help",
        [](void* player __unused, const std::vector<std::string>& args __unused) -> int {
            TestModule::printCommands(g_commandTable, true);
            return 0;
        }
    },

    {
        "pause", 0, "pause player",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            // todo
            return 0;
        }
    },

    {
        "resume", 0, "resume player",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            // todo
            return 0;
        }
    },

    {
        "gPts", 0, "get Pts",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            KtPlayback* ktPlayer = (KtPlayback*) player;
            double pts = bpni_getPts(ktPlayer->getHandleId());
            printf("Current pts: %lf\n", pts);
            return 0;
        }
    },

    {
        "gVolume", 0, "get volume",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            KtPlayback* ktPlayer = (KtPlayback*) player;
            float volume = bpni_getVolume(ktPlayer->getHandleId());
            printf("Current volume: %f\n", volume);
            return 0;
        }
    },

    {
        "sVolume", 0, "set volume",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            KtPlayback* ktPlayer = (KtPlayback*) player;
            float volume;
            if (args.size() != 2) {
                printf("Input example: sVolume volume\n");
                return -1;
            }
            printf("String input: %s\n", args[1].data());
            volume = stof(args[1]);
            bpni_setVolume(ktPlayer->getHandleId(), volume);
            return 0;
        }
    },

    {
        "Stop", 0, "call stop",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            KtPlayback* ktPlayer = (KtPlayback*) player;
            return bpni_stop(ktPlayer->getHandleId(), true, true);
        }
    },

    {
        "Destory", 0, "call destroy",
        [](void* player, const std::vector<std::string>& args __unused) -> int {
            KtPlayback* ktPlayer = (KtPlayback*) player;
            return bpni_stop(ktPlayer->getHandleId(), true, true);
        }
    },

    {nullptr, 0, nullptr, nullptr}
};

const TestModule::Command* KtPlayback::getCommandTable() const
{
    return g_commandTable;
}

void* KtPlayback::getCommandHandle() const
{
    return (void*)this;
}

}
