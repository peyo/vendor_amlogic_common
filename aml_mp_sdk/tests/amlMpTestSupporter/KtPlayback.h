/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef _PLAYBACK_KT_H_
#define _PLAYBACK_KT_H_

#include "source/Source.h"
#include <system/window.h>
#include "TestModule.h"
#include "AmlMpTestSupporter.h"
#include <utils/RefBase.h>
#include "BasePlayerNativeInterface.h"

namespace aml_mp {

// for KT play test
class KtPlayback : public TestModule, public ISourceReceiver
{
public:
    KtPlayback(Aml_MP_DemuxId demuxId, const sptr<ProgramInfo>& programInfo);
    ~KtPlayback();
    void setANativeWindow(const android::sp<ANativeWindow>& window);
    void setPmtBuffer(const uint8_t* buffer, int size);
    // int setVideoWindow(int x, int y, int width, int height);
    int start();
    virtual int writeData(const uint8_t* buffer, size_t size) override;
    void registerEventCallback(Aml_MP_PlayerEventCallback cb, void* userData);
    int setSubtitleDisplayWindow(const android::sp<ANativeWindow>& window);
    int setSubtitleEnabled(bool enabled);
    bool getSubtitleEnabled();
    int setParameter(Aml_MP_PlayerParameterKey key, void* parameter);
    int setVolume(float volume);
    int setMute(bool mute);
    int setRate(float rate);
    int stop();
    void signalQuit();

    BPNI_PLAYER_HANDLE_ID getHandleId() const{
        return mHandleId;
    }

protected:
    const Command* getCommandTable() const override;
    void* getCommandHandle() const override;

private:
    const sptr<ProgramInfo> mProgramInfo;
    const Aml_MP_DemuxId mDemuxId;
    Aml_MP_PlayerEventCallback mEventCallback = nullptr;
    void* mUserData = nullptr;

    BPNI_PLAYER_HANDLE_ID mHandleId;

private:
    KtPlayback(const KtPlayback&) = delete;
    KtPlayback& operator= (const KtPlayback&) = delete;
};

}





#endif
