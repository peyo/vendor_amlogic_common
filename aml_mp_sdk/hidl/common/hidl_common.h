/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */

#ifndef KT_PLAYER_HIDL_COMMON_H_
#define KT_PLAYER_HIDL_COMMON_H_

#include <binder/ProcessState.h>
#include <hidl/HidlTransportSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

#include <android/hidl/allocator/1.0/IAllocator.h>
#include <android/hidl/memory/1.0/IMemory.h>
#include <hidlmemory/mapping.h>

#include <binder/ProcessState.h>
#include <hidl/HidlTransportSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

#include <android/hidl/allocator/1.0/IAllocator.h>
#include <android/hidl/memory/1.0/IMemory.h>
#include <hidlmemory/mapping.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayer.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayerEventCallback.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/types.h>

using namespace android::hidl::base::V1_0;
using namespace android::hardware;
using android::sp;
using android::wp;
using android::NativeHandle;
using android::hidl::base::V1_0::IBase;
using android::hidl::memory::V1_0::IMemory;
using android::hidl::allocator::V1_0::IAllocator;


#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayer.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayerEventCallback.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/types.h>


using ::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayer;
using ::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayerEventCallback;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_MediaMetaData;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_FeedData;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_NativeWindow;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_PmtData;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_CASChange;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_EventData;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_AUDIO_CODEC;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_VIDEO_CODEC;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_VIDEO_AR_TYPE;
using ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID;

using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::Mutex;

namespace android
{

#define L_TRACE_LINE()				ALOGD("%s() : Line %d", __FUNCTION__, __LINE__)

#define RETURN_VOID_IF_MSG(cond, msg) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return - " msg, __FUNCTION__, __LINE__); \
				return; \
			} \
		} while(0)

#define RETURN_VOID_IF(cond) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return <- %s", __FUNCTION__, __LINE__, #cond); \
				return; \
			} \
		} while(0)

		
#define RETURN_IF_MSG(error, cond, msg) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d " msg, __FUNCTION__, __LINE__); \
				return error; \
			} \
		} while(0)

#define RETURN_IF(error, cond) \
		do { \
			if(cond) \
			{ \
				ALOGE("%s:%d return %s <- %s", __FUNCTION__, __LINE__, #error, #cond); \
				return error; \
			} \
		} while(0)
			

template<typename A, typename B>
void copy_vector(A & dest, const B & src)
{
	size_t size = src.size();

	if(size > 0)
	{
		dest.resize(size);
		for(size_t i = 0; i < size; ++i)
			dest[i] = src[i];
	}
}

inline std::map<std::string, std::string> makeStringMap(
		const hidl_vec<hidl_string>& keys, const hidl_vec<hidl_string>& values)
{
	std::map<std::string, std::string> map;
	if(keys.size() > 0)
	{
		std::map<std::string, std::string> headers;
		for(size_t i = 0; i < keys.size(); ++i)
			map[keys[i]] = values[i];
	}
	return map;
}


inline void KTPLAYER_HEXDUMP(const char * tag, const void *_data, size_t size)
{
	const char * data = (const char * )_data;
	//50hexdecimal + |1 + 16b + |1 = 68 chars per line
	char _buffer[128];

	for (size_t i = 0; i < size; i += 16)
	{
		char * buffer = _buffer;

		//	write hexdecimal line
		buffer = _buffer;
		size_t progress = 0;
		for (size_t j = 0; j < 16; ++j)
		{
			if (j == 8)	//	add more space after 8 bytes written.
				progress++;

			if (i + j < size)
				snprintf(buffer + progress + j * 3, 5, "%02x  ", uint8_t(data[i + j]));
			else
			{
				snprintf(buffer + progress + j * 3, 5, "    ");
			}
		}

		//	write text |...|
		buffer = _buffer;
		progress = 50;
		buffer[progress] = '|';
		progress++;

		for (size_t j = 0; j < 16; ++j)
		{
			if (i + j < size)
			{
				if(isprint(data[i + j]))
					buffer[progress + j] = uint8_t(data[i + j]);
				else
					buffer[progress + j] = '.';
			}
			else
				buffer[progress + j] = ' ';
		}
		buffer[progress + 16] = '|';
		buffer[progress + 17] = '\0';

		ALOGD("[%08zd] %s \n", i, _buffer);
	}
}

class PeerDeathMonitor : public hidl_death_recipient
{
public:
	class Handler
	{
	protected:
		Handler(){}
	public:
		virtual ~Handler(){}

		virtual void onPeerDied(uint64_t cookie, const wp<IBase>& who) = 0;
	};

	PeerDeathMonitor(Handler * handler)
	: mHandler(handler)
	{}

	virtual void serviceDied(uint64_t cookie, const wp<IBase>& who)
	{
		mHandler->onPeerDied(cookie, who);
	}

private:
	Handler * mHandler;
};

} //namespace android

#endif /* KT_PLAYER_HIDL_COMMON_H_ */
