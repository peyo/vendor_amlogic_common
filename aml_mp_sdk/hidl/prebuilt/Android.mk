ifeq ($(BOARD_USES_KT_PLAYER),true)
LOCAL_PATH:= $(call my-dir)
#####################################################################
# libktcasplugin.so
include $(CLEAR_VARS)
LOCAL_MODULE := libktcasplugin
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE := false
LOCAL_32_BIT_ONLY := true
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR)/lib/mediacas
LOCAL_SRC_FILES_arm := $(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)
LOCAL_SHARED_LIBRARIES := libam_adp libc++ libc libcrypto libcutils libdl libjsoncpp liblog libm libssl libteec libutils libz
include $(BUILD_PREBUILT)

endif

