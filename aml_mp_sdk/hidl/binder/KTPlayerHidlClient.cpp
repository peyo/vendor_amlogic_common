/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
 
#undef NDEBUG
#define LOG_NDEBUG 0
#define LOG_TAG "KTHALPlayerHidlClient"

#include <log/log.h>
#include "KTPlayerHidlClient.h"
//subtitle
#include <AmlNativeSubRender.h>

namespace android {
// ================= Navtive Window API Wrapper Start =====================
static int32_t lock(SubNativeRenderHnd hnd, SubNativeRenderBuffer *buf) {
    ANativeWindow_Buffer abuf;
    if (buf == nullptr) {
        ALOGE("Error! no buffer!");
        return -1;
    }
    int32_t r = ANativeWindow_lock((ANativeWindow*)hnd, &abuf, 0);
    buf->bits   = abuf.bits;
    buf->format = abuf.format;
    buf->height = abuf.height;
    buf->width  = abuf.width;
    buf->stride = abuf.stride;
    ALOGD("lock: r=%d w%d h:%d", r, abuf.stride, abuf.height);
    return r;
}

static int32_t unlockAndPost(SubNativeRenderHnd hnd) {
    ALOGD("unlockAndPost");
    return ANativeWindow_unlockAndPost((ANativeWindow*)hnd);
}

static int32_t setBuffersGeometry(SubNativeRenderHnd hnd, int w, int h, int format) {
    return ANativeWindow_setBuffersGeometry((ANativeWindow*)hnd, w, h, format);
}


KTPlayerHidlClient::KTPlayerHidlClient()
: mPeerDeathMonitor(new PeerDeathMonitor(this)) 
{
    connectKTPlayerService();

    SubNativeRenderCallback subCallback;
    subCallback.lock = lock;
    subCallback.unlockAndPost = unlockAndPost;
    subCallback.setBuffersGeometry = setBuffersGeometry;
    if (!aml_RegisterNativeWindowCallback(subCallback)) {
        ALOGE("register subtitle native window callback failed!");
    }

    for(int i=0; i< MAX_MP_PLAYER_INSTANCE_NUM+1; i++){
       mBufferControl[i] = std::make_shared<KTPlayerBufferControl>((BPNI_PLAYER_HANDLE_ID)i);
	}
    preparePMTSender();
}


KTPlayerHidlClient::~KTPlayerHidlClient()
{
	//Mutex::Autolock _l(mLock);
	if(mKtPlayerService)
		mKtPlayerService->unlinkToDeath(mPeerDeathMonitor);
	
}

void KTPlayerHidlClient::connectKTPlayerService()
{
	int32_t timeout = 100;
	while( (mKtPlayerService == nullptr) && (timeout >= 0) )
	{
		mKtPlayerService = IKTPlayer::getService();
		if(mKtPlayerService){
			ALOGE("get mKtPlayerService timeout=%d",timeout);
            bool linked = mKtPlayerService->linkToDeath(mPeerDeathMonitor, /*cookie*/ 0);

		}else{
			usleep(200*1000);//sleep 200ms
			timeout--;
			if( (timeout % 10)  == 0){
				ALOGE("==== not found mKtPlayerService ==== ");
			}
		}
	}
	ALOGE("mKtPlayerService timeout=%d",timeout);
}


void KTPlayerHidlClient::preparePMTSender(void)
{
	//	alloc shared memory.
	auto allocator = IAllocator::getService("ashmem");
	//RETURN_VOID_IF_MSG(nullptr == allocator.get(), " ashmem error");
	
	bool alloc_success = false;
	allocator->allocate(4096, [&](bool success, const hidl_memory& memory)
	{
		if(success == true && (memory.size() == 4096))
		{
			alloc_success = true;
			mHidlPmtData.pmtData = memory;
		}
	});
	RETURN_VOID_IF_MSG(!alloc_success, "alloc fail");
	//	map shared memory.
	mIMemory = mapMemory(mHidlPmtData.pmtData);
	//RETURN_VOID_IF_MSG(nullptr == memory.get(), "memory.get() fail");
	mPmtBuffer = static_cast<uint8_t*>(static_cast<void*>(mIMemory->getPointer()));
	RETURN_VOID_IF_MSG(nullptr == mPmtBuffer, "buffer fail");
}

void KTPlayerHidlClient::setPlayerCallback (BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback)
{
    ALOGD("setPlayerCallback %d, %p", handleId, callback);
	mPlayerCallback[handleId] = callback;
    mPlayerHidlEventCallback[handleId] = new KTPlayerEventCallback;
}

void KTPlayerHidlClient::setMDStatusCallback (BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback)
{
	mMDStatusCallback[handleId] = callback;
}

void KTPlayerHidlClient::setCasEventCallback (BPNI_CAS_CasEventCallback callback)
{
	mCasEventCallback = callback;
}


void KTPlayerHidlClient::setTunerId(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t tunerId)
{
	ALOGD("setTunerId %d, %d", handleId, tunerId);
	if(mKtPlayerService)
		mKtPlayerService->setTunerId(handleId, tunerId);
	else{
		ALOGD("wring mKtPlayerService");
	}
}

int32_t KTPlayerHidlClient::getTunerId(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	
	return mKtPlayerService->getTunerId(handleId);
}

void KTPlayerHidlClient::feedData(HIDL_FeedData& data)
{
	//L_TRACE_LINE();
	if(mKtPlayerService){

		auto ret = mKtPlayerService->feedData(data);
//        if (!ret.isOk()) {
//            ALOGE("feedData failed! handleId:%d, size:%d", data.handleId, data.len);
//        }
	}
}

int KTPlayerHidlClient::feedDataBuffer(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len)
{
	return mBufferControl[(int)handleId]->feedDataBuffer(data, len);
}


void KTPlayerHidlClient::setSurfaceNativeWindow(HIDL_NativeWindow surface)
{
	mKtPlayerService->setSurfaceNativeWindow(surface);
}
ANativeWindow* KTPlayerHidlClient::getSurfaceNativeWindow(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	ANativeWindow* nativeWindow = nullptr;
	bool success = false;
#if 0
	HIDL_NativeWindow window;
	auto res = mKtPlayerService->getSurfaceNativeWindow(handleId, [&window,success](HIDL_NativeWindow hidl_window)
	{
	    auto memory = mapMemory(hidl_window.window);
	    nativeWindow = static_cast<ANativeWindow*>(static_cast<void*>(memory->getPointer()));
	    RETURN_IF(Void(), nullptr == nativeWindow);
	});
#else	
	auto res = mKtPlayerService->getSurfaceNativeWindow(handleId, [&](HIDL_NativeWindow _window)
	{
		nativeWindow = (ANativeWindow*)_window.addr;
	});
#endif
	return nativeWindow;
}
int32_t KTPlayerHidlClient::prepare(HIDL_KTPLAYER_HANDLE_ID handleId)
{
    ALOGI("prepare %p", mPlayerHidlEventCallback[(int)handleId].get());

    if (mPlayerHidlEventCallback[(int)handleId] == nullptr) {
        mPlayerHidlEventCallback[(int)handleId] = new KTPlayerEventCallback;
    }
	if(mBufferControl[(int)handleId] != nullptr)
		mBufferControl[(int)handleId]->clear();

    mKtPlayerService->registerKTPlayerEventCallback(handleId, mPlayerHidlEventCallback[(int)handleId]);

	return mKtPlayerService->prepare(handleId);
}
int32_t KTPlayerHidlClient::playStream(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audiPID, int32_t videoPID)
{
	return mKtPlayerService->playStream(handleId, audiPID, videoPID);
}
int32_t KTPlayerHidlClient::stop(HIDL_KTPLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	ALOGI("stop, handleId=%d, setsubtitle disable", handleId);
	aml_SetSubtitleSessionEnabled((int)handleId, 0);
	return mKtPlayerService->stop(handleId, isAudioStop, isVideoStop);
}
int32_t KTPlayerHidlClient::setVolume(HIDL_KTPLAYER_HANDLE_ID handleId, float volume)
{
	return mKtPlayerService->setVolume(handleId, volume);
}
float KTPlayerHidlClient::getVolume(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return mKtPlayerService->getVolume(handleId);
}


int KTPlayerHidlClient::setAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled)
{
	return mKtPlayerService->setAudioEnabled(handleId, enabled );
}


bool KTPlayerHidlClient::getAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return mKtPlayerService->getAudioEnabled(handleId);
}



void  KTPlayerHidlClient::setBTAudioOutputPid(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audioPid)
{
	KTPlayerHidlClient::mKtPlayerService->setBTAudioOutputPid(handleId, audioPid);
}
int32_t  KTPlayerHidlClient::setRate(HIDL_KTPLAYER_HANDLE_ID handleId, float rate)
{
	return mKtPlayerService->setRate(handleId, rate);
}
float  KTPlayerHidlClient::getRate(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return mKtPlayerService->getRate(handleId);
}
double  KTPlayerHidlClient::getPts(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return mKtPlayerService->getPts(handleId);
}
void  KTPlayerHidlClient::flush(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	mKtPlayerService->flush(handleId);
}
void  KTPlayerHidlClient::setSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	//mKtPlayerService->setSubtitleSurfaceNativeWindow(surface);

    //ANativeWindow* window = (ANativeWindow*)surface.addr;
    ALOGI("window:%p, handle:%d", window, handleId);
    aml_AttachSurfaceWindow((int)handleId, window);
    aml_RegisterQtoneDataCb((int)handleId, qtoneDataCallback);
}
void  KTPlayerHidlClient::setSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled)
{
	ALOGI("setSubtitleEnabled %s", enabled?"TRUE":"FALSE");

	//mKtPlayerService->setSubtitleEnabled(handleId, enabled);
    aml_SetSubtitleSessionEnabled((int)handleId, enabled);
}
bool  KTPlayerHidlClient::getSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	//return mKtPlayerService->getSubtitleEnabled(handleId);
    return aml_GetSubtitleSessionEnabled((int)handleId);
}
void  KTPlayerHidlClient::setSubtitleSmiUrl(HIDL_KTPLAYER_HANDLE_ID handleId, string& url)
{
	//mKtPlayerService->setSubtitleSmiUrl(handleId, url);
    aml_SetSubtitleURI((int)handleId, url.c_str());
}

void  KTPlayerHidlClient::setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len)
{
	mHidlPmtData.handleId = (HIDL_KTPLAYER_HANDLE_ID)handleId;
	if( 4096 <= len ) 
	{
		ALOGE("=== KTPlayerHidlClient::setPmt oveflow ===");
		return;
	}
	mIMemory->update();
	memcpy(mPmtBuffer, pmtData, len);
	mHidlPmtData.len = len;
	mIMemory->commit();
	mKtPlayerService->setPmt(mHidlPmtData);
}
HIDL_MediaMetaData&  KTPlayerHidlClient::getMetaData(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	bool success = false;
	auto res = mKtPlayerService->getMetaData(handleId, [&](bool result, HIDL_MediaMetaData _data)
	{
		success = result;
		mMetaData[(int32_t)handleId].ar_type 	= _data.ar_type;
		mMetaData[(int32_t)handleId].width 		= _data.width;
		mMetaData[(int32_t)handleId].height 	= _data.height;
		mMetaData[(int32_t)handleId].audioCodec = _data.audioCodec;
		mMetaData[(int32_t)handleId].videoCodec = _data.videoCodec;
	});
	
	return mMetaData[(int32_t)handleId];
}

int32_t  KTPlayerHidlClient::cas_channelChange(HIDL_CASChange& info)
{
	ALOGE("cas_channelChange: handle(%d), url=%s", (int)info.handleId, info.url.c_str());
	return mKtPlayerService->cas_channelChange(info);
}
void  KTPlayerHidlClient::cas_MDReceptionStart(HIDL_KTPLAYER_HANDLE_ID handleId, string descriptor_blob_input, int32_t descriptor_blob_input_length)
{
	mKtPlayerService->cas_MDReceptionStart(handleId, descriptor_blob_input, descriptor_blob_input_length);
}
void  KTPlayerHidlClient::cas_MDReceptionEnd(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	mKtPlayerService->cas_MDReceptionEnd(handleId);
}
int32_t  KTPlayerHidlClient::cas_setPlayPosition(HIDL_KTPLAYER_HANDLE_ID handleId, long position, int32_t speed)
{
	return mKtPlayerService->cas_setPlayPosition(handleId, (int32_t)position, speed);
}
int32_t  KTPlayerHidlClient::cas_sendCasEvent(std::string& jsonString)
{
	ALOGE("cas_sendCasEvent: %s", jsonString.c_str());

	return mKtPlayerService->cas_sendCasEvent(jsonString);
}

int32_t  KTPlayerHidlClient::cas_setEMMAddress(std::string& address)
{
	ALOGE("cas_setEMMAddress: %s", address.c_str());

	return mKtPlayerService->cas_setEMMAddress(address);
}


void KTPlayerHidlClient::onPeerDied(uint64_t cookie, const wp<IBase>& who)
{
    ALOGE("ktplayerd service died!");
    mKtPlayerService->unlinkToDeath(mPeerDeathMonitor);
    mKtPlayerService.clear();
}

void KTPlayerHidlClient::qtoneDataCallback(int sessionId, const char* buf, int size)
{
    ALOGW("qtone sessionId:%d, buf:%p, size:%d", sessionId, buf, size);
}

void KTPlayerHidlClient::notifyPlayerEvent(const HIDL_EventData& eventData)
{
    BPNI_PLAYER_HANDLE_ID handleId = (BPNI_PLAYER_HANDLE_ID)eventData.handleId;

    ALOGW("notifyPlayerEvent %d, event_type:%d", handleId, eventData.event_type);
    if (mPlayerCallback[handleId] != nullptr) {
        mPlayerCallback[handleId](handleId, eventData.event_type);
    }
}


//just 
#define BUNDLE_BUFFER_COUNTS		10
#define BUNDLE_BUFFER_COUNTS_MAX	10
#define DEFAULT_BUFFER_SIZE		2*1024*1024
#define AMLMPTEST_BUFFER_SIZE	1024*188 //192,512

KTPlayerBufferControl::KTPlayerBufferControl(BPNI_PLAYER_HANDLE_ID id)
{
	mHandleId = (HIDL_KTPLAYER_HANDLE_ID)id;
	mHidlFeedData.handleId = mHandleId;

	//	alloc shared memory.
	auto allocator = IAllocator::getService("ashmem");
	//RETURN_VOID_IF_MSG(nullptr == allocator.get(), " ashmem error");
	
	bool alloc_success = false;
	allocator->allocate(DEFAULT_BUFFER_SIZE, [&](bool success, const hidl_memory& memory)
	{
		if(success == true && (memory.size() == DEFAULT_BUFFER_SIZE))
		{
			alloc_success = true;
			mHidlFeedData.data = memory;
		}
	});
	RETURN_VOID_IF_MSG(!alloc_success, "alloc fail");
	//	map shared memory.
	mIMemory = mapMemory(mHidlFeedData.data);
	//RETURN_VOID_IF_MSG(nullptr == memory.get(), "memory.get() fail");
	mBufferPointer = static_cast<uint8_t*>(static_cast<void*>(mIMemory->getPointer()));
	RETURN_VOID_IF_MSG(nullptr == mBufferPointer, "buffer fail");

}
void KTPlayerBufferControl::clear()
{
	mReadyBuffer = false;
	mWrittenBundle = 0;
	mWrittenBytes = 0;
}

void KTPlayerBufferControl::prepare(uint32_t size)
{
	mHidlFeedData.len = 0;
	mIMemory->update();
	mReadyBuffer=true;
}

int KTPlayerBufferControl::updateDataBuffer(char* data, int len)
{
	if( DEFAULT_BUFFER_SIZE <= (mWrittenBytes + len) ) 
	{
		ALOGE("=== KTPlayerBufferControl oveflow ===");
		return 0;
	}
	//mIMemory->update();
	memcpy(mBufferPointer+mWrittenBytes, data, len);
	mWrittenBytes += len;
	mWrittenBundle++;
	mHidlFeedData.len = mWrittenBytes;

	if( mWrittenBundle >= BUNDLE_BUFFER_COUNTS){
		mIMemory->commit();
		KTPlayerHidlClient::instance().feedData(mHidlFeedData);
		clear();
	}

	return len;
}

int KTPlayerBufferControl::feedDataBuffer(char* data, int len)
{
	if(not mReadyBuffer)
		prepare(DEFAULT_BUFFER_SIZE);

	if(0)//(AMLMPTEST_BUFFER_SIZE == len)
	{
		mIMemory->update();
		memcpy(mBufferPointer, data, len);
		mIMemory->commit();
		KTPlayerHidlClient::instance().feedData(mHidlFeedData);
		clear();
	}else{
		updateDataBuffer(data, len);
	}
	return len;
}


} // namespace android

