/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
 
#ifndef VENDOR_KTPLAYER_V1_0_CLIENT_H
#define VENDOR_KTPLAYER_V1_0_CLIENT_H

#include <utils/Mutex.h>
#include "hidl_common.h"
#include "impl_BasePlayerNativeInterface.h"

#define MAX_MP_PLAYER_INSTANCE_NUM   5

namespace android {

#include <hidl_common.h>
#include <android/native_window.h>

using std::string;
/* definition of player events */
typedef enum : int32_t{
	KT_PLAYER_EVENT_UNKNOWN = 0,
	KT_PLAYER_EVENT_PLAYER_LOST,  // crash
	KT_PLAYER_EVENT_PLAYER_START,
	KT_PLAYER_EVENT_PLAYER_RESTART,	//resume after crash
	KT_PLAYER_EVENT_VIDEO_CHANGED,
	KT_PLAYER_EVENT_AUDIO_CHANGED,
	KT_PLAYER_EVENT_FIRST_FRAME,
	KT_PLAYER_EVENT_AV_SYNC_DONE,
	KT_PLAYER_EVENT_DATA_LOSS,
	KT_PLAYER_EVENT_DATA_RESUME,
	KT_PLAYER_EVENT_SCRAMBLING,
	KT_PLAYER_EVENT_USERDATA_AFD,
	KT_PLAYER_EVENT_USERDATA_CC,
	KT_PLAYER_EVENT_PID_CHANGED
} BPNI_PLAYER_EVENT_TYPE;


class KTPlayerBufferControl
{
public: 
	KTPlayerBufferControl(BPNI_PLAYER_HANDLE_ID id);	
	int feedDataBuffer(char* data, int len);
	void clear();

private:
	void prepare(uint32_t size);
	int updateDataBuffer(char* data, int len);
	
	HIDL_FeedData mHidlFeedData;
	HIDL_KTPLAYER_HANDLE_ID mHandleId;
	sp<IMemory> mIMemory;
	bool mReadyBuffer = false;
	uint8_t* mBufferPointer = nullptr;
	uint32_t mWrittenBytes = 0;
	uint32_t mWrittenBundle = 0;

};

class KTPlayerHidlClient : public PeerDeathMonitor::Handler
{

public:
	static KTPlayerHidlClient& instance() {
		static KTPlayerHidlClient* instance = new KTPlayerHidlClient(); 
		return *instance; 
	};


	void setTunerId(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t tunerId);

	int32_t getTunerId(HIDL_KTPLAYER_HANDLE_ID handleId);

	void feedData(HIDL_FeedData& data);
	int feedDataBuffer(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len);

	void setSurfaceNativeWindow(HIDL_NativeWindow surface);

	ANativeWindow* getSurfaceNativeWindow(HIDL_KTPLAYER_HANDLE_ID handleId);

	int32_t prepare(HIDL_KTPLAYER_HANDLE_ID handleId);

	int32_t playStream(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audiPID, int32_t videoPID);

	int32_t stop(HIDL_KTPLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop);

	int32_t setVolume(HIDL_KTPLAYER_HANDLE_ID handleId, float volume);

	float getVolume(HIDL_KTPLAYER_HANDLE_ID handleId);

	int32_t setAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled);

	bool getAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId);


	void  setBTAudioOutputPid(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audioPid);

	int32_t  setRate(HIDL_KTPLAYER_HANDLE_ID handleId, float rate);

	float  getRate(HIDL_KTPLAYER_HANDLE_ID handleId);

	double  getPts(HIDL_KTPLAYER_HANDLE_ID handleId);

	void  flush(HIDL_KTPLAYER_HANDLE_ID handleId);

	void  setSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);

	void  setSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled);

	bool  getSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId);

	void  setSubtitleSmiUrl(HIDL_KTPLAYER_HANDLE_ID handleId, string& url);

	void  setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len);

	HIDL_MediaMetaData&  getMetaData(HIDL_KTPLAYER_HANDLE_ID handleId);

	int32_t  cas_channelChange(HIDL_CASChange& info);
	void  cas_MDReceptionStart(HIDL_KTPLAYER_HANDLE_ID handleId, string descriptor_blob_input, int32_t descriptor_blob_input_length);
	void  cas_MDReceptionEnd(HIDL_KTPLAYER_HANDLE_ID handleId);
	int32_t  cas_setPlayPosition(HIDL_KTPLAYER_HANDLE_ID handleId, long position, int32_t speed);
	int32_t  cas_sendCasEvent(std::string& jsonString);
	int32_t  cas_setEMMAddress(std::string& address);

	void setPlayerCallback (BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback);
	void setMDStatusCallback (BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback);
	void setCasEventCallback (BPNI_CAS_CasEventCallback callback);
protected:
	virtual void onPeerDied(uint64_t cookie, const wp<IBase>& who) override;

private:
    struct KTPlayerEventCallback;
    friend struct KTPlayerEventCallback;
    KTPlayerHidlClient();
    ~KTPlayerHidlClient();
	void connectKTPlayerService();
    void notifyPlayerEvent(const HIDL_EventData& eventData);
	void preparePMTSender(void);

    static void qtoneDataCallback(int session, const char* buf, int size);

	sp<IKTPlayer> mKtPlayerService = nullptr;
	sp<PeerDeathMonitor> mPeerDeathMonitor;
	//static Mutex mLock;
	HIDL_PmtData mHidlPmtData;
	sp<IMemory> mIMemory;
	uint8_t* mPmtBuffer = nullptr;
	
	std::shared_ptr<KTPlayerBufferControl> mBufferControl[MAX_MP_PLAYER_INSTANCE_NUM+1] = {nullptr,nullptr,nullptr,nullptr, nullptr};
	
	HIDL_MediaMetaData mMetaData[MAX_MP_PLAYER_INSTANCE_NUM+1];
	BPNI_PlayerEventCallback mPlayerCallback[MAX_MP_PLAYER_INSTANCE_NUM+1] = {nullptr,nullptr,nullptr,nullptr, nullptr};
    sp<KTPlayerEventCallback> mPlayerHidlEventCallback[MAX_MP_PLAYER_INSTANCE_NUM+1] = {nullptr, nullptr, nullptr, nullptr, nullptr};
	BPNI_MDStatusCallback mMDStatusCallback[MAX_MP_PLAYER_INSTANCE_NUM+1] = {nullptr,nullptr,nullptr,nullptr, nullptr};
	BPNI_CAS_CasEventCallback mCasEventCallback = nullptr;
	
};

struct KTPlayerHidlClient::KTPlayerEventCallback : IKTPlayerEventCallback
{
public:
    KTPlayerEventCallback() { }

    Return<void> notify(const HIDL_EventData& event) override {
        KTPlayerHidlClient::instance().notifyPlayerEvent(event);

        return Void();
    }

private:
    KTPlayerEventCallback(const KTPlayerEventCallback&) = delete;
    KTPlayerEventCallback& operator= (const KTPlayerEventCallback&) = delete;
};


} // namespace android

#endif //VENDOR_KTPLAYER_V1_0_CLIENT_H
