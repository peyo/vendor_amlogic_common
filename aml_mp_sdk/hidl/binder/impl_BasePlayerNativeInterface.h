/*
 * Copyright 2001-2021 by Alticast, Corp., All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Alticast, Corp. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Alticast, Corp.
 */


#ifndef BASEPLAYER_NATIVE_INTERFACE_H
#define BASEPLAYER_NATIVE_INTERFACE_H

#include <stdbool.h>

#include <android/native_window.h>


#ifdef __cplusplus
extern "C" {
#endif


/* definition of player handle ID */
typedef enum : int32_t{
    BPNI_PLAYER_HANDLE_ID_MAIN = 1,    // MAIN 
    BPNI_PLAYER_HANDLE_ID_PIP =  2,    // PIP
    BPNI_PLAYER_HANDLE_ID_SUB1 = 3,    // SUB1  // PIP and SUB1 have different ID value, but they will use same player.
    BPNI_PLAYER_HANDLE_ID_SUB2 = 4,    // SUB2
    BPNI_PLAYER_HANDLE_ID_SUB3 = 5    // SUB3
} BPNI_PLAYER_HANDLE_ID;

/**
 * Event callback function from Player
 *
 * @param[in] handleId handle ID. (player handle)
 * @param[in] event
 */
typedef void (*BPNI_PlayerEventCallback)(BPNI_PLAYER_HANDLE_ID handleId, int event);

/**
 * register event callback
 *
 * @param[in] handleId handle ID.
 * @param[in] callback function.
  */
void impl_RegisterPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback);


/*** AML NOTE: tunerId usage (normally use this way, but not always, there will be exceptional case)
 *	1-channel playback: tunerId = 1
 *  2-Channel PIP :		tunerId = 1, 2
 *  4-Channel Multiview :	tunerId = 1, 3, 4, 5
 ***/
/**
 * Set TunerID(or Demux Id), TS stream data will feed this Tuner(or Demux)
 * And CAS will use this Tuner ID
 * @param[in] handleId handle ID. 
 * @param[in] tunerId tuner ID.
 */
void impl_SetTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId);

/**
 * Return Tuner Id(or demux Id)
 *
 * @param[in] handleId handle ID. 
 * @return Tuner ID. if empty, then return "-1" (if not set before, or source were some kind of URLs)
 */
int impl_GetTunerId(BPNI_PLAYER_HANDLE_ID handleId);


/**
 * Feed TS stream data for Live Channel, VoD to given player
 * @param[in] handleId handle ID.
 * @param[in] data TS stream data.
 * @param[in] len data length.
 */
void impl_FeedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len);

/**
 * Set ANativeWindow (came from upper JAVA layer's Surface object) for setting video surface
 *
 * 		NOTE: Java layer will use this API after conversion by ANativeWindow_fromSurface() from jobject surface to ANativeWindow
 *		conversion: ANativeWindow* ANativeWindow_fromSurface(JNIEnv* env, jobject surface)
 *
 * @param[in] handleId handle ID. 
 * @param[in] pointer of native window surface for video
 */
void impl_SetSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);

/**
 *
 * @param[in] handleId handle ID.
 * @return return Player's pointer of ANativeWindow
 */
ANativeWindow* impl_GetSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId);



/*** AML NOTE:
 *	impl_prepare is almost same as CreatePlayer
 ***/
/**
 * Prepare playback.
 * blocks until MediaPlayer is ready for playback
 * 
 * Note : calling sequence - this function would be called after setTunerId() and setSurfaceNativeWindow()
 *
 * @param[in] handleId handle ID.
 * @return 0 Success. -1 fail.
 */
int impl_Prepare(BPNI_PLAYER_HANDLE_ID handleId);

/**
 * Play A/V with given a-v PID
 * <br> this API can be called multiple times without calling stop()
 * <br> i.e.) playStream() -> stop() -> playStream()
 * <br> ex 1) playStream(8006, 8005) -> playStream(8007, 8005)  : change audio language or change audio track
 * <br> ex 2) playStream(8006, 8005) -> playSteram(0, 8005) : normal AV play -> play video ONLY
 * <br> ex 3) playStream(0, 8005) -> playSteram(8006, 8005) : play video ONLY -> player both A/V
 * <br> ex 4) playStream(8006, 8005) -> playStream(0, 0) : same as stop(), no A/V playing
 *
 * @param[in] handleId handle ID.
 * @param[in] audiPID - audio PID. "0" means NO audio or stop audio (audio decoder stop)
 * @param[in] videoPID - video PID. "0" means NO video or stop video (video decoder stop)
 *
 * @return 0 Success. -1 fail.
 */
int impl_PlayStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID);

/**
 * Stop A/V.
 * Stop selective Audio or Video. (choiceable)
 *
 * @param[in] handleId handle ID.
 * @param[in] isAudioStop if true,  audio stop
 * @param[in] isVideoStop if true, video stop
 * @return 0 Success, -1 fail.
 */
int impl_Stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop);

/**
 * set volume control
 *
 * @param[in] handleId handle ID.
 * @param[in] volume value: range = 0.0f ~ 1.0f, step => 0.01
 * @return 0 Success. -1 fail.
 */
int impl_SetVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume);

/**
 * return current volume level
 *
 * @param[in] handleId handle ID.
 * @return current volume level. If not available or fail, return -1.0f.
 */
float impl_GetVolume(BPNI_PLAYER_HANDLE_ID handleId);

int impl_SetAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled);

bool impl_GetAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId);


/**
 * Configure audio PID for Bluetooth device
 *
 * @param[in] handleId handle ID.
 * @param[in] audioPid audio PID.
 * @return success: 0,  fail: -1
 */
int impl_SetBTAudioOutputPid(BPNI_PLAYER_HANDLE_ID handleId, int audioPid);

/**
 * Set trick playback rate
 * In case of normal trick (NOT 0.8x, 1.2x, 1.5x), flush remained buffer
 *
 * @param[in] handleId handle ID.
 * @param rate  : playback rate
 * @return 0 Success. -1 fail.
 */
int impl_SetRate(BPNI_PLAYER_HANDLE_ID handleId, float rate);

/**
 * return current playback rate
 *
 * @param[in] handleId handle ID. 
 * @return current playback rate. If not playing, return 0f.
 */
float impl_GetRate(BPNI_PLAYER_HANDLE_ID handleId);

/**
 * Return PTS
 *
 * @param[in] handleId handle ID.
 * @return PTS. fail: -1 return.
 */
double impl_GetPts(BPNI_PLAYER_HANDLE_ID handleId);

/**
 *  Flush stream data buffer inside of Player
 *
 * @param[in] handleId handle ID.
 */
void impl_Flush(BPNI_PLAYER_HANDLE_ID handleId);

/**
 * Set ANativeWindow surface for drawing ClosedCaption
 *
 * @param[in] handleId handle ID.
 * @param[in] window pointer of ANativeWindow surface for CC
 */
void impl_SetSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);

/**
 * Subtitle (Closed Caption / VoD SMI subtitle)  On / Off
 *
 * @param[in] handleId handle ID.
 * @param[in] enabled true = On , false = Off
 */
void impl_SetSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled);

/**
 * Return Subtitle On or Off status
 *
 * @param[in] handleId handle ID.
 * @return On / Off 여부
 */
bool impl_GetSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId);


/**
 * Set SMI URL for VoD
 *
 * @param[in] handleId handle ID.
 * @param[in] url SMI subtitle URL
 */
void impl_SetSubtitleSmiUrl(BPNI_PLAYER_HANDLE_ID handleId, char* url);

/**
 * Set raw PMT table
 *
 * @param[in] handleId handle ID.
 * @param[in] pmtData PMT data.
 * @param[in] len PMT data's length
 */
void impl_SetPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len);

typedef enum : int32_t{
    BPNI_ASPECT_RATIO_UNKNOWN = -1, /**< Unknown aspect ratio. */
    BPNI_ASPECT_RATIO_4_3= 2,       /**< Aspect ratio 4:3. */
    BPNI_ASPECT_RATIO_16_9,         /**< Aspect ratio 16:9. */
    BPNI_ASPECT_RATIO_2_21_1        /**< Aspect ratio 2.21:1. */
} BPNI_VIDEO_AR_TYPE;

typedef enum : int32_t{
    BPNI_VIDEO_CODEC_UNKNOWN,
    BPNI_VIDEO_CODEC_NONE,
    BPNI_VIDEO_CODEC_MPEG1,
    BPNI_VIDEO_CODEC_MPEG2,
    BPNI_VIDEO_CODEC_MPEG4,
    BPNI_VIDEO_CODEC_H263,
    BPNI_VIDEO_CODEC_H264,
    BPNI_VIDEO_CODEC_VC1,
    BPNI_VIDEO_CODEC_DIVX,
    BPNI_VIDEO_CODEC_AVS,
    BPNI_VIDEO_CODEC_RV40,
    BPNI_VIDEO_CODEC_VP6,
    BPNI_VIDEO_CODEC_VP7,
    BPNI_VIDEO_CODEC_VP8,
    BPNI_VIDEO_CODEC_SPARK,
    BPNI_VIDEO_CODEC_MJPEG
} BPNI_VIDEO_CODEC;

typedef enum : int32_t{
    BPNI_AUDIO_CODEC_UNKNOWN,
    BPNI_AUDIO_CODEC_NONE,
    BPNI_AUDIO_CODEC_MPEG,
    BPNI_AUDIO_CODEC_MP3,
    BPNI_AUDIO_CODEC_AAC,
    BPNI_AUDIO_CODEC_AC3,
    BPNI_AUDIO_CODEC_DTS,
    BPNI_AUDIO_CODEC_LPCM,
    BPNI_AUDIO_CODEC_WMA,
    BPNI_AUDIO_CODEC_AVS,
    BPNI_AUDIO_CODEC_PCM,
    BPNI_AUDIO_CODEC_AMR,
    BPNI_AUDIO_CODEC_DRA,
    BPNI_AUDIO_CODEC_COOK,
    BPNI_AUDIO_CODEC_ADPCM,
    BPNI_AUDIO_CODEC_SBC,
    BPNI_AUDIO_CODEC_VORBIS,
    BPNI_AUDIO_CODEC_G711,
    BPNI_AUDIO_CODEC_G723,
    BPNI_AUDIO_CODEC_G726,
    BPNI_AUDIO_CODEC_G729,
    BPNI_AUDIO_CODEC_FLAC,
    BPNI_AUDIO_CODEC_MLP,
    BPNI_AUDIO_CODEC_APE
} BPNI_AUDIO_CODEC;


typedef struct _MediaMetaData {

    /**
     * aspect ratio
     */
    BPNI_VIDEO_AR_TYPE ar_type;

    /**
     * width
     */
    int width;

    /**
     * height
     */
    int height;

    /**
     * audio codec.
     */
    BPNI_AUDIO_CODEC audioCodec;

    /**
     * video codec
     */
    BPNI_VIDEO_CODEC videoCodec;

} BPNI_MediaMetaData; 

/**
 * Get A/V meta data
 *
 * @param[in] handleId handle ID.
 * @return BaseMediaMetaData
 */
BPNI_MediaMetaData impl_GetMetaData(BPNI_PLAYER_HANDLE_ID handleId);

/* for CAS */

/**
  * callback from CAS plugin after receiving meta data,
  *	when VoD session is available or errors are happened, this callback would be called
  *
  * @param[in] handleId handle ID.
  * @param[in] md_status 0 : VoD session is ready=1 : otherwise invalid ticket signature error value
  */
typedef void (*BPNI_MDStatusCallback)(BPNI_PLAYER_HANDLE_ID handleId, int md_status);

/**
  * Register BPNI_MDStatusCallback
  *
  * @param[in] handleId handle ID.
  * @param[in] callback
  */
void impl_Cas_registerMDStatusCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback);


/**
 * This function would be called every starting/ending session for Live & VoD playbak.
 * This indicates CAS plugin have to prepare new stream playback.
 * starting new cahnnel : valid url
 * ending current channel: NULL url  (AML note: almost same as destroy player)
 *
 * @param[in] handleId handle ID.
 * @param[in] service_id: program number // In case of VoD, it should be "0"
 * @param[in] url session URL. (e.g.   udp://xxx.xxx.xxx.xxx.:port)
 * @param[in] type session Type. VoD : 0 , BTV : 1
 * @param[in] number_of_components number of ES stream
 * @param[in] pid_list array of ESPID
 * @return success: 0, fail: -1.
 */
int impl_Cas_channelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list);

/**
 * Whenever application starts receiving Meta data this function would be called, KT CAS plugin will start metat data by this.
  *
 * @param[in] handleId handle ID.
  * @param[in] descriptor_blob_input (string type) received VoD meta data from server by Java App
 * @param[in] descriptor_blob_input_length length of VOD Meta data
 * @return success: 0, fail: -1
 */
int impl_Cas_MDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, char* descriptor_blob_input, int descriptor_blob_input_length);

/**
 * In case BPNI_MDStatusCallback with success event, call impl_cas_MDReceptionEnd for notifying 
 *
 * @param[in] handleId handle ID.
 */
void impl_Cas_MDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId);

/**
 * Start VoD playback, if necessary, with adjusting playback position or direction and speed
 *
 * @param[in] handleId handle ID.
 * @param[in] [Unused] position position of VoD playback. milli second
 * @param[in] speed (+)FF, (-)REW
 * @return success 0, fail -1.
 */
int impl_Cas_setPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, long position, int speed);

/**
  * Send CAS Event
  *
  * @param[in] jsonString
  * @return 0 Success. -1 fail.
  */
int impl_Cas_sendCasEvent(char* jsonString);


/**
  * When event is received from CAS plugin (CAS plugin -> AML Hal player -> Alticast App)
  *
  * @param[in] event event from CAS plugin
  */
typedef void (*BPNI_CAS_CasEventCallback)(char* event);

/**
 * Register CAS event callback
 *
 * @param[in] callback Callback function
  */
void impl_Cas_registerCasEventCallback(BPNI_CAS_CasEventCallback callback);

/**
  * Setting EMM Address
  *
  * @return 0 Success. -1 fail.
  */
int impl_Cas_setEMMAddress(char* address);





#ifdef __cplusplus
};
#endif


#endif // BASEPLAYER_NATIVE_INTERFACE_H
