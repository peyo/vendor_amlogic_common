/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */


#define LOG_NDEBUG 0
#define LOG_TAG "KTHALBasePlayerNativeIF"
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayer.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayerEventCallback.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/types.h>

#include <android/hidl/allocator/1.0/IAllocator.h>
#include <android/hidl/memory/1.0/IMemory.h>
#include <hidlmemory/mapping.h>

#include "impl_BasePlayerNativeInterface.h"
#include "KTPlayerHidlClient.h"
#include <hidl_common.h>

// #ifndef __ANDROID_VNDK__
#include <system/window.h>
#include <amlogic/am_gralloc_ext.h>
#include <cutils/native_handle.h>
#include <video_tunnel.h>
// #endif


#define SUPPORT_DEBUG_DUMP_TS	0 // need to create "/data/temp/" due to permission.

using namespace android;
using ::android::hidl::allocator::V1_0::IAllocator;
using ::android::hidl::memory::V1_0::IMemory;
using ::android::hardware::hidl_memory;

#define VALIDCHECK(x)	(1)

int g_tunerIds[10];
static std::map<int, sp<NativeHandle>> nativeHandleMap;
static std::map<int, sp<ANativeWindow>> aNativeWindowMap;


inline bool __isValidPlyaerId(BPNI_PLAYER_HANDLE_ID handleId)
{
#if 0
	if( (handleId >= BPNI_PLAYER_HANDLE_ID_MAIN) && (handleId <= BPNI_PLAYER_HANDLE_ID_SUB3))
		return true;
	else
		return false;
#else
	return true;
#endif
}

/**
 * register event callback
 *
 * @param[in] handleId handle ID.
 * @param[in] callback function.
  */
void impl_RegisterPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback)
{
	if( nullptr != callback )
		KTPlayerHidlClient::instance().setPlayerCallback(handleId, callback);
}


/*** AML NOTE: tunerId usage (normally use this way, but not always, there will be exceptional case)
 *	1-channel playback: tunerId = 1
 *  2-Channel PIP :		tunerId = 1, 2
 *  4-Channel Multiview :	tunerId = 1, 3, 4, 5
 ***/
/**
 * Set TunerID(or Demux Id), TS stream data will feed this Tuner(or Demux)
 * And CAS will use this Tuner ID
 * @param[in] handleId handle ID. 
 * @param[in] tunerId tuner ID.
 */
void impl_SetTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId)
{
	ALOGW("impl_SetTunerId %d,%d just keep", handleId, tunerId);
	if( __isValidPlyaerId(handleId) )
	{
		g_tunerIds[handleId] = tunerId;
	}
}

void impl_SetTunerIdEx(BPNI_PLAYER_HANDLE_ID handleId, int tunerId)
{
	ALOGW("impl_SetTunerIdEx %d,%d to server", handleId, tunerId);
	if( __isValidPlyaerId(handleId) )
	{
		KTPlayerHidlClient::instance().setTunerId((HIDL_KTPLAYER_HANDLE_ID)handleId, tunerId);
	}
}

/**
 * Return Tuner Id(or demux Id)
 *
 * @param[in] handleId handle ID. 
 * @return Tuner ID. if empty, then return "-1" (if not set before, or source were some kind of URLs)
 */
int impl_GetTunerId(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return g_tunerIds[handleId];
	}
	return -1;	
}


int impl_GetTunerIdEx(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().getTunerId((HIDL_KTPLAYER_HANDLE_ID)handleId);
	}
	return -1;	
}


#if SUPPORT_DEBUG_DUMP_TS
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <cutils/properties.h>
//#include <zlib.h>

static int64_t gLastBitRateMeasureTime = -1;
static int64_t gBitRateMeasureSize = 0;

static void calStatistic(int size)
{
    int64_t nowUs = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    if (gLastBitRateMeasureTime == -1) {
        gLastBitRateMeasureTime = nowUs;
    } else {
        gBitRateMeasureSize += size;
        int64_t period = nowUs - gLastBitRateMeasureTime;
        if (period >= 2000000) {
            int64_t bitrate = gBitRateMeasureSize * 1000000 / period;
            ALOGE("receive bitrate:%.2fMB/s", (bitrate>>10)/1024.0);
            gBitRateMeasureSize = 0;
            gLastBitRateMeasureTime = nowUs;
        }
    }
}
#endif

/**
 * Feed TS stream data for Live Channel, VoD to given player
 * @param[in] handleId handle ID.
 * @param[in] data TS stream data.
 * @param[in] len data length.
 */
void impl_FeedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len)
{
	if(data==nullptr || len <=0) return;
#if SUPPORT_DEBUG_DUMP_TS
	calStatistic(len);
	
	bool enableDump = property_get_bool("vendor.amlmp.inputdump", false);
	static bool now_enabled = false;
	static int mDumpFd = -1;
	static int mDumpFd2 = -1;
	if (enableDump) {
		if(now_enabled==false){
			ALOGE("START input ts dump");
			mDumpFd = ::open("/data/temp/aml_dump.ts", O_WRONLY | O_TRUNC | O_CREAT, 0666);
			if (mDumpFd < 0) {
				ALOGE("open dump fil failed! %s", strerror(errno));
			}
			mDumpFd2 = ::open("/data/temp/aml_dump2.ts", O_WRONLY | O_TRUNC | O_CREAT, 0666);
			if (mDumpFd2 < 0) {
				ALOGE("open dump fil failed! %s", strerror(errno));
			}

			now_enabled = true;
		}
		if(handleId == BPNI_PLAYER_HANDLE_ID_MAIN){
			if (mDumpFd >= 0) {
				if (::write(mDumpFd, data, len) != len) {
					ALOGE("write dump file failed!");
					return;
				}
			}
		}else 	if(handleId == BPNI_PLAYER_HANDLE_ID_PIP){
			if (mDumpFd2 >= 0) {
				if (::write(mDumpFd2, data, len) != len) {
					ALOGE("write dump file failed!");
					return;
				}
			}
		}
	}else{
		if(enableDump){
			if(mDumpFd!=-1){
				::close(mDumpFd);
				mDumpFd=-1;
			}
			if(mDumpFd2!=-1){
				::close(mDumpFd2);
				mDumpFd2=-1;
			}
			enableDump = false;
		}

	}
	
	bool skipbackend = property_get_bool("vendor.amlmp.skipbackend", false);
	if (skipbackend) {
		return;
	}


#endif
	KTPlayerHidlClient::instance().feedDataBuffer(handleId, data, len);
}

/**
 * Set ANativeWindow (came from upper JAVA layer's Surface object) for setting video surface
 *
 * 		NOTE: Java layer will use this API after conversion by ANativeWindow_fromSurface() from jobject surface to ANativeWindow
 *		conversion: ANativeWindow* ANativeWindow_fromSurface(JNIEnv* env, jobject surface)
 *
 * @param[in] handleId handle ID. 
 * @param[in] pointer of native window surface for video
 */
void impl_SetSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	if( __isValidPlyaerId(handleId) )
	{
		ALOGW("===FROM: impl_setSurfaceNativeWindow=%p, handleId: %d", window, handleId);
		
		HIDL_NativeWindow hidlSurface;
		hidlSurface.addr = (int32_t)window;
		hidlSurface.handleId = (HIDL_KTPLAYER_HANDLE_ID)handleId;
// #ifndef __ANDROID_VNDK__
#define MAX_HANDLE_ID 20
		int tunnelId = -1;
		if (window) {
			int type = AM_FIXED_TUNNEL;
            int mesonVtFd = meson_vt_open();
            if (mesonVtFd < 0) {
                ALOGW("meson_vt_open failed!");
                return;
            }
            if (meson_vt_alloc_id(mesonVtFd, &tunnelId) < 0) {
                ALOGW("meson_vt_alloc_id failed!");
                meson_vt_close(mesonVtFd);
                return;
            }
            ALOGW("allocId: %d", tunnelId);
            meson_vt_free_id(mesonVtFd, tunnelId);
            meson_vt_close(mesonVtFd);

			ALOGW("impl_SetSurfaceNativeWindow: set sideband, tunerId:%d, aNativeWindow:%p", tunnelId, window);
			native_handle_t* sidebandHandle = am_gralloc_create_sideband_handle(AM_FIXED_TUNNEL, tunnelId);
			sp<NativeHandle> nativeHandle = NativeHandle::create(sidebandHandle, true);
			nativeHandleMap.emplace(tunnelId, nativeHandle);
			aNativeWindowMap.emplace(tunnelId, window);
			native_window_set_sideband_stream(window, sidebandHandle);
			impl_SetTunerIdEx(handleId, tunnelId);
		} else {
			tunnelId = impl_GetTunerIdEx(handleId);
			auto item = aNativeWindowMap.find(tunnelId);
			if(item != aNativeWindowMap.end()) {
				ALOGW("impl_SetSurfaceNativeWindow: unset sideband, tunerId:%d", tunnelId);
				native_window_set_sideband_stream(item->second.get(), nullptr);
				nativeHandleMap.erase(tunnelId);
				aNativeWindowMap.erase(tunnelId);
			}
		}
// #endif
		// KTPlayerHidlClient::instance().setSurfaceNativeWindow(hidlSurface);
	}
}

/**
 *
 * @param[in] handleId handle ID.
 * @return return Player's pointer of ANativeWindow
 */
ANativeWindow* impl_GetSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId)
{
	int tunnelId = impl_GetTunerId(handleId);
	auto item = aNativeWindowMap.find(tunnelId);
	if(item != aNativeWindowMap.end()) {
		return item->second.get();
	}
	return nullptr;
}



/*** AML NOTE:
 *	impl_prepare is almost same as CreatePlayer
 ***/
/**
 * Prepare playback.
 * blocks until MediaPlayer is ready for playback
 * 
 * Note : calling sequence - this function would be called after setTunerId() and setSurfaceNativeWindow()
 *
 * @param[in] handleId handle ID.
 * @return 0 Success. -1 fail.
 */
int impl_Prepare(BPNI_PLAYER_HANDLE_ID handleId)
{
	ALOGW("impl_Prepare %d", handleId);
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().prepare((HIDL_KTPLAYER_HANDLE_ID)handleId);
	}
	return -1;
}

/**
 * Play A/V with given a-v PID
 * <br> this API can be called multiple times without calling stop()
 * <br> i.e.) playStream() -> stop() -> playStream()
 * <br> ex 1) playStream(8006, 8005) -> playStream(8007, 8005)  : change audio language or change audio track
 * <br> ex 2) playStream(8006, 8005) -> playSteram(0, 8005) : normal AV play -> play video ONLY
 * <br> ex 3) playStream(0, 8005) -> playSteram(8006, 8005) : play video ONLY -> player both A/V
 * <br> ex 4) playStream(8006, 8005) -> playStream(0, 0) : same as stop(), no A/V playing
 *
 * @param[in] handleId handle ID.
 * @param[in] audiPID - audio PID. "0" means NO audio or stop audio (audio decoder stop)
 * @param[in] videoPID - video PID. "0" means NO video or stop video (video decoder stop)
 *
 * @return 0 Success. -1 fail.
 */
int impl_PlayStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().playStream((HIDL_KTPLAYER_HANDLE_ID)handleId, audiPID, videoPID);
	}
	return -1;
}

/**
 * Stop A/V.
 * Stop selective Audio or Video. (choiceable)
 *
 * @param[in] handleId handle ID.
 * @param[in] isAudioStop if true,  audio stop
 * @param[in] isVideoStop if true, video stop
 * @return 0 Success, -1 fail.
 */
int impl_Stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().stop((HIDL_KTPLAYER_HANDLE_ID)handleId, isAudioStop, isVideoStop);
	}
	return -1;
}



/**
 * set volume control
 *
 * @param[in] handleId handle ID.
 * @param[in] volume value: range = 0.0f ~ 1.0f, step => 0.01
 * @return 0 Success. -1 fail.
 */
int impl_SetVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().setVolume((HIDL_KTPLAYER_HANDLE_ID)handleId, volume);
	}	
	return -1;
}

/**
 * return current volume level
 *
 * @param[in] handleId handle ID.
 * @return current volume level. If not available or fail, return -1.0f.
 */
float impl_GetVolume(BPNI_PLAYER_HANDLE_ID handleId)
{
	
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().getVolume((HIDL_KTPLAYER_HANDLE_ID)handleId );
	}	
	return -1.0f;

}


int impl_SetAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().setAudioEnabled((HIDL_KTPLAYER_HANDLE_ID)handleId, enabled );
	}
	return 0;
}


bool impl_GetAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().getAudioEnabled((HIDL_KTPLAYER_HANDLE_ID)handleId);

	}
	return false;
}




/**
 * Configure audio PID for Bluetooth device
 *
 * @param[in] handleId handle ID.
 * @param[in] audioPid audio PID.
 * @return success: 0,  fail: -1
 */
int impl_SetBTAudioOutputPid(BPNI_PLAYER_HANDLE_ID handleId, int audioPid)
{
	
	//TODO:::: *****
	
	
	return -1;
}

/**
 * Set trick playback rate
 * In case of normal trick (NOT 0.8x, 1.2x, 1.5x), flush remained buffer
 *
 * @param[in] handleId handle ID.
 * @param rate  : playback rate
 * @return 0 Success. -1 fail.
 */
int impl_SetRate(BPNI_PLAYER_HANDLE_ID handleId, float rate)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().setRate((HIDL_KTPLAYER_HANDLE_ID)handleId, rate );
	}	

	return -1;
}

/**
 * return current playback rate
 *
 * @param[in] handleId handle ID. 
 * @return current playback rate. If not playing, return 0f.
 */
float impl_GetRate(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().getRate((HIDL_KTPLAYER_HANDLE_ID)handleId);
	}	

	return -1;
}

/**
 * Return PTS
 *
 * @param[in] handleId handle ID.
 * @return PTS. fail: -1 return.
 */
double impl_GetPts(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().getPts((HIDL_KTPLAYER_HANDLE_ID)handleId);
	}	
	return -1;
}

/**
 *  Flush stream data buffer inside of Player
 *
 * @param[in] handleId handle ID.
 */
void impl_Flush(BPNI_PLAYER_HANDLE_ID handleId)
{
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().flush((HIDL_KTPLAYER_HANDLE_ID)handleId);
	}	
}

/**
 * Set ANativeWindow surface for drawing ClosedCaption
 *
 * @param[in] handleId handle ID.
 * @param[in] window pointer of ANativeWindow surface for CC
 */
void impl_SetSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	//TODO -----> must refer to nativerander example
    if (__isValidPlyaerId(handleId)) {
        ALOGW("impl_SetSubtitleSurfaceNativeWindow: %p, handleId:%d", window, handleId);

		//HIDL_NativeWindow hidlSurface;
		//hidlSurface.addr = (int32_t)window;
		//hidlSurface.handleId = (HIDL_KTPLAYER_HANDLE_ID)handleId;
        KTPlayerHidlClient::instance().setSubtitleSurfaceNativeWindow(handleId, window);
    }
}

/**
 * Subtitle (Closed Caption / VoD SMI subtitle)  On / Off
 *
 * @param[in] handleId handle ID.
 * @param[in] enabled true = On , false = Off
 */
void impl_SetSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	//TODO
	ALOGW("impl_SetSubtitleEnabled: %d", enabled);
    if (__isValidPlyaerId(handleId)) {
        KTPlayerHidlClient::instance().setSubtitleEnabled((HIDL_KTPLAYER_HANDLE_ID)handleId, enabled);
    }
}

/**
 * Return Subtitle On or Off status
 *
 * @param[in] handleId handle ID.
 * @return On / Off 여부
 */
bool impl_GetSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	//TODO
    bool ret = false;
    if (__isValidPlyaerId(handleId)) {
        ret = KTPlayerHidlClient::instance().getSubtitleEnabled((HIDL_KTPLAYER_HANDLE_ID)handleId);
    }

	ALOGW("impl_GetSubtitleEnabled: %d", ret);
    return ret;
}


/**
 * Set SMI URL for VoD
 *
 * @param[in] handleId handle ID.
 * @param[in] url SMI subtitle URL
 */
void impl_SetSubtitleSmiUrl(BPNI_PLAYER_HANDLE_ID handleId, char* url)
{
    if (__isValidPlyaerId(handleId)) {
		std::string subtitleUrl;
		if(subtitleUrl != nullptr)
		{
			ALOGD("impl_SetSubtitleSmiUrl url=%s",url);
			subtitleUrl = url;
		}else
		{
			subtitleUrl = "";
		}
       
        KTPlayerHidlClient::instance().setSubtitleSmiUrl((HIDL_KTPLAYER_HANDLE_ID)handleId, subtitleUrl);
    }
}

/**
 * Set raw PMT table
 *
 * @param[in] handleId handle ID.
 * @param[in] pmtData PMT data.
 * @param[in] len PMT data's length
 */
void impl_SetPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len)
{
	ALOGW("impl_SetPmt %d", handleId);
//	KTPLAYER_HEXDUMP(LOG_TAG, (void *)pmtData, (size_t)len);
	if( __isValidPlyaerId(handleId) )
	{
		return KTPlayerHidlClient::instance().setPmt(handleId, pmtData, len);
	}
}

/**
 * Get A/V meta data
 *
 * @param[in] handleId handle ID.
 * @return BaseMediaMetaData
 */
BPNI_MediaMetaData impl_GetMetaData(BPNI_PLAYER_HANDLE_ID handleId)
{
	static BPNI_MediaMetaData meta;
	if( __isValidPlyaerId(handleId) )
	{
		HIDL_MediaMetaData hidlMeta = KTPlayerHidlClient::instance().getMetaData((HIDL_KTPLAYER_HANDLE_ID)handleId);
		
		meta.ar_type 	= (BPNI_VIDEO_AR_TYPE)hidlMeta.ar_type;
		meta.width 		= hidlMeta.width;
		meta.height 	= hidlMeta.height;
		meta.audioCodec = (BPNI_AUDIO_CODEC)hidlMeta.audioCodec;
		meta.videoCodec = (BPNI_VIDEO_CODEC)hidlMeta.videoCodec;
	}
	return meta;
}


/**
  * Register BPNI_MDStatusCallback
  *
  * @param[in] handleId handle ID.
  * @param[in] callback
  */
void impl_Cas_registerMDStatusCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback)
{
	//TODO
	KTPlayerHidlClient::instance().setMDStatusCallback(handleId, callback);

}


/**
 * This function would be called every starting/ending session for Live & VoD playbak.
 * This indicates CAS plugin have to prepare new stream playback.
 * starting new cahnnel : valid url
 * ending current channel: NULL url  (AML note: almost same as destroy player)
 *
 * @param[in] handleId handle ID.
 * @param[in] service_id: program number // In case of VoD, it should be "0"
 * @param[in] url session URL. (e.g.   udp://xxx.xxx.xxx.xxx.:port)
 * @param[in] type session Type. VoD : 0 , BTV : 1
 * @param[in] number_of_components number of ES stream
 * @param[in] pid_list array of ESPID
 * @return success: 0, fail: -1.
 */
int impl_Cas_channelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list)
{
	std::string str;
	if(url != nullptr)
	{
		ALOGD("impl_Cas_channelChange url=%s",url);
		str = url;
	}else
	{
		str = "";
	}

	HIDL_CASChange casChange;
	casChange.handleId = (HIDL_KTPLAYER_HANDLE_ID)handleId;
	casChange.service_id = service_id;
	casChange.url = str;
	casChange.type = type;
	casChange.number_of_components = number_of_components;
	
	std::vector<int32_t> _vec;
	for(int i=0; i<number_of_components; i++)
	{
		_vec.push_back(*(pid_list++));
	}
	casChange.pid_list = _vec;
	KTPlayerHidlClient::instance().cas_channelChange(casChange);
	return 0;
}

/**
 * Whenever application starts receiving Meta data this function would be called, KT CAS plugin will start metat data by this.
  *
 * @param[in] handleId handle ID.
  * @param[in] descriptor_blob_input (string type) received VoD meta data from server by Java App
 * @param[in] descriptor_blob_input_length length of VOD Meta data
 * @return success: 0, fail: -1
 */
int impl_Cas_MDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, char* descriptor_blob_input, int descriptor_blob_input_length)
{
	
	KTPlayerHidlClient::instance().cas_MDReceptionStart((HIDL_KTPLAYER_HANDLE_ID)handleId, descriptor_blob_input, descriptor_blob_input_length);
	return 0;
	
}

/**
 * In case BPNI_MDStatusCallback with success event, call impl_cas_MDReceptionEnd for notifying 
 *
 * @param[in] handleId handle ID.
 */
void impl_Cas_MDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId)
{
	
	KTPlayerHidlClient::instance().cas_MDReceptionEnd((HIDL_KTPLAYER_HANDLE_ID)handleId);

}

/**
 * Start VoD playback, if necessary, with adjusting playback position or direction and speed
 *
 * @param[in] handleId handle ID.
 * @param[in] [Unused] position position of VoD playback. milli second
 * @param[in] speed (+)FF, (-)REW
 * @return success 0, fail -1.
 */
int impl_Cas_setPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, long position, int speed)
{
	KTPlayerHidlClient::instance().cas_setPlayPosition((HIDL_KTPLAYER_HANDLE_ID)handleId, position, speed);
	return 0;
}

/**
  * Send CAS Event
  *
  * @param[in] jsonString
  * @return 0 Success. -1 fail.
  */
int impl_Cas_sendCasEvent(char* jsonString)
{
	std::string str;
	if(jsonString != nullptr)
	{
		ALOGD("impl_Cas_sendCasEvent url=%s",jsonString);
		str = jsonString;
	}else
	{
		str = "";
	}

	KTPlayerHidlClient::instance().cas_sendCasEvent(str);
	return 0;
}



/**
 * Register CAS event callback
 *
 * @param[in] callback Callback function
  */
void impl_Cas_registerCasEventCallback(BPNI_CAS_CasEventCallback callback)
{
	KTPlayerHidlClient::instance().setCasEventCallback(callback);
	
}

/**
  * Setting EMM Address
  *
  * @return 0 Success. -1 fail.
  */
int impl_Cas_setEMMAddress(char* address)
{
	ALOGW("impl_Cas_setEMMAddress: %s", address);

	std::string addr;
	if(address != nullptr)
	{
		addr = address;
	}else
	{
		addr = "";
	}



	KTPlayerHidlClient::instance().cas_setEMMAddress(addr);
	return 0;
}


