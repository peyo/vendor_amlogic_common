// FIXME: your file license if you have one

#pragma once

#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayerEventCallback.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace vendor::amlogic::hardware::ktplayer::implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct KTPlayerEventCallback : public V1_0::IKTPlayerEventCallback {
    // Methods from ::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayerEventCallback follow.
    Return<void> notify(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_EventData& event) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" IKTPlayerEventCallback* HIDL_FETCH_IKTPlayerEventCallback(const char* name);

}  // namespace vendor::amlogic::hardware::ktplayer::implementation
