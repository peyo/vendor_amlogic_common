/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
 
#undef NDEBUG
#define LOG_NDEBUG 0
#define LOG_TAG "KTPlayerd"
#include <log/log.h>
#include <binder/ProcessState.h>
#include <hidl/LegacySupport.h>
#include <vendor/amlogic/hardware/ktplayer/1.0/IKTPlayer.h>
#include <KTPlayerServer.h>
using namespace android;

using vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayer;
using android::hardware::defaultPassthroughServiceImplementation;

int main() 
{
    ALOGD("prepare IKTPlayer Service vndbinder");
    android::sp<IKTPlayer> ktPlayer = new KTPlayerServer();

    android::ProcessState::initWithDriver("/dev/vndbinder");
    android::ProcessState::self()->startThreadPool();	//	enable other binder's callback.
    {
        ALOGD("Starting IKTPlayer Service vndbinder");

        android::hardware::configureRpcThreadpool(16, true /* callerWillJoin */);
        ktPlayer->registerAsService();
        android::hardware::joinRpcThreadpool();
    }

    return 1;
}


