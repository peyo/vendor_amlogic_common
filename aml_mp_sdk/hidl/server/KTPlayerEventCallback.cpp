// FIXME: your file license if you have one

#include "KTPlayerEventCallback.h"

namespace vendor::amlogic::hardware::ktplayer::implementation {

// Methods from ::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayerEventCallback follow.
Return<void> KTPlayerEventCallback::notify(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_EventData& event) {
    // TODO implement
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IKTPlayerEventCallback* HIDL_FETCH_IKTPlayerEventCallback(const char* /* name */) {
    //return new KTPlayerEventCallback();
//}
//
}  // namespace vendor::amlogic::hardware::ktplayer::implementation
