/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
 
#undef NDEBUG
#define LOG_NDEBUG 0
#define LOG_TAG "KTHALPlayerServer"

#include <log/log.h>
#include "KTPlayerServer.h"
#include "AmlPlayerManager.h"

namespace android {
sp<IKTPlayerEventCallback> KTPlayerServer::mKTPlayerHidlEventCallback[MAX_MP_PLAYER_INSTANCE_NUM+1];

KTPlayerServer::KTPlayerServer()
: mPeerDeathMonitor(new PeerDeathMonitor(this)) //TODO
{
	mServer = nullptr;
}


KTPlayerServer::~KTPlayerServer()
{
	if(mServer)
		mServer->unlinkToDeath(mPeerDeathMonitor);
	mPeerDeathMonitor.clear();
	
}

void KTPlayerServer::connectServer()
{
	if(mServer == nullptr)
	{
		mServer = IKTPlayer::getService();

		if(mServer != nullptr)
		{
			ALOGI("Connected: %p", mServer.get());
			mServer->linkToDeath(mPeerDeathMonitor, 0);
		}
		else
			return ;
	}
	return ;
}

void KTPlayerServer::onPeerDied(uint64_t cookie, const wp<IBase>& who)
{
    ALOGE("ktplayer(handle:%lld) died, release resource!", cookie);

    AmlPlayerManager::instance().stop((BPNI_PLAYER_HANDLE_ID)cookie, true, true);
}

Return<void> KTPlayerServer::registerKTPlayerEventCallback(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, const sp<::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayerEventCallback>& callback) {
    ALOGI("registerKTPlayerEventCallback handleId:%d, cb:%p", handleId, callback.get());
    mKTPlayerHidlEventCallback[(int)handleId] = callback;

    callback->linkToDeath(mPeerDeathMonitor, (int)handleId);

    AmlPlayerManager::instance().registerPlayerEventCallback(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), [](BPNI_PLAYER_HANDLE_ID handleId, int event) {
        HIDL_EventData eventData;
        eventData.handleId = (HIDL_KTPLAYER_HANDLE_ID)handleId;
        eventData.event_type = event;
        mKTPlayerHidlEventCallback[handleId]->notify(eventData);
    });
    return Void();
}

Return<void> KTPlayerServer::setTunerId(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t tunerId)
{
	connectServer();
	AmlPlayerManager::instance().setTunerId(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), tunerId);
	return Void();
}

Return<int32_t> KTPlayerServer::getTunerId(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	connectServer();
	return AmlPlayerManager::instance().getTunerId(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
}

Return<void> KTPlayerServer::feedData(const HIDL_FeedData& data)
{
	connectServer();
	auto memory = mapMemory(data.data);
	char* buffer = static_cast<char*>(static_cast<void*>(memory->getPointer()));
	RETURN_IF_MSG(Void(), nullptr == buffer, "null buffer");
	AmlPlayerManager::instance().feedData(static_cast<BPNI_PLAYER_HANDLE_ID>(data.handleId), buffer, data.len);
	return Void();
}

Return<void> KTPlayerServer::setSurfaceNativeWindow(const HIDL_NativeWindow& surface)
{
	connectServer();
#if 0
	auto memory = mapMemory(surface.window);
	ANativeWindow* window = static_cast<ANativeWindow*>(static_cast<void*>(memory->getPointer()));
	RETURN_IF(Void(), nullptr == window);
#else//TODO: using passthrough (same process), not binderized
	ANativeWindow* window = (ANativeWindow*)surface.addr;
#endif	
	ALOGW("===TO: setSurfaceNativeWindow=%p, handleId: %d", window, surface.handleId);
	AmlPlayerManager::instance().setSurfaceNativeWindow(static_cast<BPNI_PLAYER_HANDLE_ID>(surface.handleId), window);

	return Void();
}
Return<void> KTPlayerServer::getSurfaceNativeWindow(HIDL_KTPLAYER_HANDLE_ID handleId, getSurfaceNativeWindow_cb _hidl_cb)
{
	//TODO:  
	connectServer();
	ANativeWindow* window = AmlPlayerManager::instance().getSurfaceNativeWindow(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
	RETURN_IF(Void(), nullptr == window);

	HIDL_NativeWindow hidlSurface;

	hidlSurface.handleId = handleId;

#if 0
    //	alloc shared memory.
    auto allocator = IAllocator::getService("ashmem");
    RETURN_IF(Void(), nullptr == allocator.get());

    bool alloc_success = false;
    allocator->allocate(sizeof(window), [&](bool success, const hidl_memory& memory)
    	{
    		if(success == true && memory.size() == sizeof(window))
    		{
				alloc_success = true;
    			hidlSurface.window = memory;
				hidlSurface.len = sizeof(window);
    		}
    	});
    if(!alloc_success) return Void();

    //	map shared memory.
    auto memory = mapMemory(hidlSurface.window);
	RETURN_IF(Void(), nullptr == memory.get());
    uint8_t* buffer = static_cast<uint8_t*>(static_cast<void*>(memory->getPointer()));
    RETURN_IF(Void(), nullptr == buffer);

    //	copy section data to shared memory.
    memory->update();
    memcpy(buffer, window, sizeof(window));
    memory->commit();
#else
	//TODO: using passthrough (same process), not binderized
	hidlSurface.addr = (int32_t)window;

#endif
	_hidl_cb(hidlSurface);
	return Void();
}

Return<int32_t> KTPlayerServer::prepare(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	connectServer();
	return AmlPlayerManager::instance().prepare(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
}

Return<int32_t> KTPlayerServer::playStream(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audiPID, int32_t videoPID)
{
	connectServer();
	return AmlPlayerManager::instance().playStream(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), audiPID, videoPID);
}

Return<int32_t> KTPlayerServer::stop(HIDL_KTPLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	connectServer();
	return AmlPlayerManager::instance().stop(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), isAudioStop, isVideoStop);
}

Return<int32_t> KTPlayerServer::setVolume(HIDL_KTPLAYER_HANDLE_ID handleId, float volume)
{
	connectServer();
	return AmlPlayerManager::instance().setVolume(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), volume);
}
Return<float> KTPlayerServer::getVolume(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	connectServer();
	return AmlPlayerManager::instance().getVolume(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId) );
}

Return<int32_t> KTPlayerServer::setAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled) 
{
	connectServer();
	return AmlPlayerManager::instance().setAudioEnabled(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId),  enabled);
}

Return<bool> KTPlayerServer::getAudioEnabled(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	connectServer();
	return AmlPlayerManager::instance().getAudioEnabled(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId) );
}


Return<int32_t>  KTPlayerServer::setBTAudioOutputPid(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audioPid)
{
	//TODO:::: *****

	return -1;
}
Return<int32_t>  KTPlayerServer::setRate(HIDL_KTPLAYER_HANDLE_ID handleId, float rate)
{
	return AmlPlayerManager::instance().setRate(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId), rate );
}
Return<float>  KTPlayerServer::getRate(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return AmlPlayerManager::instance().getRate(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
}
Return<double>  KTPlayerServer::getPts(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return AmlPlayerManager::instance().getPts(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
}
Return<void>  KTPlayerServer::flush(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	AmlPlayerManager::instance().flush(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
	return Void(); 
}
Return<void>  KTPlayerServer::setSubtitleSurfaceNativeWindow(const HIDL_NativeWindow& surface)
{
	connectServer();
	return Void(); 
}
Return<void>  KTPlayerServer::setSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled)
{
	return Void(); 
}
Return<bool>  KTPlayerServer::getSubtitleEnabled(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	return false; 
}
Return<void>  KTPlayerServer::setSubtitleSmiUrl(HIDL_KTPLAYER_HANDLE_ID handleId, const hidl_string& url)
{
	connectServer();
	std::string _url(url);
	ALOGW("setSubtitleSmiUrl[%d] : %s", handleId, _url.c_str());
	//AmlPlayerManager::instance().setSubtitleSmiUrl
	return Void(); 
}
Return<void>  KTPlayerServer::setPmt(const HIDL_PmtData& pmt)
{
	connectServer();
	auto memory = mapMemory(pmt.pmtData);
	char* buffer = static_cast<char*>(static_cast<void*>(memory->getPointer()));
	RETURN_IF(Void(), nullptr == buffer);
	AmlPlayerManager::instance().setPmt(static_cast<BPNI_PLAYER_HANDLE_ID>(pmt.handleId), buffer, pmt.len);
	
//	KTPLAYER_HEXDUMP(LOG_TAG, (void *)buffer, (size_t)pmt.len);
	
	return Void(); 
}
Return<void>  KTPlayerServer::getMetaData(HIDL_KTPLAYER_HANDLE_ID handleId, getMetaData_cb _hidl_cb)
{
	BPNI_MediaMetaData meta = AmlPlayerManager::instance().getMetaData(static_cast<BPNI_PLAYER_HANDLE_ID>(handleId));
	
	HIDL_MediaMetaData hidlMeta;
	hidlMeta.ar_type 	=  static_cast<HIDL_KTPLAYER_VIDEO_AR_TYPE>(meta.ar_type);
	hidlMeta.width 		=  meta.width;
	hidlMeta.height 	=  meta.height;
	hidlMeta.audioCodec =  static_cast<HIDL_KTPLAYER_AUDIO_CODEC>(meta.audioCodec);
	hidlMeta.videoCodec =  static_cast<HIDL_KTPLAYER_VIDEO_CODEC>(meta.videoCodec);

	_hidl_cb(true, hidlMeta);
	return Void(); 
}

Return<int32_t>  KTPlayerServer::cas_channelChange(const HIDL_CASChange& info)
{
	BPNI_PLAYER_HANDLE_ID handleId = (BPNI_PLAYER_HANDLE_ID)info.handleId;
	char* _url = (char*)info.url.c_str();

	std::vector<int32_t> pid_vector;
	copy_vector(pid_vector, info.pid_list);
	int* pid_list = (int*)pid_vector.data();

	AmlPlayerManager::instance().casChannelChange(handleId, info.service_id, _url, info.type, info.number_of_components, pid_list);
	return 0; 
}
Return<void>  KTPlayerServer::cas_MDReceptionStart(HIDL_KTPLAYER_HANDLE_ID handleId, const hidl_string& descriptor_blob_input, int32_t descriptor_blob_input_length)
{
	std::string descriptor_blob(descriptor_blob_input);
	AmlPlayerManager::instance().casMDReceptionStart((BPNI_PLAYER_HANDLE_ID)handleId, descriptor_blob, descriptor_blob_input_length);
	return Void(); 
}
Return<void>  KTPlayerServer::cas_MDReceptionEnd(HIDL_KTPLAYER_HANDLE_ID handleId)
{
	AmlPlayerManager::instance().casMDReceptionEnd((BPNI_PLAYER_HANDLE_ID)handleId);
	return Void(); 
}
Return<int32_t>  KTPlayerServer::cas_setPlayPosition(HIDL_KTPLAYER_HANDLE_ID handleId, int32_t position, int32_t speed)
{
	return AmlPlayerManager::instance().casSetPlayPosition((BPNI_PLAYER_HANDLE_ID)handleId, position, speed);
}
Return<int32_t>  KTPlayerServer::cas_sendCasEvent(const hidl_string& jsonString)
{
	std::string jsonStr(jsonString);

	return AmlPlayerManager::instance().casSendCasEvent(jsonStr);
}
Return<int32_t>  KTPlayerServer::cas_setEMMAddress(const hidl_string& address)
{
	std::string _addr(address);

	return AmlPlayerManager::instance().casSetEMMAddress(_addr);

}
Return<void> KTPlayerServer::notify(const HIDL_EventData& event)
{
	return Void(); 
}


} // namespace android
