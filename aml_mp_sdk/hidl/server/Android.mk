LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := ktplayerd
#LOCAL_PROPRIETARY_MODULE := true
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_CLASS := EXECUTABLES
#LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_INIT_RC := ktplayerd.rc
# LOCAL_VINTF_FRAGMENTS := vendor.amlogic.hardware.ktplayer@1.0.xml

LOCAL_SRC_FILES:= KTPlayerServer.cpp \
			KTPlayerEventCallback.cpp \
			service.cpp

LOCAL_SHARED_LIBRARIES := \
        vendor.amlogic.hardware.ktplayer@1.0 \
        libbase  \
		libbinder  \
        libhidlbase  \
        libcutils \
        libutils \
        liblog \
		android.hidl.allocator@1.0  \
		android.hidl.memory@1.0 \
		libhidlmemory \
		android.hardware.cas@1.0 \
		android.hardware.cas.native@1.0 \
		libaml_kt_hal.vendor \


LOCAL_C_INCLUDES +=	\
		bionic/libc/private \
		system/libhidl/libhidlmemory/include \
		vendor/amlogic/common/aml_mp_sdk \
		vendor/amlogic/common/aml_mp_sdk/hidl/common \
		vendor/amlogic/common/aml_mp_sdk/include \
		vendor/amlogic/common/aml_mp_sdk/tests/amlKTPlayerHal \
		vendor/amlogic/common/mediahal_sdk/include \
		frameworks/native/include \
		frameworks/native/libs/nativewindow/include \
		frameworks/native/libs/arect/include \
		frameworks/native/libs/nativebase/include \
		system/libhidl/libhidlmemory/include \
		system/libhidl/transport/token/1.0/utils/include \
		hardware/libhardware/include \

LOCAL_CFLAGS += \
		-Wall \
		-O0 \
		-Wno-error \
		-Wno-unused-function \
		-Werror=format-security \
		-Werror=array-bounds \
		-Wno-unused-variable \
		-Wno-unused-parameter \


include $(BUILD_EXECUTABLE)