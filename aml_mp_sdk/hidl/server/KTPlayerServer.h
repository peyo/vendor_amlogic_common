/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */
 
#ifndef VENDOR_KTPLAYER_V1_0_MP_PLAYER_H
#define VENDOR_KTPLAYER_V1_0_MP_PLAYER_H

#include <utils/Mutex.h>
#include <vector>
#include <map>
#include <utils/Mutex.h>
#include <vector>
#include <map>
#include <hidl_common.h>
#include <android/native_window.h>
#include "BasePlayerNativeInterface.h"

#define MAX_MP_PLAYER_INSTANCE_NUM   (4 + 1)

namespace android {
using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;


struct KTPlayerServer : public IKTPlayer, public IKTPlayerEventCallback, public PeerDeathMonitor::Handler
{
public:
    KTPlayerServer();
    ~KTPlayerServer();

    Return<void> registerKTPlayerEventCallback(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, const sp<::vendor::amlogic::hardware::ktplayer::V1_0::IKTPlayerEventCallback>& callback) override;
    Return<void> setTunerId(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, int32_t tunerId) override;
    Return<int32_t> getTunerId(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<void> feedData(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_FeedData& data) override;
    Return<void> setSurfaceNativeWindow(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_NativeWindow& surface) override;
    Return<void> getSurfaceNativeWindow(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, getSurfaceNativeWindow_cb _hidl_cb) override;
    Return<int32_t> prepare(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<int32_t> playStream(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audiPID, int32_t videoPID) override;
    Return<int32_t> stop(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop) override;
    Return<int32_t> setVolume(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, float volume) override;
    Return<float> getVolume(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<int32_t> setAudioEnabled(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled) override;
    Return<bool> getAudioEnabled(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<int32_t> setBTAudioOutputPid(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, int32_t audioPid) override;
    Return<int32_t> setRate(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, float rate) override;
    Return<float> getRate(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<double> getPts(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<void> flush(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<void> setSubtitleSurfaceNativeWindow(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_NativeWindow& surface) override;
    Return<void> setSubtitleEnabled(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, bool enabled) override;
    Return<bool> getSubtitleEnabled(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<void> setSubtitleSmiUrl(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, const hidl_string& url) override;
    Return<void> setPmt(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_PmtData& pmt) override;
    Return<void> getMetaData(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, getMetaData_cb _hidl_cb) override;
    Return<int32_t> cas_channelChange(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_CASChange& info) override;
    Return<void> cas_MDReceptionStart(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, const hidl_string& descriptor_blob_input, int32_t descriptor_blob_input_length) override;
    Return<void> cas_MDReceptionEnd(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId) override;
    Return<int32_t> cas_setPlayPosition(::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_KTPLAYER_HANDLE_ID handleId, int32_t position, int32_t speed) override;
    Return<int32_t> cas_sendCasEvent(const hidl_string& jsonString) override;
    Return<int32_t> cas_setEMMAddress(const hidl_string& address) override;

protected:
	virtual void onPeerDied(uint64_t cookie, const wp<IBase>& who) override;
	Return<void> notify(const ::vendor::amlogic::hardware::ktplayer::V1_0::HIDL_EventData& event) override;


private:

	void connectServer();

    static sp<IKTPlayerEventCallback> mKTPlayerHidlEventCallback[MAX_MP_PLAYER_INSTANCE_NUM+1];

	sp<IKTPlayer> mServer;
	sp<PeerDeathMonitor> mPeerDeathMonitor;
};

} // namespace android

#endif //VENDOR_KTPLAYER_V1_0_MP_PLAYER_H
