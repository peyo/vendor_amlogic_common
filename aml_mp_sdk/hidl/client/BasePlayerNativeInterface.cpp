/*
 * Copyright (c) 2020 Amlogic, Inc. All rights reserved.
 *
 * This source code is subject to the terms and conditions defined in the
 * file 'LICENSE' which is part of this source code package.
 *
 * Description:
 */


#define LOG_NDEBUG 0
#define LOG_TAG "KTclient"
#include <BasePlayerNativeInterface.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <fcntl.h>
#ifndef ALOGV
#define ALOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__);
#define ALOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);
#define ALOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);
#define ALOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__);
#define ALOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);
#endif

#define RETURN(value) \
		ALOGW("%s:%d return " #value, __FUNCTION__, __LINE__); \
		return value;

#define RETURN_VOID_IF(cond) \
		do { \
			if(cond) \
			{ \
				ALOGW("%s:%d return <- %s", __FUNCTION__, __LINE__, #cond); \
				return; \
			} \
		} while(0)
#define RETURN_IF(error, cond) \
		do { \
			if(cond) \
			{ \
				ALOGW("%s:%d return %s <- %s", __FUNCTION__, __LINE__, #error, #cond); \
				return error; \
			} \
		} while(0)
#define CHECK_INTF(a, b) \
		load_library(); \
		RETURN_IF(0, sHandle == NULL); \
		if(a == NULL) \
			a = (b)dlsym(sHandle, "impl_"#b); \
		RETURN_IF(0, a == NULL)

#define CHECK_INTF_VOID(a, b) \
		load_library(); \
		RETURN_VOID_IF(sHandle == NULL); \
		if(a == NULL) \
			a = (b)dlsym(sHandle, "impl_"#b); \
		RETURN_VOID_IF(a == NULL)
#define CHECK_INTF_BOOL(a, b) \
		load_library(); \
		RETURN_IF(false, sHandle == NULL); \
		if(a == NULL) \
			a = (b)dlsym(sHandle, "impl_"#b); \
		RETURN_IF(false, a == NULL)


typedef void (*RegisterPlayerEventCallback)(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback);
typedef void (*SetTunerId)(BPNI_PLAYER_HANDLE_ID handleId, int tunerId);
typedef int (*GetTunerId)(BPNI_PLAYER_HANDLE_ID handleId);
typedef void (*FeedData)(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len);
typedef void (*SetSurfaceNativeWindow)(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);
typedef ANativeWindow* (*GetSurfaceNativeWindow)(BPNI_PLAYER_HANDLE_ID handleId);
typedef int (*Prepare)(BPNI_PLAYER_HANDLE_ID handleId);
typedef int (*PlayStream)(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID);
typedef int (*Stop)(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop);
typedef int (*SetVolume)(BPNI_PLAYER_HANDLE_ID handleId, float volume);
typedef float (*GetVolume)(BPNI_PLAYER_HANDLE_ID handleId);
typedef int (*SetAudioEnabled)(BPNI_PLAYER_HANDLE_ID handleId, bool enabled);
typedef bool (*GetAudioEnabled)(BPNI_PLAYER_HANDLE_ID handleId);
typedef int (*SetBTAudioOutputPid)(BPNI_PLAYER_HANDLE_ID handleId, int audioPid);
typedef int (*SetRate)(BPNI_PLAYER_HANDLE_ID handleId, float rate);
typedef float (*GetRate)(BPNI_PLAYER_HANDLE_ID handleId);
typedef double (*GetPts)(BPNI_PLAYER_HANDLE_ID handleId);
typedef void (*Flush)(BPNI_PLAYER_HANDLE_ID handleId);
typedef void (*SetSubtitleSurfaceNativeWindow)(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window);
typedef void (*SetSubtitleEnabled)(BPNI_PLAYER_HANDLE_ID handleId, bool enabled);
typedef bool (*GetSubtitleEnabled)(BPNI_PLAYER_HANDLE_ID handleId);
typedef void (*SetSubtitleSmiUrl)(BPNI_PLAYER_HANDLE_ID handleId, char* url);
typedef void (*SetPmt)(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len);
typedef BPNI_MediaMetaData (*GetMetaData)(BPNI_PLAYER_HANDLE_ID handleId);
typedef void (*Cas_registerMDStatusCallback)(BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback);
typedef int (*Cas_channelChange)(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list);
typedef int (*Cas_MDReceptionStart)(BPNI_PLAYER_HANDLE_ID handleId, char* descriptor_blob_input, int descriptor_blob_input_length);
typedef void (*Cas_MDReceptionEnd)(BPNI_PLAYER_HANDLE_ID handleId);
typedef int (*Cas_setPlayPosition)(BPNI_PLAYER_HANDLE_ID handleId, long position, int speed);
typedef int (*Cas_sendCasEvent)(char* jsonString);
typedef void (*Cas_registerCasEventCallback)(BPNI_CAS_CasEventCallback callback);
typedef int (*Cas_setEMMAddress)(char* address);

struct Interfaces
{
	RegisterPlayerEventCallback	registerPlayerEventCallback;
	SetTunerId	setTunerId;
	GetTunerId	getTunerId;
	FeedData	feedData;
	SetSurfaceNativeWindow	setSurfaceNativeWindow;
	GetSurfaceNativeWindow	getSurfaceNativeWindow;
	Prepare	prepare;
	PlayStream	playStream;
	Stop	stop;
	SetVolume	setVolume;
	GetVolume	getVolume;
	SetAudioEnabled setAudioEnabled;
	GetAudioEnabled getAudioEnabled;
	SetBTAudioOutputPid	setBTAudioOutputPid;
	SetRate	setRate;
	GetRate	getRate;
	GetPts	getPts;
	Flush	flush;
	SetSubtitleSurfaceNativeWindow	setSubtitleSurfaceNativeWindow;
	SetSubtitleEnabled	setSubtitleEnabled;
	GetSubtitleEnabled	getSubtitleEnabled;
	SetSubtitleSmiUrl	setSubtitleSmiUrl;
	SetPmt	setPmt;
	GetMetaData	getMetaData;
	Cas_registerMDStatusCallback	cas_registerMDStatusCallback;
	Cas_channelChange	cas_channelChange;
	Cas_MDReceptionStart	cas_MDReceptionStart;
	Cas_MDReceptionEnd	cas_MDReceptionEnd;
	Cas_setPlayPosition	cas_setPlayPosition;
	Cas_sendCasEvent	cas_sendCasEvent;
	Cas_registerCasEventCallback	cas_registerCasEventCallback;
	Cas_setEMMAddress	cas_setEMMAddress;
};


static void* sHandle = NULL;
static Interfaces intf;

static void unload_library()
{
	if(sHandle != NULL)
	{
		ALOGE("unload !!");
		dlclose(sHandle);
	}
}

static void load_library()
{
	if(sHandle != NULL)
		return;
	const char * libname = LIB_LOAD_TARGET;
	sHandle = dlopen(libname, RTLD_NOW);
	if(sHandle == NULL)
	{
		ALOGE("Load %s error : %s", libname, dlerror());
	}
	atexit(unload_library);
}


void bpni_registerPlayerEventCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_PlayerEventCallback callback)
{
	CHECK_INTF_VOID(intf.registerPlayerEventCallback, RegisterPlayerEventCallback);
	intf.registerPlayerEventCallback(handleId, callback);
}

void bpni_setTunerId(BPNI_PLAYER_HANDLE_ID handleId, int tunerId)
{
	ALOGE("bpni_setTunerId %d %d", handleId,tunerId );
	CHECK_INTF_VOID(intf.setTunerId, SetTunerId);
	intf.setTunerId(handleId, tunerId);
}

int bpni_getTunerId(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.getTunerId, GetTunerId);
	return intf.getTunerId(handleId);
}


void bpni_feedData(BPNI_PLAYER_HANDLE_ID handleId, char* data, int len)
{
	CHECK_INTF_VOID(intf.feedData, FeedData);
	intf.feedData(handleId, data, len);
}


void bpni_setSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	CHECK_INTF_VOID(intf.setSurfaceNativeWindow, SetSurfaceNativeWindow);
	intf.setSurfaceNativeWindow(handleId, window);
}


ANativeWindow* bpni_getSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId)
{
	load_library();
	RETURN_IF(nullptr, sHandle == nullptr);
	if(intf.getSurfaceNativeWindow == nullptr)
		intf.getSurfaceNativeWindow = (GetSurfaceNativeWindow)dlsym(sHandle, "impl_GetSurfaceNativeWindow");
	RETURN_IF(nullptr, intf.getSurfaceNativeWindow == nullptr);
	intf.getSurfaceNativeWindow(handleId);
	return nullptr;
}


int bpni_prepare(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.prepare, Prepare);
	return intf.prepare(handleId);
}


int bpni_playStream(BPNI_PLAYER_HANDLE_ID handleId, int audiPID, int videoPID)
{
	CHECK_INTF(intf.playStream, PlayStream);
	return intf.playStream(handleId, audiPID, videoPID);
}

int bpni_stop(BPNI_PLAYER_HANDLE_ID handleId, bool isAudioStop, bool isVideoStop)
{
	CHECK_INTF(intf.stop, Stop);
	return intf.stop(handleId, isAudioStop, isVideoStop);
}

int bpni_setVolume(BPNI_PLAYER_HANDLE_ID handleId, float volume)
{
	CHECK_INTF(intf.setVolume, SetVolume);
	return intf.setVolume(handleId, volume);
}

float bpni_getVolume(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.getVolume, GetVolume);
	return intf.getVolume(handleId);

}

int bpni_setAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	CHECK_INTF(intf.setAudioEnabled, SetAudioEnabled);
	return intf.setAudioEnabled(handleId, enabled);
}


bool bpni_getAudioEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.getAudioEnabled, GetAudioEnabled);
	return intf.getAudioEnabled(handleId);
}

int bpni_setBTAudioOutputPid(BPNI_PLAYER_HANDLE_ID handleId, int audioPid)
{
	CHECK_INTF(intf.setBTAudioOutputPid, SetBTAudioOutputPid);
	return intf.setBTAudioOutputPid(handleId, audioPid);
}

int bpni_setRate(BPNI_PLAYER_HANDLE_ID handleId, float rate)
{
	CHECK_INTF(intf.setRate, SetRate);
	return intf.setRate(handleId, rate);
}


float bpni_getRate(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.getRate, GetRate);
	return intf.getRate(handleId);
}


double bpni_getPts(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF(intf.getPts, GetPts);
	return intf.getPts(handleId);
}


void bpni_flush(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF_VOID(intf.flush, Flush);
	intf.flush(handleId);
}


void bpni_setSubtitleSurfaceNativeWindow(BPNI_PLAYER_HANDLE_ID handleId, ANativeWindow* window)
{
	CHECK_INTF_VOID(intf.setSubtitleSurfaceNativeWindow, SetSubtitleSurfaceNativeWindow);
	intf.setSubtitleSurfaceNativeWindow(handleId, window);
}

void bpni_setSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId, bool enabled)
{
	CHECK_INTF_VOID(intf.setSubtitleEnabled, SetSubtitleEnabled);
	intf.setSubtitleEnabled(handleId, enabled);
	
}

bool bpni_getSubtitleEnabled(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF_BOOL(intf.getSubtitleEnabled, GetSubtitleEnabled);
	return intf.getSubtitleEnabled(handleId);
}

void bpni_setSubtitleSmiUrl(BPNI_PLAYER_HANDLE_ID handleId, char* url)
{
    CHECK_INTF_VOID(intf.setSubtitleSmiUrl, SetSubtitleSmiUrl);
    return intf.setSubtitleSmiUrl(handleId, url);
}

void bpni_setPmt(BPNI_PLAYER_HANDLE_ID handleId, char* pmtData, int len)
{
	CHECK_INTF_VOID(intf.setPmt, SetPmt);
	intf.setPmt(handleId, pmtData, len);
}

BPNI_MediaMetaData bpni_getMetaData(BPNI_PLAYER_HANDLE_ID handleId)
{
	static BPNI_MediaMetaData temp;
	load_library();
	RETURN_IF(temp, sHandle == nullptr);
	if(intf.getMetaData == nullptr)
		intf.getMetaData = (GetMetaData)dlsym(sHandle, "impl_GetMetaData");
	RETURN_IF(temp, intf.getMetaData == nullptr);

	return intf.getMetaData(handleId);
}

void bpni_cas_registerMDStatusCallback(BPNI_PLAYER_HANDLE_ID handleId, BPNI_MDStatusCallback callback)
{
	CHECK_INTF_VOID(intf.cas_registerMDStatusCallback, Cas_registerMDStatusCallback);
	intf.cas_registerMDStatusCallback(handleId, callback);

}

int bpni_cas_channelChange(BPNI_PLAYER_HANDLE_ID handleId, int service_id, char* url, int type, int number_of_components, int* pid_list)
{
	CHECK_INTF(intf.cas_channelChange, Cas_channelChange);
	return intf.cas_channelChange(handleId, service_id, url, type, number_of_components, pid_list);
}

int bpni_cas_MDReceptionStart(BPNI_PLAYER_HANDLE_ID handleId, char* descriptor_blob_input, int descriptor_blob_input_length)
{
	CHECK_INTF(intf.cas_MDReceptionStart, Cas_MDReceptionStart);
	return intf.cas_MDReceptionStart(handleId, descriptor_blob_input, descriptor_blob_input_length);

}

void bpni_cas_MDReceptionEnd(BPNI_PLAYER_HANDLE_ID handleId)
{
	CHECK_INTF_VOID(intf.cas_MDReceptionEnd, Cas_MDReceptionEnd);
	intf.cas_MDReceptionEnd(handleId);
}

int bpni_cas_setPlayPosition(BPNI_PLAYER_HANDLE_ID handleId, long position, int speed)
{
	CHECK_INTF(intf.cas_setPlayPosition, Cas_setPlayPosition);
	return intf.cas_setPlayPosition(handleId, position, speed);
}

int bpni_cas_sendCasEvent(char* jsonString)
{
	CHECK_INTF(intf.cas_sendCasEvent, Cas_sendCasEvent);
	return intf.cas_sendCasEvent(jsonString);
}

void bpni_cas_registerCasEventCallback(BPNI_CAS_CasEventCallback callback)
{
	CHECK_INTF_VOID(intf.cas_registerCasEventCallback, Cas_registerCasEventCallback);
	intf.cas_registerCasEventCallback(callback);
}

int bpni_cas_setEMMAddress(char* address)
{
	CHECK_INTF(intf.cas_setEMMAddress, Cas_setEMMAddress);
	return intf.cas_setEMMAddress(address);
}


