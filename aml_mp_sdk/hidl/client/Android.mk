LOCAL_PATH:=$(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE:= libkthal.vendor

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES:= \
	BasePlayerNativeInterface.cpp

LOCAL_SHARED_LIBRARIES := \
	libdl \
	liblog

LOCAL_C_INCLUDES += \
	frameworks/native/include \
	frameworks/native/libs/nativewindow/include \
	frameworks/native/libs/arect/include

LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)

LOCAL_CFLAGS += -DLIB_LOAD_TARGET=\"libktplayerclient.vendor.so\"

LOCAL_PROPRIETARY_MODULE:=true

include $(BUILD_SHARED_LIBRARY)



#######################################
include $(CLEAR_VARS)

LOCAL_MODULE:= libkthal.platform

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES:= \
	BasePlayerNativeInterface.cpp

LOCAL_SHARED_LIBRARIES := \
	libdl \
	liblog


LOCAL_C_INCLUDES += \
	frameworks/native/include \
	frameworks/native/libs/nativewindow/include \
	frameworks/native/libs/arect/include

LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)

LOCAL_CFLAGS += -DLIB_LOAD_TARGET=\"libktplayerclient.platform.amlogic.so\"

LOCAL_PROPRIETARY_MODULE:=false

ifeq (1, $(shell expr $(PLATFORM_SDK_VERSION) \>= 30))
LOCAL_SYSTEM_EXT_MODULE := true
endif

 include $(BUILD_SHARED_LIBRARY)
